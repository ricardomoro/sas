$(document).ready(function() {
	$('#adicionar_imagem').click(function(e) {
		$('.ui-loader').show();
		
		$.ajax({
			url: '/admin/noticias/ajax_adicionar_nova_imagem',
			success: function(data) {
				$(data).hide().appendTo('#imagens').fadeIn(500);
				$('#imagens').trigger('create');
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert('Ocorreu um erro. Recarregue a página e tente novamente.');
			},
			complete: function() {
				$('.ui-loader').hide();
			}
		});
		
		e.preventDefault();
	});
	
	$('#imagens').on('click', '.remover_imagem', function(e) {
		var imagem_id = $(this).attr('imagem_id');
		if (imagem_id) {
			if (!confirm('Você tem certeza que deseja remover essa imagem?')) {
				return false;
			}
		}
		$(this).parent().fadeOut(500, function() {
			$(this).remove();
		});
		
		if (imagem_id) {
			$(this).parent().parent().append('<input type="hidden" value="'+imagem_id+'" name="Deletar[]"/>');
		}
		
		e.preventDefault();
	});
});
