$(document).ready(function() {
	$('#limpa_atividade').click(function(e) {
		$(".radio_atividade").removeAttr("checked");
		
		e.preventDefault();
	});
	
	$('#obra').hide();
	
	$('#obra_toggle_sim').click(function(e) {
		$('#ObraTitulo').val('');
		$('#ObraTipo').val('');
		
		$('#obra').fadeIn(500).show();
		
		$('#obra_toggle_nao').removeClass('negrito');
		$(this).addClass('negrito');
		
		e.preventDefault();
	});
	
	$('#obra_toggle_nao').click(function(e) {
		$('#ObraTitulo').val('');
		$('#ObraTipo').val('');
		
		$('#obra').fadeOut(300, function() {
			$(this).hide();
		});
		
		$('#obra_toggle_sim').removeClass('negrito');
		$(this).addClass('negrito');
		
		e.preventDefault();
	});
	
	/*$('#necessidade_especial_sim').click(function(e) {
		$('#ParticipanteNecessidadeEspecial').parent().show();
		e.preventDefault();
	});
	$('#necessidade_especial_nao').click(function(e) {
		$('#ParticipanteNecessidadeEspecial').parent().hide();
		e.preventDefault();
	});*/
});
