$(document).ready(function() {
	var filtro = false;
	$('.toggle_confirmado').click(function(e) {
		if (confirm('Você tem certeza que deseja mudar a confirmação do participante?')) {
			$('.ui-loader').show();
			
			var id = $(this).attr('participante_id');
			var element = this;
			
			$.ajax({
				type: 'POST',
				url: '/admin/participantes/toggle_confirmacao',
				data: {
					'id' : id,
				},
				success: function(data) {
					if (data == false) {
						alert('Ocorreu um erro. Por favor, recarregue a página e tente novamente.');
					} else {
						if (data == 'S') {
							$(element).removeClass('nao');
							$(element).addClass('sim');
							$(element).find('.ui-btn-text').text('Sim'); //JQuery Mobile
						} else if (data == 'N') {
							$(element).removeClass('sim');
							$(element).addClass('nao');
							$(element).find('.ui-btn-text').text('Não'); //JQuery Mobile
						}
					}
				},
				error: function() {
					alert('Ocorreu um erro. Por favor, recarregue a página e tente novamente.');
				},
				complete: function() {
					$('.ui-loader').hide();
				},
			});
		}
		
		e.preventDefault();
	});
	
	$('.filtros').click(function(e) {
		if (filtro) {
			$('#filtros').fadeOut(500, function() {
				$(this).hide();
			});
			filtro = false;
		} else {
			$('#filtros').fadeIn(500).show();
			filtro = true;
		}
	});
});
