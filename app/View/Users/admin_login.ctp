<h2>Formulário de Login</h2>
<?php
echo $this->Form->create('User');
	echo $this->Form->input('User.username', array('type' => 'text', 'label' => 'Usuário'));
	echo $this->Form->input('User.password', array('type' => 'password', 'label' => 'Senha'));
echo $this->Form->end(array('label' => 'Entrar', 'data-type' => 'button', 'data-icon' => 'forward', 'data-inline' => 'true'));
