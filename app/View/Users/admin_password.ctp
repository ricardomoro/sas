<h2>Troca de Senha</h2>
<?php
echo $this->Form->create('User');
	echo $this->Form->input('User.id', array('type' => 'hidden'));
	echo $this->Form->input('User.current_password', array('type' => 'password', 'label' => 'Senha Atual'));
	echo $this->Form->input('User.new_password', array('type' => 'password', 'label' => 'Nova Senha'));
	echo $this->Form->input('User.check_password', array('type' => 'password', 'label' => 'Confirme a Nova Senha'));
echo $this->Form->end(array('label' => 'Salvar', 'data-type' => 'button', 'data-icon' => 'check', 'data-inline' => 'true'));
echo $this->Html->link('Voltar', array('controller' => 'index', 'action' => 'index'), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true'));