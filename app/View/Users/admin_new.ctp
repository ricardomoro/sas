<h2>Novo Usu&aacute;rio</h2>
<?php
echo $this->Form->create('User');
	echo $this->Form->input('User.id', array('type' => 'hidden'));
	echo $this->Form->input('User.username', array('type' => 'text', 'label' => 'Usuário'));
	echo $this->Form->input('User.password', array('type' => 'password', 'label' => 'Senha'));
	echo $this->Form->input('User.role', array('type' => 'select', 'label' => 'Papel', 'options' => array('credenciador' => 'Credenciador', 'admin' => 'Administrador')));
echo $this->Form->end(array('label' => 'Salvar', 'data-type' => 'button', 'data-icon' => 'check', 'data-inline' => 'true'));
