<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index'))
		)
	));
$this->end();
?>
<div class="row">
	<div class="onecol"></div>
	<div class="fivecol">
		<h2 class="center">Bem-vindo</h2>
		<?php
			if ($edicao_info && $edicao_info['Edicao']['descricao']) {
				echo $edicao_info['Edicao']['descricao'];
			} else {
		?>
				<p>Seja bem-vindo ao s&iacute;tio do SAS.</p>
		<?php
			}
		?>
	</div>

<?php
foreach ($noticias as $key => $noticia):
	if ($key == 0):
?>
		<div class="fivecol">
			<h2 class="center">&Uacute;ltima Not&iacute;cia</h2>
<?php	
	endif;
?>
<?php
	if ($key == 1):
?>
	<div class="row">
		<div class="onecol"></div>
		<div class="tencol">
			<h2 class="center">Mais Not&iacute;cias</h2>
		</div>
		<div class="onecol last"></div>
	</div>
		
<?php
	endif;
?>
<?php if ($key != 0 && ($key % 2) != 0): ?>
	<div class="row">
		<div class="onecol"></div>
<?php endif; ?>
		<?php if ($key != 0): ?>
		<div class="fivecol">
		<?php endif; ?>
			<h3><?php echo $this->Html->link($noticia['Noticia']['titulo'], array('controller' => 'noticias', 'action' => 'ver', $noticia['Noticia']['id'])); ?></h3>
			<?php
				if (isset($noticia['Imagem']) && !empty($noticia['Imagem'])) {
					echo $this->Html->image('/files/uploads/'.$noticia['Imagem'][0]['arquivo'], array('alt' => $noticia['Imagem'][0]['titulo'], 'title' => $noticia['Imagem'][0]['titulo'],'class' => 'flutua img_noticia_home'));
				}
				echo $noticia['Noticia']['conteudo'];
				echo $this->Html->link('Leia a notícia completa', array('controller' => 'noticias', 'action' => 'ver', $noticia['Noticia']['id']));
			?>
		<?php if ($key != 0): ?>
		</div>
		<?php endif; ?>
<?php if ($key != 0 && (($key % 2) == 0 || $key == sizeof($noticias)-1)): ?>
		<div class="onecol last"></div>
	</div>
<?php endif; ?>
<?php
	if ($key == 0):
?>
	</div>
	<div class="onecol last"></div>
</div>
<?php
	endif;
?>
<?php endforeach; ?>
