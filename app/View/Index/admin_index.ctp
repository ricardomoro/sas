<h2>Bem-vindo Administrador!</h2>
<p>Utilize o menu superior para navegar no sistema.<br/>
Abaixo voc&ecirc; encontra algumas estat&iacute;sticas.</p>

<br/>

<p><strong>Número de Fotos: </strong><?php echo $fotos; ?> fotos em <?php echo $albuns; ?> &aacute;lbuns</p>
<p><strong>Número de Notícias: </strong><?php echo $noticias; ?> publicadas e <?php echo $noticias2; ?> não publicadas</p>
<p><strong>Número de Participantes Confirmados: </strong><?php echo $inscritos; ?></p>

<?php if ($inscricoes_por_dia): ?>
	
	<div id="chart_div" style="width: 900px; height: 500px;"></div>
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
	  google.load("visualization", "1", {packages:["corechart"]});
	  google.setOnLoadCallback(drawChart);
	  function drawChart() {
	    var data = google.visualization.arrayToDataTable([
	      ['Dia', 'Inscrições'],
		<?php
			foreach ($inscricoes_por_dia as $numero) {
				echo '["' . $this->Time->format('d/m/Y', $numero[0]['dia']) . '", ' . $numero[0]['count'] . '],';
			}
		?>
	    ]);
	
	    var options = {
	      title: 'Número de inscrições por dia',
	      backgroundColor: '#F9F9F9',
	    };
	
	    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
	    chart.draw(data, options);
	  }
	</script>
<?php endif; ?>