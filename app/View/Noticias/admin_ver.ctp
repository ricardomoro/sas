<h2><?php echo $noticia['Noticia']['titulo'] ?></h2>
<?php echo $noticia['Noticia']['conteudo'];?>

<?php if (isset($noticia['Imagem']) && !empty($noticia['Imagem'])): ?>
	<h3>Imagens</h3>
	<?php foreach ($noticia['Imagem'] as $key => $value): ?>
		<?php echo $this->Html->image('/files/uploads/'.$noticia['Imagem'][$key]['arquivo'], array('class' => 'imagem_mini', 'alt' => $noticia['Imagem'][$key]['titulo'])); ?>
	<?php endforeach; ?>
<?php endif; ?>

<?php if ($noticia['Noticia']['publicar'] == 'S'): ?>
	<p>Data de Publica&ccedil;&atilde;o: <?php echo $this->Time->format('d/m/Y', $noticia['Noticia']['publicado_em']); ?> às <?php echo $this->Time->format('H:i:s', $noticia['Noticia']['publicado_em']); ?></p>
<?php endif; ?>

<?php if ($noticia['Noticia']['publicar'] == 'N'): ?>
	<p>Não publicada.</p>
<?php endif; ?>

<p>Data de Cria&ccedil;&atilde;o: <?php echo $this->Time->format('d/m/Y', $noticia['Noticia']['created']); ?> às <?php echo $this->Time->format('H:i:s', $noticia['Noticia']['created']); ?></p>
<p>Data da &Uacute;ltima Modifica&ccedil;&atilde;o: <?php echo $this->Time->format('d/m/Y', $noticia['Noticia']['modified']); ?> às <?php echo $this->Time->format('H:i:s', $noticia['Noticia']['modified']); ?></p>
<?php echo $this->Html->link('Voltar', array('action' => 'index'), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true')); ?>
