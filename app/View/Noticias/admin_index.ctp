<?php echo $this->Html->script('admin_noticias', array('inline' => false)); ?>
<h2>Not&iacute;cias</h2>
<?php echo $this->Html->link('Nova Notícia', array('action' => 'nova'), array('data-role' => 'button', 'data-icon' => 'plus', 'data-inline' => 'true')) ?>
<br/>
<br/>
<table id="tabela_noticias">
	<thead>
		<tr>
			<th>Data de Publica&ccedil;&atilde;o</th>
			<th>T&iacute;tulo</th>
			<th>Publicada</th>
			<th>Data de Cria&ccedil;&atilde;o</th>
			<th>Data de Modifica&ccedil;&atilde;o</th>
			<th>A&ccedil;&otilde;es</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach ($noticias as $key => $noticia): ?>
		<tr>
			<td><?php echo ($noticia['Noticia']['publicado_em'] ? $this->Time->format('d/m/Y - H:i:s', $noticia['Noticia']['publicado_em']) : '-'); ?></td>
			<td><?php echo $this->Html->link($noticia['Noticia']['titulo'], array('action' => 'ver', $noticia['Noticia']['id'])); ?></td>
			<?php if ($noticia['Noticia']['publicar'] == 'S'): ?>
				<td class="center sim negrito">Sim</td>
			<?php endif; ?>
			<?php if ($noticia['Noticia']['publicar'] == 'N'): ?>
				<td class="center nao negrito">N&atilde;o</td>
			<?php endif; ?>
			<td><?php echo $this->Time->format('d/m/Y - H:i:s', $noticia['Noticia']['created']); ?></td>
			<td><?php echo $this->Time->format('d/m/Y - H:i:s', $noticia['Noticia']['modified']); ?></td>
			<td class="center">
				<?php echo $this->Html->link('Editar', array('action' => 'editar', $noticia['Noticia']['id']), array('data-role' => 'button', 'data-icon' => 'gear', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true')); ?>
				<?php echo $this->Html->link('Deletar', array('action' => 'deletar', $noticia['Noticia']['id']), array('data-role' => 'button', 'data-icon' => 'delete', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true'), 'Você tem certeza que deseja deletar essa notícia?'); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<script>
	$(document).ready(function() {
		$('#tabela_noticias').dataTable({
			"aaSorting" : [
				
			],
		});
	});
</script>