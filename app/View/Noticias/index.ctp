<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Notícias', array('controller' => 'noticias', 'action' => 'index')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Todas as Not&iacute;cias</h2>
	</div>
	<div class="onecol last"></div>
</div>
<?php if ($noticias) { ?>
	<?php foreach ($noticias as $key => $noticia): ?>
		<div class="row">
			<div class="onecol"></div>
			<div class="tencol">
				<h4><?php echo $this->Html->link($noticia['Noticia']['titulo'], array('controller' => 'noticias', 'action' => 'ver', $noticia['Noticia']['id'])); ?></h4>
				<?php
					echo $noticia['Noticia']['conteudo'];
					echo '<p class="nota">' . $this->Time->format('d/m/Y', $noticia['Noticia']['publicado_em']) . '</p>';
				?>
			</div>
			<div class="onecol last"></div>
		</div>
	<?php endforeach; ?>
<?php } else { ?>
	<p class="center">N&atilde;o existem not&iacute;cias publicadas até o momento.</p>
<?php } ?>
