<?php echo $this->Html->css('lightbox', null, array('inline' => false)); ?>
<?php echo $this->Html->script('jquery-ui-1.10.2.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.smooth-scroll.min', array('inline' => false)); ?>
<?php echo $this->Html->script('lightbox', array('inline' => false)); ?>
<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Notícias', array('controller' => 'noticias', 'action' => 'index')),
			$this->Html->link($noticia['Noticia']['titulo'], array('controller' => 'noticias', 'action' => 'ver', $noticia['Noticia']['id'])),

		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2><?php echo $noticia['Noticia']['titulo']; ?></h2>
		<p class="nota">Not&iacute;cia publicada no dia <?php echo $this->Time->format('d/m/Y', $noticia['Noticia']['publicado_em']); ?> &agrave;s <?php echo $this->Time->format('H:i', $noticia['Noticia']['publicado_em']); ?></p>
		<?php if (strtotime($noticia['Noticia']['modified']) > strtotime($noticia['Noticia']['publicado_em'])): ?>
			<p class="nota">Not&iacute;cia atualizada no dia <?php echo $this->Time->format('d/m/Y', $noticia['Noticia']['modified']); ?> &agrave;s <?php echo $this->Time->format('H:i', $noticia['Noticia']['modified']); ?></p>
		<?php endif;?>
		<?php
			if (isset($noticia['Imagem']) && !empty($noticia['Imagem']) && sizeof($noticia['Imagem']) >= 1) {
				$imagem = $this->Html->image('/files/uploads/'.$noticia['Imagem'][0]['arquivo'], array('alt' => $noticia['Imagem'][0]['titulo'], 'title' => $noticia['Imagem'][0]['titulo'], 'class' => 'flutua img_noticia'));
				echo $this->Html->link($imagem, '/files/uploads/'.$noticia['Imagem'][0]['arquivo'], array('rel' => 'lightbox[noticia]', 'title' => $noticia['Imagem'][0]['titulo'], 'escape' => false));
			}
			
			echo $noticia['Noticia']['conteudo'];
		?>
		<br class="clear" />
	</div>
	<div class="onecol last"></div>
</div>

<?php if (isset($noticia['Imagem']) && !empty($noticia['Imagem']) && sizeof($noticia['Imagem']) >= 2): ?>
	<br/>
	<br/>
	<br/>
	<div class="row">
		<div class="onecol"></div>
		<div class="tencol">
			<?php
				foreach ($noticia['Imagem'] as $key => $imagem) {
					if ($key == 0) {
						continue;
					}
					$imagem = $this->Html->image('/files/uploads/'.$noticia['Imagem'][$key]['arquivo'], array('alt' => $noticia['Imagem'][$key]['titulo'], 'title' => $noticia['Imagem'][$key]['titulo'], 'class' => 'img_noticia'));
					echo $this->Html->link($imagem, '/files/uploads/'.$noticia['Imagem'][$key]['arquivo'], array('rel' => 'lightbox[noticia]', 'title' => $noticia['Imagem'][$key]['titulo'], 'escape' => false));
				}
			?>
		</div>
		<div class="onecol last"></div>
	</div>
<?php endif; ?>