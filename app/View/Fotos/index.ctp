<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Fotos', array('controller' => 'fotos', 'action' => 'index')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Fotos</h2>
	</div>
	<div class="onecol last"></div>
</div>
<?php
	if ($albuns) {
		$i = 0;
?>
		<div class="row">
			<div class="onecol"></div>
<?php
		foreach ($albuns as $album):
			$i++;
?>
			<div class="threecol foto">
				<?php echo $this->Html->image('/img/generic2.png', array('alt' => $album['Album']['nome'], 'url' => array('action' => 'ver_album', $album['Album']['id']))); ?>
				<p><?php echo $this->Html->link($album['Album']['nome'], array('action' => 'ver_album', $album['Album']['id'])); ?></p>
			</div>
<?php
			if ($i == sizeof($albuns)) {
?>
			<div class="twocol last"></div>
		</div>
<?php
			} else if ($i % 3 == 0) {
?>
			<div class="twocol last"></div>
		</div>
		<div class="row">
			<div class="onecol"></div>
<?php
			}
		endforeach;
	} else {
?>
		<p>N&atilde;o existem fotos enviadas at&eacute; o momento.</p>
<?php
	}
?>