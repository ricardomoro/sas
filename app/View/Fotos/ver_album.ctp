<?php echo $this->Html->css('lightbox', null, array('inline' => false)); ?>
<?php echo $this->Html->script('jquery-ui-1.10.2.min', array('inline' => false)); ?>
<?php echo $this->Html->script('jquery.smooth-scroll.min', array('inline' => false)); ?>
<?php echo $this->Html->script('lightbox', array('inline' => false)); ?>
<?php
	$links = array(
		$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
		$this->Html->link('Fotos', array('controller' => 'fotos', 'action' => 'index')),
	);
	
	foreach ($caminho as $no) {
		$links[] = $this->Html->link($no['Album']['nome'], array('controller' => 'fotos', 'action' => 'ver_album', $no['Album']['id']));
	}
	
	$this->start('breadcrumb');
		echo $this->element('layout/breadcrumb', array('links' => $links));
	$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Fotos</h2>
		<h3><?php echo $album['Album']['nome']; ?></h3>
	</div>
	<div class="onecol last"></div>
</div>
<?php
	if ($filhos):
		$i = 0;
?>
		<div class="row">
			<div class="onecol"></div>
<?php
		foreach ($filhos as $filho):
			$i++;
?>
			<div class="threecol foto">
				<?php echo $this->Html->image('/img/generic2.png', array('alt' => $filho['Album']['nome'], 'url' => array('action' => 'ver_album', $filho['Album']['id']))); ?>
				<p><?php echo $this->Html->link($filho['Album']['nome'], array('action' => 'ver_album', $filho['Album']['id'])); ?></p>
			</div>
<?php
			if ($i == sizeof($filhos)) {
?>
			<div class="twocol last"></div>
		</div>
<?php
			} else if ($i % 3 == 0) {
?>
			<div class="twocol last"></div>
		</div>
		<div class="row">
			<div class="onecol"></div>
<?php
			}
		endforeach;
	endif;
?>

<?php
	if ($album['Foto']) {
		$j = 0;
?>
		<div class="row">
			<div class="onecol"></div>
<?php
		foreach ($album['Foto'] as $key => $value):
			$j++;
?>
			<div class="threecol foto">
				<a href="<?php echo '/files/uploads/'.$value['arquivo']; ?>" rel="lightbox[<?php echo $album['Album']['id']; ?>]" title="<?php echo $value['titulo'] . ($value['creditos'] ? ' / Créditos: ' . $value['creditos'] : ''); ?>">
					<img src="<?php echo '/files/uploads/'.$value['arquivo']; ?>" alt="<?php echo $value['descricao']; ?>" />
					<p><?php echo $value['titulo']; ?></p>
				</a>
			</div>
<?php
			if ($j == sizeof($album['Foto'])) {
?>
			<div class="twocol last"></div>
		</div>
<?php
			} else if ($j % 3 == 0) {
?>
			<div class="twocol last"></div>
		</div>
		<div class="row">
			<div class="onecol"></div>
<?php
			}
		endforeach;
	} else {
?>
		<div class="row">
			<div class="onecol"></div>
			<div class="threecol foto">
				<img src="/img/no_photo.png" alt="Sem fotos nesse álbum" />
				<p>N&atilde;o h&aacute; fotos nesse &aacute;lbum</p>
			</div>
			<div class="eightcol last"></div>
		</div>
<?php
	}
?>
