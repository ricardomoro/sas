<h2>Fotos</h2>
<h3><?php echo $album['Album']['nome']; ?></h3>
<?php
	if ($pai):
?>
	<?php echo $this->Html->link($pai['Album']['nome'], array('action' => 'ver_album', $pai['Album']['id']), array('data-role' => 'button', 'data-icon' => 'arrow-u', 'data-inline' => 'true', 'data-iconpos' => 'notext')); ?>
<?php
	endif;
?>
<?php echo $this->Html->link('Nova Foto', array('action' => 'nova_foto', 'album' => $album['Album']['id']), array('data-role' => 'button', 'data-icon' => 'plus', 'data-inline' => 'true')); ?>
<br/>
<?php
	if ($filhos):
		foreach ($filhos as $filho):
?>
			<div class="foto">
				<?php echo $this->Html->image('/img/generic.png', array('alt' => $filho['Album']['nome'], 'url' => array('action' => 'ver_album', $filho['Album']['id']))); ?>
				<p><?php echo $this->Html->link($filho['Album']['nome'], array('action' => 'ver_album', $filho['Album']['id'])); ?></p>
				
			</div>
<?php
		endforeach;
	endif;
?>

<?php
	if ($album['Foto']):
		foreach ($album['Foto'] as $key => $value):
?>
			<div class="foto">
				<?php echo $this->Html->image('/files/uploads/'.$value['arquivo'], array('alt' => $value['descricao'], 'title' => $value['descricao'])); ?>
				<p><?php echo $value['titulo']; ?></p>
				<?php if ($value['creditos']): ?>
					<p>Créditos: <?php echo $value['creditos']; ?></p>
				<?php endif; ?>
				<?php echo $this->Html->link('Deletar', array('action' => 'deletar_foto', $value['id'], 'album' => $value['album_id']), array('data-role' => 'button', 'data-icon' => 'delete', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true'), 'Você tem certeza que deseja deletar essa foto?'); ?>
			</div>
<?php
		endforeach;
	endif;
?>
<br class="clear" />
<br />
<?php echo $this->Html->link('Voltar', array('action' => 'index'), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true')); ?>