<h2>Fotos</h2>
<?php echo $this->Html->link('Novo Álbum', array('action' => 'novo_album'), array('data-role' => 'button', 'data-icon' => 'plus', 'data-inline' => 'true')) ?>
<h3>Lista de Álbuns</h3>
<table id="tabela_albums">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Álbum Pai</th>
			<th>Data de Cria&ccedil;&atilde;o</th>
			<th>A&ccedil;&otilde;es</th>
		</tr>
	</thead>
	<tbody>
<?php
	foreach ($albuns as $album):
?>
		<tr>
			<td><?php echo $this->Html->link($album['Album']['nome'], array('action' => 'ver_album', $album['Album']['id'])); ?></td>
			<td><?php echo ($album['pai'] ? $album['pai']['Album']['nome'] : '-'); ?></td>
			<td class="center"><?php echo $this->Time->format('d/m/Y - H:i:s', $album['Album']['created']); ?></td>
			<td class="center">
				<?php echo $this->Html->link('Editar', array('action' => 'editar_album', $album['Album']['id']), array('data-role' => 'button', 'data-icon' => 'gear', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true')); ?>
				<?php echo $this->Html->link('Deletar', array('action' => 'deletar_album', $album['Album']['id']), array('data-role' => 'button', 'data-icon' => 'delete', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true'), 'Você tem certeza que deseja deletar esse álbum?'); ?>
			</td>
		</tr>
<?php
	endforeach;
?>
	</tbody>
</table>
<script>
	$(document).ready(function() {
		$('#tabela_albums').dataTable({
			"aaSorting" : [],
		});
	});
</script>