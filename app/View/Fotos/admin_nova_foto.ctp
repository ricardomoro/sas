<h2>Nova Foto</h2>
<?php
	echo $this->Form->create('Foto', array('type' => 'file'));
		echo $this->element('forms/foto');
	echo $this->Form->end(array('label' => 'Salvar', 'data-icon' => 'check', 'data-inline' => 'true'));
	echo $this->Html->link('Voltar', array('action' => 'ver_album', $this->params['named']['album']), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true'));
