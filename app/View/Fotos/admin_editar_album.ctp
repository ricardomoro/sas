<h2>Editar &Aacute;lbum</h2>
<?php
	echo $this->Form->create('Album');
		echo $this->element('forms/album');
	echo $this->Form->end(array('label' => 'Salvar', 'data-icon' => 'check', 'data-inline' => 'true'));
	echo $this->Html->link('Voltar', array('action' => 'index'), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true'));
