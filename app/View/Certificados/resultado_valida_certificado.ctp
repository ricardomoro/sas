<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Certificados', array('controller' => 'certificados', 'action' => 'index')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<p class="center">Certificado v&aacute;lido para</p>
		<h2 class="center"><?php echo $participante['Participante']['nome_completo']; ?></h2>
		<p class="center">pela participa&ccedil;&atilde;o no <strong><?php echo $participante['Edicao']['numero'] ?>&ordm;</strong> SAS.</p>
		<p class="center">Com a carga hor&aacute;ria de <strong><?php echo $participante['Edicao']['carga_horaria']; ?>h</strong>, entre os dias <strong><?php echo $participante['Edicao']['inicio']; ?></strong> e <strong><?php echo $participante['Edicao']['fim']; ?></strong>.</p>
		<?php echo $this->Html->link('Voltar', array('action' => 'index')); ?>
	</div>
	<div class="onecol last"></div>
</div>
