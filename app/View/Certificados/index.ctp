<?php echo $this->Html->css('forms', null, array('inline' => false)); ?>
<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Certificados', array('controller' => 'certificados', 'action' => 'index')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Certificados</h2>
		<?php
			echo $this->Form->create('Certificado');
		?>
				<fieldset>
					<legend class="negrito">Valida&ccedil;&atilde;o</legend>
					<p>Para validar um certificado, informe o n&uacute;mero de registro (que se encontra no verso do certificado) no campo abaixo.</p>
		<?php
					echo $this->element('forms/valida_certificado');
		?>
				</fieldset>
		<?php
			echo $this->Form->end('Verificar');
		?>
		
		<?php
			echo $this->Form->create('Certificado');
		?>
				<fieldset>
					<legend class="negrito">Consulta</legend>
					<p>Para consultar e emitir um certificado de participação, insira sua matrícula SIAPE no campo abaixo.</p>
		<?php
					echo $this->element('forms/consulta_certificados');
		?>
				</fieldset>
		<?php
			echo $this->Form->end('Consultar');
		?>
	</div>
	<div class="onecol last"></div>
</div>