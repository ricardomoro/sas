<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Certificados', array('controller' => 'certificados', 'action' => 'index')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center"><?php echo $this->Html->link('Gerar certificado do ' . $participantes[0]['Edicao']['numero'] . 'º SAS para ' . $participantes[0]['Participante']['nome_completo'] . ' (formato PDF)', array('controller' => 'certificados', 'action' => 'gerar_certificado', $participantes[0]['Participante']['id']), array('class' => 'sublinhado')); ?></h2>
	</div>
	<div class="onecol last"></div>
</div>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h3 class="center">Pré-visualização do Certificado</h3>
		<div id="pre_cert">
			<?php echo $this->element('../Participantes/certificado'); ?>
		</div>
	</div>
	<div class="onecol last"></div>
</div>
<br />
<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<?php echo $this->Html->link('Voltar', array('action' => 'index')); ?>
	</div>
	<div class="onecol last"></div>
</div>