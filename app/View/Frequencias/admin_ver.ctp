<h2><?php echo $participante['Participante']['nome_completo']; ?></h2>
	<p><strong>Inscrição: </strong><?php echo $participante['Participante']['inscricao']; ?></p>
	<p><strong>SIAPE: </strong><?php echo $participante['Participante']['siape']; ?></p>
	<p><strong>Câmpus: </strong><?php echo $participante['Campus']['nome']; ?></p>
<?php if (!empty($participante['Frequencia'])) { ?>
<h3>Frequ&ecirc;ncias</h3>
	<table>
		<thead>
			<tr>
				<th>Data</th>
				<th>Hora</th>
				<th>Entrada/Sa&iacute;da</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach ($participante['Frequencia'] as $frequencia): ?>
			<tr>
				<td class="center"><?php echo $this->Time->format('d/m/Y', $frequencia['data_hora']); ?></td>
				<td class="center"><?php echo $this->Time->format('H:i:s', $frequencia['data_hora']); ?></td>
				<td class="center"><?php echo $frequencia['entrada_saida']; ?></td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>
<?php } else { ?>
	<p>Nenhuma frequ&ecirc;ncia registrada para esse participante.</p>
<?php } ?>
<?php echo $this->Html->link('Voltar', array('action' => 'index'), array('id' => 'voltar', 'data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true')); ?>
<script>
	$(document).ready(function(){
		$('#voltar').focus();
	});
</script>
