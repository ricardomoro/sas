<div class="center">
	<h2>Confirmação de Registro de Frequ&ecirc;ncia</h2>
	<h3><?php echo $participante['Participante']['inscricao']; ?></h3>
	<p><strong><?php echo $participante['Participante']['nome_completo']; ?></strong></p>
	<p><strong><?php echo $participante['Campus']['nome']; ?></strong></p>
	<?php
		echo $this->Form->create('Frequencia');
			echo $this->Form->input('etapa', array('type' => 'hidden', 'value' => '2'));
			echo $this->Form->input('Participante.id', array('type' => 'hidden', 'value' => $participante['Participante']['id']));
		echo $this->Form->end('Registrar');
		echo $this->Html->link('Voltar', array('action' => 'registrar'), array('id' => 'voltar', 'data-role' => 'button', 'data-icon' => 'back'));
	?>
</div>
<script>
	$(document).ready(function(){
		$('#FrequenciaAdminRegistrarForm input[type=submit]').focus();
	});
</script>