<h2>Registrar Frequ&ecirc;ncia</h2>
<?php
echo $this->Form->create('Frequencia');
	echo $this->Form->input('etapa', array('type' => 'hidden', 'value' => '1'));
	echo $this->Form->input('Participante.inscricao', array('type' => 'text', 'label' => 'Número da Inscrição do Participante'));
echo $this->Form->end('Enviar');
?>
<script>
	$(document).ready(function(){
		$('#ParticipanteInscricao').focus();
		$('#ParticipanteInscricao').select();
		$('#flashMessage.success').delay(4000).fadeOut(1000, function(){
			$(this).hide();
		});
	});
</script>