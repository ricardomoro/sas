<?php echo $this->Html->css('programacao', null, array('inline' => false)); ?>
<?php $this->set('title_for_layout', 'Programação'); ?>
<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Programação', array('controller' => 'pages', 'action' => 'programacao')),
		)
	));
$this->end();
?>
<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Programa&ccedil;&atilde;o</h2>
		<?php echo $this->element('programacao'); ?>
	</div>
	<div class="onecol last"></div>
</div>