<?php $this->set('title_for_layout', 'Regulamento'); ?>
<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Regulamento', array('controller' => 'pages', 'action' => 'regulamento')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Regulamento</h2>
		<p>O regulamento do SAS est&aacute; dispon&iacute;vel em dois formatos, DOC e PDF. Para baixar e visualizar o regulamento, escolha um dos links abaixo.</p>
		<br/>
		<a href="/files/regulamento.pdf">Regulamento em formato PDF (60 KB).</a>
		<br/>
		<a href="/files/regulamento.doc">Regulamento em formato DOC (83 KB).</a>
	</div>
	<div class="onecol last"></div>
</div>