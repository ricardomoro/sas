<?php echo $this->Html->script('admin_participantes', array('inline' => true)); ?>
<h2>Inscri&ccedil;&otilde;es</h2>
<?php echo $this->element('admin_participantes_filtros'); ?>
<table id="tabela_participantes">
	<thead>
		<tr>
			<th>N&uacute;mero</th>
			<th>Nome</th>
			<th>SIAPE</th>
			<th>C&acirc;mpus</th>
			<th>Confirmado?</th>
			<th>Participou?</th>
			<th>Certificado?</th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($participantes as $key => $participante): ?>
		<tr>
			<td><?php echo $participante['Participante']['inscricao']; ?></td>
			<td><?php echo $this->Html->link($participante['Participante']['nome_completo'], array('action' => 'ver', $participante['Participante']['id'])); ?></td>
			<td><?php echo $participante['Participante']['siape']; ?></td>
			<td><?php echo $participante['Campus']['nome']; ?></td>
			<td class="center"><?php echo $this->Html->link(($participante['Participante']['confirmado'] == 'S' ? 'Sim' : 'Não'), '#', array('data-role' => 'button', 'data-inline' => 'true', 'data-mini' => 'true', 'class' => 'toggle_confirmado '.($participante['Participante']['confirmado'] == 'S' ? 'sim' : 'nao'), 'participante_id' => $participante['Participante']['id'])); ?></td>
			<?php
				if (!isset($participante['Participante']['presente'])) {
			?>
					<td class="center">-</td>
			<?php
				} else if ($participante['Participante']['presente'] == 'S') {
			?>
					<td class="center sim">Sim</td>
			<?php
				} else {
			?>
					<td class="center nao">N&atilde;o</td>
			<?php
				}
			?>
			<?php
				if (!isset($participante['Participante']['presente']) || !$participante['Participante']['certificado']) {
			?>
					<td class="center">-</td>
			<?php
				} else if ($participante['Participante']['certificado'] == 'S') {
			?>
					<td class="center sim">Sim</td>
			<?php	
				} else {
			?>
					<td class="center nao">N&atilde;o</td>
			<?php
				}
			?>
		</tr>
	<?php endforeach; ?>
	</tbody>
</table>
<script>
	$(document).ready(function() {
		$('#tabela_participantes').dataTable({
			"aaSorting" : [
				[0, "desc"],
			],
		});
	});
</script>