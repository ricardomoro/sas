<head>
	<style>
		.cert table {
			margin-top: 2mm;
			margin-bottom: 4mm;
			width: 100%
		}

		.cert table td,
		.cert table th {
			border: 1px solid #000000;
		}
		
		.cert td {
			font-size: 12px;
		}

		.cert .arial {
			font-family: Arial, sans-serif;
		}

		.cert .bold {
			font-weight: bold;
		}

		.cert .subl {
			text-decoration: underline;
		}

		.cert .center {
			text-align: center;
		}

		.cert .espaco {
			margin: 15mm 0;
		}

		.cert .floatLeft {
			float: left;
		}

		.cert #topo {
			text-align: center;
		}

		.cert #topo img {
			width: 100px;
		}

		.cert #topo p {
			margin: 0;
		}

		.cert #conteudo #certi {
			margin: 15mm 0;
		}

		.cert #espacobaixo {
			margin-bottom: 30mm;
		}

		.cert #baixo {
			width: 50%
		}

		.cert #baixo img {
			width: 200px;
			margin: 0;
			padding: 0;
		}

		.cert #assinatura {
			width: 100%;
		}

		.cert #assinatura p {
			margin: 2mm 0;
		}
		
		.cert #assinatura .arial {
			border: 1px solid black;
		}

		.cert #programacao p {
			margin: 0mm;
			font-size: 8pt; 
		}

		.cert #programacao ol {
			margin: 0mm;
		}

		.cert #programacao li {
			margin: 0mm;
			font-size: 8pt;
		}
	</style>
</head>

<?php
foreach ($participantes as $participante):
?>
<!-- Início da primeira página -->
<body class="cert">
	<div id="topo">
		<img src="img/brasao.png" />
		<p>Minist&eacute;rio da Educa&ccedil;&atilde;o</p>
		<p>Secretaria de Educa&ccedil;&atilde;o Profissional e Tecnol&oacute;gica</p>
		<p>Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul</p>
		<p>Diretoria de Gest&atilde;o de Pessoas</p>
		<p>&nbsp;</p>
		<p>Departamento de Administra&ccedil;&atilde;o e Desenvolvimento de Pessoas</p>
	</div>
	<div id="conteudo" class="arial">
		<p class="bold center" id="certi">CERTIFICADO</p>
		<p class="center" id="certi">Evento: <strong><?php echo ($participante['Edicao'] ? $participante['Edicao']['numero'] . '&ordm; ': ''); ?>SAS - Semin&aacute;rio Anual dos Servidores do IFRS</strong></p>
		<p>Participante: <?php echo $participante['Participante']['nome_completo']; ?></p>
		<p>Local de realiza&ccedil;&atilde;o: Bento Gon&ccedil;alves</p>
		<p>Per&iacute;odo de realiza&ccedil;&atilde;o: <?php echo $participante['Edicao']['inicio'] ?> a <?php echo $participante['Edicao']['fim'] ?></p>
		<p id="espacobaixo">Carga hor&aacute;ria: <?php echo $participante['Edicao']['carga_horaria'] ?>h</p>
	 </div>
	<div id="assinatura" class="arial">
		<p class="center">Bento Gon&ccedil;alves, <?php echo $data; ?></p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<p>&nbsp;</p>
		<div class="center" style="width:100%;" id="baixo">
			<img src="img/assinatura_diretor.png" />
			<p>Luiz Vicente Koche Vieira</p>
			<p>Diretor DGP</p>
			<p>Portaria n&ordm; 18/2009</p>
		</div>
	</div>
</body>
<!-- Fim da primeira página -->
<!-- Início da segunda página -->
<body class="cert">
<?php
	// Número de Registro = edição do evento + número de inscrição.
	$registro = str_pad($edicao_atual, 2, "0", STR_PAD_LEFT) . str_pad($participante['Participante']['inscricao'], 8, "0", STR_PAD_LEFT);
?>
	<div id="assinatura">
		<p class="arial">Registro n&ordm; <?php echo $registro; ?></p>
		<p>Para verificar a autenticidade deste certificado, acesse: http://sas.ifrs.edu.br/certificados</p>
	</div>
</body>
<!-- Fim da segunda página -->
<?php
endforeach;
?>