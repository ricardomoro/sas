<?php echo $this->Html->css('programacao', null, array('inline' => false)); ?>

<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Inscrições', array('controller' => 'participantes', 'action' => 'nova_inscricao')),
		)
	));
$this->end();
?>

<?php if ($edicao_atual) { ?>

<?php if (strtotime(date('Y-m-d')) > strtotime(implode("-", array_reverse(explode("/", $edicao_info['Edicao']['encerramento_inscricoes']))))
		&& strtotime(date("Y-m-d")) < strtotime("-30 days", strtotime(implode("-", array_reverse(explode("/", $edicao_info['Edicao']['inicio'])))))) { ?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Inscrições</h2>
		<p>Inscrições encerradas! Em breve divulga&ccedil;&atilde;o da lista de participantes.</p>
	</div>
	<div class="onecol last"></div>
</div>
<?php } else if (strtotime(date("Y-m-d")) >= strtotime("-30 days", strtotime(implode("-", array_reverse(explode("/", $edicao_info['Edicao']['inicio'])))))
				&& strtotime(date("Y-m-d")) <= strtotime(implode("-", array_reverse(explode("/", $edicao_info['Edicao']['inicio']))))) { ?>
	<div class="row">
		<div class="onecol"></div>
		<div class="tencol">
			<h2 class="center">Divulga&ccedil;&atilde;o da Lista de Participantes</h2>
			<p>
				Verifique a lista abaixo com as inscri&ccedil;&otilde;es homologadas. Caso voc&ecirc; tenha realizado a inscri&ccedil;&atilde;o e seu nome n&atilde;o conste na lista, por favor, entrar em contato pelo e-mail: <a href="mailto:sas@ifrs.edu.br">sas@ifrs.edu.br</a>
			</p>
		</div>
		<div class="onecol last"></div>
	</div>
	<div class="row">
		<div class="onecol"></div>
		<div class="tencol">
			<?php foreach ($participantes as $campus => $value): ?>
				<h3 class="center"><?php echo $campus ?></h3>
				<?php if (sizeof($value) > 0) { ?>
					<table class="programacao">
						<thead>
							<tr>
								<th>N&uacute;mero de Inscri&ccedil;&atilde;o</th>
								<th>Nome do Participante</th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($value as $participante): ?>
							<tr>
								<td class="center"><?php echo $participante['Participante']['inscricao']; ?></td>
								<td><?php echo $participante['Participante']['nome_completo']; ?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				<?php } else { ?>
					<p class="center">N&atilde;o existem participantes homologados nesse c&acirc;mpus.</p>
				<?php } ?>
			<?php endforeach; ?>
		</div>
		<div class="onecol last"></div>
	</div>
</div>
<?php } else { ?>
	<div class="row">
		<div class="onecol"></div>
		<div class="tencol">
			<p>Aguarde mais informa&ccedil;&otilde;es.</p>
		</div>
		<div class="onecol last"></div>
	</div>
<?php } ?>

<?php } else { ?>
	<div class="row">
		<div class="onecol"></div>
		<div class="tencol">
			<p class="center">Aguarde o próximo SAS!</p>
		</div>
		<div class="onecol last"></div>
	</div>
<?php } ?>