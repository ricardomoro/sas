<?php echo $this->Html->css('forms', null, array('inline' => false)); ?>
<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Formulário de Inscrição', array('controller' => 'participantes', 'action' => 'nova_inscricao')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Formul&aacute;rio de Inscri&ccedil;&atilde;o</h2>
		<?php
			echo $this->Form->create('Participante');
				echo $this->element('forms/inscricao');
			echo $this->Form->end('Inscrever-se');
		?>
	</div>
	<div class="onecol last"></div>
</div>