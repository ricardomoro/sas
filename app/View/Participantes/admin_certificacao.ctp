<h2>Certifica&ccedil;&atilde;o</h2>
<p>
	Abaixo est&aacute; a lista de participantes, juntamente com suas frequ&ecirc;ncias registradas.
	Escolha se a frequ&ecirc;ncia de cada participante est&aacute; dentro do limite para certifica&ccedil;&atilde;o.
</p>
<p>
	<strong>Atenção: </strong>
	Participantes que n&atilde;o tiveram nenhuma frequ&ecirc;ncia registrada s&atilde;o marcados automaticamente com "N&atilde;o".
	Participantes que estiveram presentes com mais de 75% da carga hor&aacute;ria s&atilde;o marcados automaticamente com "Sim".
</p>
<p>
	<strong>Legenda:</strong> <span class="sim">Entrada</span> | <span class="nao">Sa&iacute;da</span>
</p>

<?php echo $this->Form->create('Participante'); ?>

<table id="tabela_certificacao">
	<thead>
		<tr>
			<th>Certificar?</th>
			<th>Inscri&ccedil;&atilde;o</th>
			<th>Nome Completo</th>
			<th>C&acirc;mpus</th>
			<th>Frequ&ecirc;ncias</th>
		</tr>
	</thead>
	<tbody>
		<?php
		if ($participantes) { 
			foreach ($participantes as $participante):
		?>
			<tr>
				<?php
					if ($participante['Participante']['presente'] == 'N') {
				?>
						<td><?php echo $this->Form->input($participante['Participante']['id'], array('type' => 'radio', 'legend' => false, 'options' => array('S' => 'Sim', 'N' => 'Não'), 'value' => 'N')) ?></td>
				<?php
					} else if ($participante['Participante']['presente'] == 'S' && $participante['Participante']['horas_presente'] >= ($edicao_info['Edicao']['carga_horaria'] * 0.75)) {
				?>
						<td><?php echo $this->Form->input($participante['Participante']['id'], array('type' => 'radio', 'legend' => false, 'options' => array('S' => 'Sim', 'N' => 'Não'), 'value' => 'S')) ?></td>
				<?php
					} else {
				?>
						<td><?php echo $this->Form->input($participante['Participante']['id'], array('type' => 'radio', 'legend' => false, 'options' => array('S' => 'Sim', 'N' => 'Não'))) ?></td>
				<?php
					}
				?>
				<td class="center"><?php echo $participante['Participante']['inscricao']; ?></td>
				<td><?php echo $participante['Participante']['nome_completo']; ?></td>
				<td><?php echo $participante['Campus']['nome']; ?></td>
				<td class="center">
					<?php foreach ($participante['Frequencia'] as $frequencia): ?>
						<?php echo $this->Time->format('d/m/Y', $frequencia['data_hora']); ?>
						&agrave;s
						<?php if ($frequencia['entrada_saida'] == 'E') { ?>
							<span class="sim">
						<?php } else { ?>
							<span class="nao">
						<?php } ?>
						<?php echo $this->Time->format('H:i:s', $frequencia['data_hora']); ?>
							</span>
						<br />
					<?php endforeach; ?>
				</td>
			</tr>
		<?php
			endforeach;
		} else {
		?>
			<tr>
				<td colspan="5" class="center">N&atilde;o existem participantes para certifica&ccedil;&atilde;o. Verifique a p&aacute;gina de certificados.</td>
			</tr>
		<?php
		}
		?>
	</tbody>
</table>

<?php if ($participantes): ?>
<?php echo $this->Form->end('Salvar'); ?>
<p class="center">Antes de trocar de p&aacute;gina, certifique-se de ter salvo suas escolhas. Caso contr&aacute;rio ser&atilde;o perdidas.</p>
<div class="center">
	<?php echo $this->Paginator->first('<< '); ?>
	<?php if ($this->Paginator->hasPrev()) echo $this->Paginator->prev('< ') . '|'; ?>
	<?php echo $this->Paginator->numbers(); ?>
	<?php if ($this->Paginator->hasNext()) echo '|' . $this->Paginator->next(' >'); ?>
	<?php echo $this->Paginator->last(' >>'); ?>
</div>
<?php endif; ?>