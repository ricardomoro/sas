<h2><?php echo $participante['Participante']['nome_completo']; ?></h2>
<p><strong>Número de Inscrição: </strong><?php echo $participante['Participante']['inscricao']; ?></p>
<p><strong>Nome para Crach&aacute;: </strong><?php echo $participante['Participante']['nome_cracha']; ?></p>
<p><strong>Email: </strong><?php echo $this->Html->link($participante['Participante']['email'], 'mailto:'.$participante['Participante']['email']); ?></p>
<p><strong>Matr&iacute;cula SIAPE: </strong><?php echo $participante['Participante']['siape']; ?></p>
<p><strong>C&acirc;mpus de Origem: </strong><?php echo $participante['Campus']['nome']; ?></p>
<p><strong>Necessidades especiais: </strong>
	<?php
	if ($participante['Participante']['necessidade_especial'] == NULL) {
		echo '-';
	} else {
		echo $participante['Participante']['necessidade_especial'];
	}
	?>
</p>
<p><strong>Restrições alimentares: </strong>
	<?php
	if ($participante['Participante']['restricao_alimentar'] == NULL) {
		echo '-';
	} else {
		echo $participante['Participante']['restricao_alimentar'];
	}
	?>
</p>
<p><strong>Inscrição realizada em: </strong><?php echo $this->Time->format('d/m/Y', $participante['Participante']['data']); ?> &agrave;s <?php echo $this->Time->format('H:i:s', $participante['Participante']['data']); ?></p>
<p><strong>Inscrição Confirmada: </strong>
	<?php
	if ($participante['Participante']['confirmado'] == 'S') {
	?>
		<span class="sim">Sim</span>
	<?php
	} else if ($participante['Participante']['confirmado'] == 'N') {
	?>
		<span class="nao">N&atilde;o</span>
	<?php
	}
	?>
	
</p>
<p><strong>Participa&ccedil;&atilde;o no Evento: </strong>
	<?php
		if (strtotime(date('Y-m-d')) <= strtotime(implode("-", array_reverse(explode("/", $edicao_info['Edicao']['fim']))))) {
	?>
			-
	<?php
		} else if (!empty($participante['Frequencia'])) {
	?>
			<span class="sim">Sim</span>
	<?php
		} else {
	?>
			<span class="nao">N&atilde;o</span>
	<?php
		}
	?>
</p>
<p><strong>Mais de 75% de presença: </strong>
	<?php
	if (!isset($participante['Participante']['presente']) || $participante['Participante']['presente'] == NULL) {
		echo '-';
	} else if ($participante['Participante']['presente'] == 'S') {
		echo 'Sim';
	} else if ($participante['Participante']['presente'] == 'N') {
		echo 'Não';
	}
	?>
</p>
<br />
<?php
	if ($participante['Participante']['atividade_id']):
?>
		<h3>Atividade Especial</h3>
		<p><?php echo $participante['Atividade']['nome']; ?></p>
		<br />
<?php
	endif;
?>
<?php
	if ($participante['Participante']['apresentacao_id']):
?>
		<h3>Apresenta&ccedil;&atilde;o Cultural</h3>
		<p><?php echo $participante['Apresentacao']['tipo']; ?></p>
		<br />
<?php
	endif;
?>
<?php
	if ($participante['Obra']['id']):
?>
<h3>Divulga&ccedil;&atilde;o de Obra Liter&aacute;ria Cient&iacute;fica</h3>
<p><strong>T&iacute;tulo:</strong>
	<?php
	if ($participante['Obra']['titulo']) {
		echo $participante['Obra']['titulo'];
	}
	?>
</p>
<p><strong>Tipo da Obra:</strong>
	<?php
	if ($participante['Obra']['tipo']) {
		echo $participante['Obra']['tipo'];
	}
	?>
</p>
<?php endif; ?>
<?php echo $this->Html->link('Voltar', array('action' => 'index'), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true')); ?>