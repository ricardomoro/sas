<?php
echo 'Inscrição,Nome Crachá,Nome Completo,Nome Reduzido,Câmpus' . "\n";
foreach ($participantes as $participante) {
	echo $participante['Participante']['inscricao'] . ',';
	echo $participante['Participante']['nome_cracha'] . ',';
	echo $participante['Participante']['nome_completo'] . ',';
	
	$nome_reduzido = explode(' ', $participante['Participante']['nome_completo']);
	if (sizeof($nome_reduzido) >= 2) {
		$nome_reduzido = implode(' ', array($nome_reduzido[0], end($nome_reduzido)));
	} else {
		$nome_reduzido = $nome_reduzido[0];
	}
	echo $nome_reduzido . ',';
	echo $participante['Campus']['nome'];
	echo "\n";
}
