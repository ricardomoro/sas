<?php echo $this->element('relatorios_css'); ?>
<body>
	<h1>Relat&oacute;rio de Participantes com Restri&ccedil;&otilde;es Alimentares</h1>
<?php
	if (sizeof($participantes) > 0) {
?>
		<h2>Participantes</h2>
		<table>
			<thead>
				<tr>
					<th>N&uacute;mero</th>
					<th>Nome</th>
					<th>Email</th>
					<th>Restri&ccedil;&atilde;o Alimentar</th>
				</tr>
			</thead>
			<tbody>
<?php
				foreach ($participantes as $key => $value):
?>
				<tr>
					<td class="center"><?php echo $value['Participante']['inscricao']; ?></td>
					<td><?php echo $value['Participante']['nome_completo']; ?></td>
					<td><?php echo $value['Participante']['email']; ?></td>
					<td><?php echo $value['Participante']['restricao_alimentar']; ?></td>
				</tr>	
<?php
				endforeach;
?>
			</tbody>
		</table>
		<p>N&uacute;mero de participantes com restri&ccedil;&otilde;es alimentares: <strong><?php echo sizeof($participantes); ?></strong></p>
<?php
	} else {
?>
		<p>N&atilde;o existem participantes com restri&ccedil;&otilde;es alimentares.</p>
<?php		
	}
?>
</body>