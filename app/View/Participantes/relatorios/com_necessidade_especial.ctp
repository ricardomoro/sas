<?php echo $this->element('relatorios_css'); ?>
<body>
	<h1>Relat&oacute;rio de Participantes com Necessidades Especiais</h1>
<?php
	if (sizeof($participantes) > 0) {
?>
		<h2>Participantes</h2>
		<table>
			<thead>
				<tr>
					<th>N&uacute;mero</th>
					<th>Nome</th>
					<th>Email</th>
					<th>Necessidade Especial</th>
				</tr>
			</thead>
			<tbody>
<?php
				foreach ($participantes as $key => $value):
?>
				<tr>
					<td class="center"><?php echo $value['Participante']['inscricao']; ?></td>
					<td><?php echo $value['Participante']['nome_completo']; ?></td>
					<td><?php echo $value['Participante']['email']; ?></td>
					<td><?php echo $value['Participante']['necessidade_especial']; ?></td>
				</tr>	
<?php
				endforeach;
?>
			</tbody>
		</table>
		<p>N&uacute;mero de participantes com necessidades especiais: <strong><?php echo sizeof($participantes); ?></strong></p>
<?php
	} else {
?>
		<p>N&atilde;o existem participantes com necessidades especiais.</p>
<?php		
	}
?>
</body>