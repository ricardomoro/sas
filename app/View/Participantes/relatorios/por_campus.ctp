<head>
	<?php echo $this->element('relatorios_css'); ?>
</head>
<body>
	<h1>Relat&oacute;rio de Participantes por C&acirc;mpus de Origem</h1>
<?php
	$i = 0;
	foreach ($participantes as $key => $value):
		if ($i != 0):
?>
			<body>
<?php
		endif;
?>
		<h2><?php echo $key ?></h2>
<?php
		if (sizeof($participantes[$key]) > 0) {
?>
			<table>
				<thead>
					<tr>
						<th>N&uacute;mero</th>
						<th>Nome</th>
						<th>Matr&iacute;cula SIAPE</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
<?php
					foreach ($value as $k => $v):
?>
					<tr>
						<td class="center"><?php echo $v['Participante']['inscricao']; ?></td>
						<td><?php echo $v['Participante']['nome_completo']; ?></td>
						<td class="center"><?php echo $v['Participante']['siape']; ?></td>
						<td><?php echo $v['Participante']['email']; ?></td>
					</tr>	
<?php
					endforeach;
?>
				</tbody>
			</table>
			<p>N&uacute;mero de participantes nesse c&acirc;mpus: <strong><?php echo sizeof($participantes[$key]); ?></strong></p>
<?php
		} else {
?>
			<p>N&atilde;o existem participantes nesse c&acirc;mpus.</p>
<?php		
		}
?>
</body>
<?php
		$i++;
	endforeach;
?>