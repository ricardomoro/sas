<?php echo $this->element('relatorios_css'); ?>
<body>
	<h1>Relat&oacute;rio de Participantes por Obra Cient&iacute;fica</h1>
<?php
		if (sizeof($obras) > 0) {
?>
			<h2>Obras Cient&iacute;ficas</h2>
			<table>
				<thead>
					<tr>
						<th>T&iacute;tulo</th>
						<th>Tipo de Publica&ccedil;&atilde;o</th>
						<th>Nome do Participante</th>
						<th>Email do Participante</th>
					</tr>
				</thead>
				<tbody>
<?php
					foreach ($obras as $key => $value):
?>
					<tr>
						<td><?php echo $value['Obra']['titulo']; ?></td>
						<td><?php echo $value['Obra']['tipo']; ?></td>
						<td><?php echo $value['Participante']['nome_completo']; ?></td>
						<td><?php echo $value['Participante']['email']; ?></td>
					</tr>	
<?php
					endforeach;
?>
				</tbody>
			</table>
			<p>N&uacute;mero de participantes com obra cient&iacute;fica: <strong><?php echo sizeof($obras); ?></strong></p>
<?php
		} else {
?>
			<p>N&atilde;o existem participantes com obra cient&iacute;fica.</p>
<?php		
		}
?>
</body>