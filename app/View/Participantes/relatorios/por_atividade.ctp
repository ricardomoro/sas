<?php echo $this->element('relatorios_css'); ?>
<body>
	<h1>Relat&oacute;rio de Participantes por Tipo de Atividade Especial</h1>
<?php
	foreach ($participantes as $key => $value):
?>
		<h2><?php echo $key ?></h2>
<?php
		if (sizeof($participantes[$key]) > 0) {
?>
			<table>
				<thead>
					<tr>
						<th>Nome</th>
						<th>Matr&iacute;cula SIAPE</th>
						<th>Email</th>
					</tr>
				</thead>
				<tbody>
<?php
					foreach ($value as $k => $v):
?>
					<tr>
						<td><?php echo $v['Participante']['nome_completo']; ?></td>
						<td class="center"><?php echo $v['Participante']['siape']; ?></td>
						<td><?php echo $v['Participante']['email']; ?></td>
					</tr>	
<?php
					endforeach;
?>
				</tbody>
			</table>
			<p>N&uacute;mero de participantes nesse tipo de atividade especial: <strong><?php echo sizeof($participantes[$key]); ?></strong></p>
<?php
		} else {
?>
			<p>N&atilde;o existem participantes com esse tipo de atividade especial.</p>
<?php		
		}
	endforeach;
?>
</body>