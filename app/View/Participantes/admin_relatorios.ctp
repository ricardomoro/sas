<h2>Relat&oacute;rios</h2>
<p>
	Abaixo &eacute; poss&iacute;vel gerar diversos relat&oacute;rios sobre os inscritos/participantes.
	Ao requisitar um relat&oacute;rio, o mesmo ser&aacute; baixado para seu computador.
</p>

<br />

<h3>Pré-evento</h3>
<ul data-role="listview">
	<li data-icon="grid"><?php echo $this->Html->link('Inscritos por câmpus de origem', array('action' => 'relatorios', 'tipo' => 'por_campus')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Inscritos por tipo de atividade especial', array('action' => 'relatorios', 'tipo' => 'por_atividade')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Inscritos por tipo de apresentação cultural', array('action' => 'relatorios', 'tipo' => 'por_apresentacao')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Inscritos por obra científica', array('action' => 'relatorios', 'tipo' => 'por_obra')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Inscritos com necessidade especial', array('action' => 'relatorios', 'tipo' => 'com_necessidade_especial')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Inscritos com restrição alimentar', array('action' => 'relatorios', 'tipo' => 'com_restricao_alimentar')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Lista de Emails dos Inscritos', array('action' => 'relatorios', 'tipo' => 'lista_emails')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Dados para Crachá', array('action' => 'relatorios', 'tipo' => 'dados_cracha')); ?></li>
</ul>

<br />

<h3>Pós-evento</h3>
<ul data-role="listview">
	<li data-icon="grid"><?php echo $this->Html->link('Lista de Emails dos Participantes Efetivos', array('action' => 'relatorios', 'tipo' => 'lista_emails_participantes')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Frequências', array('action' => 'relatorios', 'tipo' => 'frequencia')); ?></li>
	<li data-icon="grid"><?php echo $this->Html->link('Ausências', array('action' => 'relatorios', 'tipo' => 'faltas')); ?></li>
</ul>
