<h2>Gerar Certificados</h2>
<p>
	Abaixo &eacute; poss&iacute;vel ver a lista de participantes aptos a serem certificados.
	Tamb&eacute;m &eacute; poss&iacute;vel gerar os certificados individualmente ou para todos os participantes de uma unidade.
	Ao requisitar um ou mais certificados, os mesmos ser&atilde;o baixados para seu computador.
</p>

<br />

<h3>Gerar certificados por unidade</h3>
<div data-role="controlgroup">
<?php
	foreach ($campus as $value) {
		echo $this->Html->link($value['Campus']['nome'], array('action' => 'certificados', 'todos' => true, 'campus' => $value['Campus']['id']), array('data-role' => 'button'));
	}
?>
</div>

<br />

<h3>Gerar certificados individuais</h3>
<table id="tabela_certificados">
	<thead>
		<tr>
			<th>Inscri&ccedil;&atilde;o</th>
			<th>Nome</th>
			<th>Matr&iacute;cula SIAPE</th>
			<th>C&acirc;mpus</th>
			<th>Gerar Certificado</th>
		</tr>
	</thead>
	<tbody>
<?php
	foreach ($participantes as $key => $value):
?>
		<tr>
			<td class="center"><?php echo $value['Participante']['inscricao']; ?></td>
			<td><?php echo $value['Participante']['nome_completo']; ?></td>
			<td class="center"><?php echo $value['Participante']['siape']; ?></td>
			<td><?php echo $value['Campus']['nome']; ?></td>
			<td class="center"><?php echo $this->Html->link('Gerar certificado individual de ' . $value['Participante']['nome_completo'], array('action' => 'certificados', $value['Participante']['id']), array('data-role' => 'button', 'data-icon' => 'info', 'data-inline' => 'true', 'data-iconpos' => 'notext')); ?></td>
		</tr>
<?php	
	endforeach;
?>
	</tbody>
</table>

<script>
	$(document).ready(function() {
		$('#tabela_certificados').dataTable({
			"aaSorting" : [
				
			],
		});
	});
</script>