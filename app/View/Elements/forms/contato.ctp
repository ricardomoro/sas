<?php echo $this->element('forms/dica_campos_obrigatorios'); ?>
<?php
	echo $this->Form->input('Contato.id', array('type' => 'hidden'));
	echo $this->Form->input('Contato.nome', array('type' => 'text', 'label' => 'Nome'));
	echo $this->Form->input('Contato.email', array('type' => 'text', 'label' => 'Email'));
	echo $this->Form->input('Contato.assunto', array('type' => 'text', 'label' => 'Assunto'));
	echo $this->Form->input('Contato.mensagem', array('type' => 'textarea', 'label' => 'Mensagem'));
	