<div class="imagem">
<?php
	if (!isset($key)) {
		$key = explode(' ', microtime());
		$key = explode('.', $key[0]);
		$key = $key[1];
	}
	echo $this->Form->input('Imagem.'.$key.'.id', array('type' => 'hidden'));
	echo $this->Form->input('Imagem.'.$key.'.titulo', array('type' => 'text', 'label' => 'Título'));
	
	if (isset($this->data['Imagem'])) {
		if ($this->data['Imagem'][$key]['arquivo'] != null && !empty($this->data['Imagem'][$key]['arquivo'])) {
			if (!is_array($this->data['Imagem'][$key]['arquivo'])) {
				echo $this->Html->image('/files/uploads/'.$this->data['Imagem'][$key]['arquivo'], array('class' => 'imagem_mini', 'alt' => $this->data['Imagem'][$key]['titulo']));
			}
		} else {
			echo $this->Form->input('Imagem.'.$key.'.arquivo', array('type' => 'file', 'label' => 'Arquivo'));
		}
	} else {
		echo $this->Form->input('Imagem.'.$key.'.arquivo', array('type' => 'file', 'label' => 'Arquivo'));
	}
	
	echo $this->Html->link('Remover Imagem', '#', array('class' => 'remover_imagem', 'data-role' => 'button', 'data-icon' => 'plus', 'data-inline' => 'true', 'data-mini' => 'true', 'imagem_id' => (isset($this->data['Imagem']) ? $this->data['Imagem'][$key]['id'] : '')));
?>
</div>