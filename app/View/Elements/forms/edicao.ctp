<?php echo $this->Html->script('tiny_mce/jquery.tinymce', array('inline' => false)); ?>
<?php
echo $this->Form->input('Edicao.id', array('type' => 'hidden'));
echo $this->Form->input('Edicao.numero', array('type' => 'text', 'label' => 'Número'));
echo $this->Form->input('Edicao.abertura_inscricoes', array('type' => 'text', 'label' => 'Data de Abertura das Inscrições (dia/mês/ano)'));
echo $this->Form->input('Edicao.encerramento_inscricoes', array('type' => 'text', 'label' => 'Data de Encerramento das Inscrições (dia/mês/ano)'));
echo $this->Form->input('Edicao.inicio', array('type' => 'text', 'label' => 'Data de Início da Edição (dia/mês/ano)'));
echo $this->Form->input('Edicao.fim', array('type' => 'text', 'label' => 'Data de Fim da Edição (dia/mês/ano)'));
echo $this->Form->input('Edicao.carga_horaria', array('type' => 'text', 'label' => 'Carga Horária'));
echo $this->Form->input('Edicao.atual', array('type' => 'checkbox', 'label' => 'Edição Atual?', 'value' => '1'));
echo $this->Form->input('Edicao.titulo', array('type' => 'text', 'label' => 'Título'));
echo $this->Form->input('Edicao.descricao', array('type' => 'textarea', 'label' => 'Descrição (aparece na página inicial)', 'class' => 'tinymce'));
?>
<script>
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// General options
			language: "pt",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,cleanup",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap",
			//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : false,
			
			// Example content CSS (should be your site CSS)
			content_css : "/css/tinymce_content.css",
			
			// Drop lists for link/image/media/template dialogs
			//template_external_list_url : "lists/template_list.js",
			//external_link_list_url : "lists/link_list.js",
			//external_image_list_url : "lists/image_list.js",
			//media_external_list_url : "lists/media_list.js",
			
			// Replace values for the template plugin
			/*template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}*/
		});
	});
</script>