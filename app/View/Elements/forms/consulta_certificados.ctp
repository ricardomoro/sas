<?php
echo $this->Form->input('siape', array('type' => 'text', 'label' => 'Matrícula SIAPE'));
echo $this->Form->input('edicao', array('type' => 'select', 'label' => 'Edição do SAS', 'options' => $edicoes, 'empty' => 'Selecione...'));