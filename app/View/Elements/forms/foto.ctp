<?php
	echo $this->Form->input('Foto.id', array('type' => 'hidden'));
	echo $this->Form->input('Foto.album_id', array('type' => 'hidden', 'value' => $this->params['named']['album']));
	echo $this->Form->input('Foto.titulo', array('type' => 'text', 'label' => 'Título'));
	echo $this->Form->input('Foto.descricao', array('type' => 'text', 'label' => 'Descrição'));
	echo $this->Form->input('Foto.creditos', array('type' => 'text', 'label' => 'Créditos'));
	
	if (isset($this->data['Foto'])) {
		if (!empty($this->data['Foto']['arquivo'])) {
			if (!is_array($this->data['Foto']['arquivo'])) {
				echo $this->Html->image('/files/uploads/'.$this->data['Foto']['arquivo'], array('class' => 'imagem_mini', 'alt' => $this->data['Foto']['titulo']));
			}
			echo $this->Form->input('Foto.arquivo', array('type' => 'file'));
		} else {
			echo $this->Form->input('Foto.arquivo', array('type' => 'file', 'label' => 'Arquivo'));
		}
	} else {
		echo $this->Form->input('Foto.arquivo', array('type' => 'file', 'label' => 'Arquivo'));
	}