<?php echo $this->Html->script('admin_noticias', array('inline' => false)); ?>
<?php echo $this->Html->script('tiny_mce/jquery.tinymce', array('inline' => false)); ?>
<?php
	echo $this->Form->input('Noticia.id', array('type' => 'hidden'));
	echo $this->Form->input('Noticia.titulo', array('type' => 'text', 'label' => 'Título'));
	echo $this->Form->input('Noticia.conteudo', array('type' => 'textarea', 'label' => 'Conteúdo', 'class' => 'tinymce'));
	echo $this->Form->input('Noticia.publicar', array('type' => 'radio', 'legend' => 'Publicar?', 'options' => array('S' => 'Sim', 'N' => 'Não'), 'value' => (isset($this->data['Noticia']) ?  $this->data['Noticia']['publicar']: 'N')));
?>
<fieldset id="imagens">
	<legend>Imagens</legend>
	<?php echo $this->Html->link('Adicionar Imagem', '#', array('id' => 'adicionar_imagem', 'data-role' => 'button', 'data-icon' => 'plus', 'data-inline' => 'true', 'data-mini' => 'true')); ?>
<?php
	if (isset($this->data['Imagem'])) {
		foreach ($this->data['Imagem'] as $key => $value) {
			echo $this->element('forms/imagem', array('key' => $key));
		}
	}
?>
</fieldset>
<script>
	$().ready(function() {
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '/js/tiny_mce/tiny_mce.js',
			
			// General options
			language: "pt",
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
			
			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,cleanup",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap",
			//theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : false,
			
			// Example content CSS (should be your site CSS)
			content_css : "/css/tinymce_content.css",
			
			// Drop lists for link/image/media/template dialogs
			//template_external_list_url : "lists/template_list.js",
			//external_link_list_url : "lists/link_list.js",
			//external_image_list_url : "lists/image_list.js",
			//media_external_list_url : "lists/media_list.js",
			
			// Replace values for the template plugin
			/*template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}*/
		});
	});
</script>