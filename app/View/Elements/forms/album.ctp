<?php
	echo $this->Form->input('Album.id', array('type' => 'hidden'));
	echo $this->Form->input('Album.nome', array('type' => 'text', 'label' => 'Nome'));
	echo $this->Form->input('Album.parent_id', array('type' => 'select', 'label' => 'Álbum Pai', 'options' => $albuns, 'empty' => 'Nenhum'));
	