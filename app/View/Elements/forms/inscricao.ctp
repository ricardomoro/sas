<?php echo $this->Html->css('inscricao', null, array('inline' => false)); ?>
<?php echo $this->Html->script('inscricao', array('inline' => false)); ?>
<?php echo $this->element('forms/dica_campos_obrigatorios'); ?>
<?php
	echo $this->Form->input('Participante.id', array('type' => 'hidden'));
	echo $this->Form->input('Participante.nome_completo', array('type' => 'text', 'label' => 'Nome Completo'));
	echo $this->Form->input('Participante.nome_cracha', array('type' => 'text', 'label' => 'Nome para Crachá'));
	echo $this->Form->input('Participante.siape', array('type' => 'text', 'label' => 'Matrícula SIAPE'));
	echo $this->Form->input('Participante.email', array('type' => 'text', 'label' => 'Email Institucional'));
	echo $this->Form->input('Participante.campus_id', array('label' => 'Câmpus de Origem', 'empty' => 'Selecione...', 'options' => $campus));
	echo $this->Form->input('Participante.necessidade_especial', array('type' => 'text', 'label' => 'Você possui alguma necessidade especial? Se sim, qual?'));
	echo $this->Form->input('Participante.restricao_alimentar', array('type' => 'text', 'label' => 'Você possui alguma restrição alimentar? Se sim, qual?'));
	
	$link_limpa_atividade = $this->Html->link('Desmarcar opção selecionada', '#', array('id' => 'limpa_atividade'));
	echo $this->Form->input('Participante.atividade_id',array('type' => 'radio', 'legend' => 'Você gostaria de participar de uma atividade especial? Se sim, qual?', 'after' => $link_limpa_atividade, 'class' => 'radio_atividade', 'options' => $atividades));
	
	echo $this->Form->input('Participante.apresentacao_id', array('type' => 'select', 'label' => 'Você gostaria de fazer alguma apresentação cultural?', 'empty' => 'Nenhuma', 'options' => $apresentacoes));
?>
	<p>Voc&ecirc; teria interesse em divulgar ou lan&ccedil;ar uma obra liter&aacute;ria cient&iacute;fica?
	<a href="#" id="obra_toggle_sim">Sim</a> | <a href="#" id="obra_toggle_nao" class="negrito">N&atilde;o</a>
	</p>
	<fieldset id="obra">
		<legend>Dados da Obra</legend>
<?php
		echo $this->Form->input('Obra.titulo', array('type' => 'text', 'label' => 'Título da Obra'));
		echo $this->Form->input('Obra.tipo', array('type' => 'text', 'label' => 'Tipo de Publicação (Livro, revista, etc.)'));
?>
	</fieldset>

<?php
	$link_regulamento = $this->Html->link('Regulamento', array('controller' => 'pages', 'action' => 'regulamento'), array('target' => '_blank'));
	echo "<br/>";
	echo $this->Form->input('aceito', array('type' => 'checkbox', 'label' => 'Li e aceito os termos presentes no ' . $link_regulamento));
?>