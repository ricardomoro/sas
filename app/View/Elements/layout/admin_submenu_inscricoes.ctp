<div data-role="navbar">
	<ul>
		<li><?php echo $this->Html->link('Relatórios', array('controller' => 'participantes', 'action' => 'relatorios')); ?></li>
		<li><?php echo $this->Html->link('Certificação', array('controller' => 'participantes', 'action' => 'certificacao')); ?></li>
		<li><?php echo $this->Html->link('Certificados', array('controller' => 'participantes', 'action' => 'certificados')); ?></li>
	</ul>
</div>