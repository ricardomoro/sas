<table class="programacao">
	<caption>Dia 17/04 (Quarta-feira)</caption>
	<thead>
		<tr>
			<th>Hor&aacute;rio</th>
			<th>Atividade</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="center">A partir das 17:30</td>
			<td>Credenciamento</td>
		</tr>
		<tr>
			<td class="center">19:30</td>
			<td>Solenidade de Abertura</td>
		</tr>
		<tr>
			<td class="center">21:00</td>
			<td>
				Jantar de Confraterniza&ccedil;&atilde;o<br/>
				Apresenta&ccedil;&otilde;es Culturais dos Servidores
			</td>
		</tr>
	</tbody>
</table>
<table class="programacao">
	<caption>Dia 18/04 (Quinta-feira)</caption>
	<thead>
		<tr>
			<th>Hor&aacute;rio</th>
			<th>Atividade</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="center">08:00</td>
			<td>
				<strong>Palestra:</strong> IFRS: Identidade, Protagonismo e Transforma&ccedil;&atilde;o Social<br/>
				<strong>Palestrante:</strong> M&aacute;rcia Amaral Corr&ecirc;a de Moraes <em>(IFRS POA)</em>
			</td>
		</tr>
		<tr>
			<td class="center">10:00</td>
			<td>Coffee Break</td>
		</tr>
		<tr>
			<td class="center">10:15</td>
			<td>
				<strong>Palestra: </strong>Compet&ecirc;ncias Essenciais na Administra&ccedil;&atilde;o P&uacute;blica Federal<br/>
				<strong>Palestrante: </strong>Maria J&uacute;lia Pantoja Britto <em>(UnB)</em>
			</td>
		</tr>
		<tr>
			<td class="center">12:15</td>
			<td>Almo&ccedil;o</td>
		</tr>
		<tr>
			<td class="center">13:45</td>
			<td>
				<strong>Mesa: </strong>Possibilidades e Desafios de uma Institui&ccedil;&atilde;o Multic&acirc;mpus: Como Construir a Unidade?<br/>
				<strong>Coordenador: </strong>Osvaldo Casares Pinto <em>(IFRS Reitoria)</em><br/>
				<strong>Participantes: </strong>Ulrika Arms <em>(Unipampa)</em>, Paulo Rog&eacute;rio Ara&uacute;jo Guimar&atilde;es <em>(IF Sudeste de Minas)</em>, Mariano Nicolao <em>(IFRS Canoas)</em>, Jos&eacute; Eli Santos dos Santos <em>(IFRS Rio Grande)</em>.<br/>
				<br/>
				<strong>Mesa: </strong>Pesquisa, P&oacute;s-gradua&ccedil;&atilde;o e Internacionaliza&ccedil;&atilde;o do IFRS: Experi&ecirc;ncias, Possibilidades e Desafios<br/>
				<strong>Coordenador: </strong>Julio Xandro Heck <em>(IFRS Reitoria)</em><br/>
				<strong>Participantes: </strong>Evandro Manara Miletto <em>(IFRS POA)</em>, Fl&aacute;via Santos Twardowski Pinto <em>(IFRS Os&oacute;rio)</em>, Maria Cristina Caminha de Castilhos Fran&ccedil;a <em>(IFRS POA)</em>, Giovani Giotto <em>(Aluno do Curso Superior de Enologia do C&acirc;mpus Bento Gon&ccedil;alves)</em>.<br/>
				<br/>
				<strong>Mesa: </strong>As Pol&iacute;ticas de Inclus&atilde;o e a Extens&atilde;o no IFRS: Experi&ecirc;ncias, Possibilidades e Desafios<br/>
				<strong>Coordenador: </strong>Viviane Silva Ramos <em>(IFRS Reitoria)</em><br/>
				<strong>Participantes: </strong>Carla Regina Andr&eacute; Silva <em>(IFRS Rio Grande)</em>, Olavo Ramalho Marques <em>(IFRS Caxias do Sul)</em>, Cibele Schwanke <em>(IFRS POA)</em>, Cristiane C&acirc;mara <em>(IFRS Erechim)</em>.<br/>
				<br/>
				<strong>Mesa: </strong>O Trabalho como Viv&ecirc;ncia e a Institui&ccedil;&atilde;o que Queremos<br/>
				<strong>Coordenador: </strong>Amilton Moura Figueiredo <em>(IFRS Reitoria)</em><br/>
				<strong>Participantes: </strong>Adriana de Farias Ramos <em>(IFRS POA)</em>, Rudinei Muller <em>(IFRS Bento Gon&ccedil;alves)</em>, Marcelo Rauh Schmitt <em>(IFRS POA)</em>, F&aacute;bio Azambuja Mar&ccedil;al <em>(IFRS Restinga)</em>.<br/>
			</td>
		</tr>
		<tr>
			<td class="center">16:00</td>
			<td>Coffee Break</td>
		</tr>
		<tr>
			<td class="center">16:15</td>
			<td>Continuidade dos Trabalhos das Mesas</td>
		</tr>
		<tr>
			<td class="center">18:30</td>
			<td>Encerramento dos Trabalhos <em>(Previs&atilde;o)</em></td>
		</tr>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2" class="center">NOITE LIVRE</td>
		</tr>
	</tfoot>
</table>
<table class="programacao">
	<caption>Dia 19/04 (Sexta-feira)</caption>
	<thead>
		<tr>
			<th>Hor&aacute;rio</th>
			<th>Atividade</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="center">08:00</td>
			<td>
				Vivendo IFRS<br/>
				<em>Visita aos Estandes</em><br/>
				<em>Apresenta&ccedil;&otilde;es Culturais</em><br/>
				<em>Lan&ccedil;amentos de Livros</em>
			</td>
		</tr>
		<tr>
			<td class="center">12:00</td>
			<td>Almo&ccedil;o</td>
		</tr>
		<tr>
			<td class="center">13:30</td>
			<td>Plen&aacute;ria para Sociliza&ccedil;&atilde;o das Conclus&otilde;es das Mesas Tem&aacute;ticas</td>
		</tr>
		<tr>
			<td class="center">16:30</td>
			<td>
				Encerramento <em>(Previs&atilde;o)</em><br/>
				Coffee Break
			</td>
		</tr>
	</tbody>
</table>