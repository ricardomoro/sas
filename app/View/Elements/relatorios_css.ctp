<style>
	* {
		font-family: sans-serif;
	}
	
	h1 {
		text-align: center;
	}
	
	/* Tabelas */
	table {
		width: 100%;
		border-collapse: collapse;
		margin-bottom: 7px;
	}
	
	table td,
	table th {
		border: 1px solid #A0A0A0;
		padding: 5px 10px;
	}
	
	table th {
		background-color: #DDDDDD;
	}
	
	.center {
		text-align: center;
	}
	
	.sim {
	color: green !important;
	}
	
	.nao {
		color: red !important;
	}
</style>