<a href="#" class="filtros">Filtros</a>
<div id="filtros">
	<?php echo $this->Form->create('Participante', array('url' => array('controller' => 'participantes', 'action' => 'index'))); ?>
	<div class="filtro">
		<?php echo $this->Form->input('filtro_campus', array('label' => 'Por Câmpus: ', 'data-enhance' => 'false', 'options' => $campus, 'empty' => 'Todos')); ?>
	</div>
	<div class="filtro">
		<?php echo $this->Form->input('filtro_atividade', array('label' => 'Por Atividade: ', 'data-enhance' => 'false', 'options' => $atividades, 'empty' => 'Todas')); ?>
	</div>
	<div class="filtro">
		<?php echo $this->Form->input('filtro_apresentacao', array('label' => 'Por Apresentação: ', 'data-enhance' => 'false', 'options' => $apresentacoes, 'empty' => 'Todas')); ?>
	</div>
	<div class="filtro">
		<?php echo $this->Form->input('filtro_obra', array('label' => 'Por Obra Científica: ', 'data-enhance' => 'false', 'options' => array('C' => 'Com Obra', 'S' => 'Sem Obra'), 'empty' => 'Todas')); ?>
	</div>
	
	<br class="clear"/>
	
	<div class="filtro">
		<?php echo $this->Form->input('filtro_necessidade_especial', array('label' => 'Por Necessidades Especiais: ', 'data-enhance' => 'false', 'options' => array('1' => 'Possui'), 'empty' => 'Todos')); ?>
	</div>
	<div class="filtro">
		<?php echo $this->Form->input('filtro_restricao_alimentar', array('label' => 'Por Restrições Alimentares: ', 'data-enhance' => 'false', 'options' => array('1' => 'Possui'), 'empty' => 'Todos')); ?>
	</div>
	
	<br class="clear"/>
	<div class="filtro">
		<?php echo $this->Form->end(array('label' => 'Filtrar', 'id' => 'filtro_submit', 'data-enhance' => 'false')); ?>
	</div>
</div>
