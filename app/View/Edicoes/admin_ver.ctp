<h2><?php echo $edicao['Edicao']['numero']; ?>&ordf; Edi&ccedil;&atilde;o</h2>
<p><strong>Data de Abertura das Inscri&ccedil;&otilde;es: </strong><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['abertura_inscricoes']))); ?></p>
<p><strong>Data de Encerramento das Inscri&ccedil;&otilde;es: </strong><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['encerramento_inscricoes']))); ?></p>
<p><strong>Data de In&iacute;cio da Edi&ccedil;&atilde;o: </strong><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['inicio']))); ?></p>
<p><strong>Data de T&eacute;rmino da Edi&ccedil;&atilde;o: </strong><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['fim']))); ?></p>
<p><strong>Carga Hor&aacute;ria: </strong><?php echo $edicao['Edicao']['carga_horaria']; ?>h</p>
<p><strong>T&iacute;tulo da Edi&ccedil;&atilde;o: </strong><?php echo ($edicao['Edicao']['titulo'] ? $edicao['Edicao']['titulo'] : '-'); ?></p>
<p><strong>Descri&ccedil;&atilde;o da Edi&ccedil;&atilde;o: </strong><?php echo ($edicao['Edicao']['descricao'] ? $edicao['Edicao']['descricao'] : '-'); ?></p>
<p><strong>Edi&ccedil;&atilde;o Atual? </strong>
	<?php if ($edicao['Edicao']['atual'] == '0'): ?>
		<span class="nao negrito">N&atilde;o</span>
	<?php endif; ?>
	<?php if ($edicao['Edicao']['atual'] == '1'): ?>
		<span class="sim negrito">Sim</span>
	<?php endif; ?>
</p>
<?php echo $this->Html->link('Voltar', array('action' => 'index'), array('data-role' => 'button', 'data-icon' => 'back', 'data-inline' => 'true')); ?>