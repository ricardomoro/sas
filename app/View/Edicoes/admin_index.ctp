<h2>Edi&ccedil;&otilde;es</h2>
<?php echo $this->Html->link('Nova Edição', array('action' => 'nova'), array('data-role' => 'button', 'data-icon' => 'plus', 'data-inline' => 'true')) ?>
<br/>
<br/>
<table id="tabela_edicoes">
	<thead>
		<tr>
			<th>N&uacute;mero</th>
			<th>Abertura das Inscri&ccedil;&otilde;es</th>
			<th>Encerramento das Inscri&ccedil;&otilde;es</th>
			<th>In&iacute;cio da Edi&ccedil;&atilde;o</th>
			<th>Fim da Edi&ccedil;&atilde;o</th>
			<th>Edi&ccedil;&atilde;o Atual?</th>
			<th>A&ccedil;&otilde;es</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ($edicoes as $key => $edicao): ?>
			<tr>
				<td class="center"><?php echo $this->Html->link($edicao['Edicao']['numero'], array('action' => 'ver', $edicao['Edicao']['id'])); ?></td>
				<td><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['abertura_inscricoes']))); ?></td>
				<td><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['encerramento_inscricoes']))); ?></td>
				<td><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['inicio']))); ?></td>
				<td><?php echo implode("/", array_reverse(explode("-", $edicao['Edicao']['fim']))); ?></td>
				<?php if ($edicao['Edicao']['atual'] == '0'): ?>
					<td class="center nao negrito">N&atilde;o</td>
				<?php endif; ?>
				<?php if ($edicao['Edicao']['atual'] == '1'): ?>
					<td class="center sim negrito">Sim</td>
				<?php endif; ?>
				<td class="center">
					<?php echo $this->Html->link('Editar', array('action' => 'editar', $edicao['Edicao']['id']), array('data-role' => 'button', 'data-icon' => 'gear', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true')); ?>
					<?php echo $this->Html->link('Deletar', array('action' => 'deletar', $edicao['Edicao']['id']), array('data-role' => 'button', 'data-icon' => 'delete', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true'), 'Você tem certeza que deseja deletar essa edição?'); ?>
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<script>
	$(document).ready(function() {
		$('#tabela_edicoes').dataTable({
			"aaSorting" : [
				[0, "desc"],
			],
		});
	});
</script>