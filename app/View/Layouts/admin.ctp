<?php echo $this->Html->docType('html5'); ?>
<html lang="pt-BR">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo ($edicao_atual ? $edicao_atual . '&ordm; ': ''); ?>SAS - Administra&ccedil;&atilde;o
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->Html->meta(array('name' => 'robots', 'content' => 'noindex,nofollow'));
		
		echo $this->fetch('meta');
		
		echo $this->Html->css('admin');
		echo $this->Html->css('jquery.mobile-1.3.1.min');
		echo $this->Html->css('jquery.mobile.theme-1.3.1.min');
		echo $this->Html->css('jquery.mobile.structure-1.3.1.min');
		echo $this->Html->css('jquery.dataTables');
		echo $this->Html->css('mensagens');
		echo $this->fetch('css');
		
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('jquery-migrate-1.1.1.min');
		echo $this->Html->script('jquery.mobile.defaults');
		echo $this->Html->script('jquery.mobile-1.3.1.min');
		echo $this->Html->script('jquery.dataTables.min');
		echo $this->Html->script('jquery.dataTables.settings');
		echo $this->fetch('script');
	?>
</head>
<body>
	<div data-role="page"> 
		<div data-role="header">
			<?php echo $this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index'), array('data-icon' => 'home', 'data-iconpos' => 'notext')); ?>
			<h1><?php echo ($edicao_atual ? $edicao_atual . '&ordm; ': ''); ?>SAS - Administra&ccedil;&atilde;o</h1>
			<?php
				if ($this->Session->read('Auth.User')):
			?>
					<a href="#popupMenu" data-rel="popup" data-role="button" data-icon="arrow-d" data-iconpos="left" data-inline="true" data-transition="fade">Opções</a>
					<div data-role="popup" id="popupMenu" data-overlay-theme="a">
						<ul data-role="listview" data-inset="true" data-theme="a">
						<?php
							if ($this->Session->read('Auth.User.role') == 'admin'):
						?>
								<li data-icon="gear"><?php echo $this->Html->link('Trocar Senha', array('controller' => 'users', 'action' => 'password')); ?></li>
						<?php
							endif;
						?>
							<li data-icon="delete" data-iconpos="left"><?php echo $this->Html->link('Sair', array('controller' => 'users', 'action' => 'logout')); ?></li>
						</ul>
					</div>
			<?php
				endif;
			?>
			<?php
				if ($this->Session->read('Auth.User')):
					if ($this->Session->read('Auth.User.role') == 'admin') {
			?>
					<div data-role="navbar">
						<ul>
							<li><?php echo $this->Html->link('Edições', array('controller' => 'edicoes', 'action' => 'index')); ?></li>
							<li><?php echo $this->Html->link('Inscrições', array('controller' => 'participantes', 'action' => 'index')); ?></li>
							<li><?php echo $this->Html->link('Notícias', array('controller' => 'noticias', 'action' => 'index')); ?></li>
							<li><?php echo $this->Html->link('Fotos', array('controller' => 'fotos', 'action' => 'index')); ?></li>
							<li><?php echo $this->Html->link('Contatos', array('controller' => 'contatos', 'action' => 'index')); ?></li>
						</ul>
					</div>
			<?php
				} else if ($this->Session->read('Auth.User.role') == 'credenciador') {
			?>
					<div data-role="navbar">
						<ul>
							<li><?php echo $this->Html->link('Consultar Frequências', array('controller' => 'frequencias', 'action' => 'index')); ?></li>
							<li><?php echo $this->Html->link('Registrar Frequência', array('controller' => 'frequencias', 'action' => 'registrar')); ?></li>
						</ul>
					</div>
			<?php
				}
				endif;
			?>
		</div>
		<?php
			if ($this->request->controller == 'participantes') {
				if ($this->request->action == 'admin_index' || $this->request->action == 'admin_relatorios' || $this->request->action == 'admin_certificacao' || $this->request->action == 'admin_certificados') {
					echo $this->element('layout/admin_submenu_inscricoes');
				}
			}
		?>
		<div data-role="content">
			<?php echo $this->Session->flash(); ?>
			
			<?php echo $this->fetch('content'); ?>
		</div> 
		<div data-role="footer">
			<p>Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul</p>
			<?php echo $this->element('sql_dump'); ?>
		</div> 
	</div>
</body>
</html>