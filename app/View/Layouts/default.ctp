<?php echo $this->Html->docType('xhtml-strict'); ?>
<html lang="pt-BR">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo ($edicao_atual ? $edicao_atual . '&ordm; ': ''); ?>SAS - <?php echo $title_for_layout; ?>
	</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->Html->meta(array('name' => 'author', 'content' => 'Ricardo Moro'));
		echo $this->Html->meta(array('name' => 'description', 'content' => 'Site do Seminário Anual dos Serviores do IFRS'));
		echo $this->Html->meta(array('name' => 'keywords', 'content' => 'SAS, seminário, servidores, IFRS, evento, site, sítio'));
		echo $this->Html->meta(array('name' => 'reply-to', 'content' => 'sas@ifrs.edu.br'));
		echo $this->Html->meta(array('name' => 'robots', 'content' => 'index,follow'));
		
		echo $this->fetch('meta');
		
		echo $this->Html->css('1140');
		echo $this->Html->css('ie');
		echo $this->Html->css('principal');
		echo $this->Html->css('mensagens');
		echo $this->fetch('css');
		
		echo $this->Html->script('css3-mediaqueries');
		echo $this->Html->script('jquery-1.8.3.min');
		echo $this->fetch('script');
	?>
</head>
<body>
	<?php echo $this->element('layout/barra_governo'); ?>
	<div id="header" class="container">
		<div class="row">
			<div class="twelvecol">
				<h1><a href="/" title="Página Inicial"><img src="/img/topo.jpg" alt="2º Seminário Anual dos Servidores do IFRS" /></a></h1>
			</div>
		</div>
	</div>
	<div id="menu" class="container">
		<div class="row">
			<div class="twelvecol">
				<a id="inicioMenu" href="#inicioMenu" class="oculto" accesskey="1">In&iacute;cio do menu</a>
				<ul>
					<li class="programacao primeiro"><?php echo $this->Html->link('Programação', array('prefix' => false, 'controller' => 'pages', 'action' => 'programacao')); ?></li>
					<li class="inscricoes"><?php echo $this->Html->link('Inscrições', array('prefix' => false, 'controller' => 'participantes', 'action' => 'index')); ?></li>
					<li class="certificados"><?php echo $this->Html->link('Certificados', array('prefix' => false, 'controller' => 'certificados', 'action' => 'index')); ?></li>
					<li class="noticias"><?php echo $this->Html->link('Notícias', array('prefix' => false, 'controller' => 'noticias', 'action' => 'index')); ?></li>
					<li class="fotos"><?php echo $this->Html->link('Fotos', array('prefix' => false, 'controller' => 'fotos', 'action' => 'index')); ?></li>
					<li class="regulamento"><?php echo $this->Html->link('Regulamento', array('prefix' => false, 'controller' => 'pages', 'action' => 'regulamento')); ?></li>
					<li class="contato ultimo"><?php echo $this->Html->link('Contato', array('prefix' => false, 'controller' => 'contatos', 'action' => 'index')); ?></li>
				</ul>
				<a href="#" class="oculto">Fim do menu</a>
			</div>
		</div>
	</div>
	<div id="breadcrumb" class="container">
		<div class="row">
			<div class="onecol"></div>
			<div class="tencol">
				<?php echo $this->fetch('breadcrumb'); ?>
			</div>
			<div class="onecol last"></div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="onecol"></div>
			<div class="tencol">
				<?php echo $this->Session->flash(); ?>
			</div>
			<div class="onecol last"></div>
		</div>
	</div>
	<div id="content" class="container">
		<a id="inicioConteudo" href="#inicioConteudo" class="oculto" accesskey="2">In&iacute;cio do conte&uacute;do</a>
		<?php echo $this->fetch('content'); ?>
		<a href="#" class="oculto">Fim do conte&uacute;do</a>
	</div>
	<div id="footer" class="container">
		<div class="row">
			<div class="twelvecol">
				<div id="voltarTopo">
					<a href="#barra_governo">Topo da p&aacute;gina</a>
				</div>
				<p class="titulo">Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul - IFRS</p>
				<address>Rua General Os&oacute;rio, 348 | Bairro Centro | CEP: 95700-000 | Bento Gon&ccedil;alves/RS</address>
				<p>E-mail: <a href="mailto:sas@ifrs.edu.br">sas@ifrs.edu.br</a></p>
				<p class="copyright"><a href="https://bitbucket.org/ricardomoro/sas/">Código-fonte deste sítio sob a licença GPL v2</a></p>
			</div>
		</div>
	</div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>