<?php echo $this->Html->docType('html5'); ?>
<html lang="pt-BR">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo ($edicao_atual ? $edicao_atual . '&ordm; ': ''); ?>SAS - <?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		
		echo $this->Html->meta(array('name' => 'author', 'content' => 'Ricardo Moro'));
		echo $this->Html->meta(array('name' => 'description', 'content' => 'Site do 2º Seminário Anual dos Serviores do IFRS'));
		echo $this->Html->meta(array('name' => 'keywords', 'content' => 'SAS, seminário, servidores, IFRS, evento, II SAS, 2º SAS'));
		echo $this->Html->meta(array('name' => 'reply-to', 'content' => 'sas@ifrs.edu.br'));
		echo $this->Html->meta(array('name' => 'robots', 'content' => 'index,nofollow'));
		echo $this->fetch('meta');
		
		echo $this->Html->css('1140');
		echo $this->Html->css('ie');
		echo $this->Html->css('mobile');
		echo $this->fetch('css');
		
		echo $this->Html->script('css3-mediaqueries');
		echo $this->fetch('script');
	?>
</head>
<body>
	<?php echo $this->element('layout/barra_governo'); ?>
	<div id="header" class="container">
		<div class="row">
			<div class="twelvecol">
				<h1><a href="/" title="Página Inicial"><img src="/img/topo.jpg" alt="2º Seminário Anual dos Servidores do IFRS" /></a></h1>
			</div>
		</div>
	</div>
	<div id="content" class="container">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
	<div id="footer" class="container">
		<div class="row">
			<div class="twelvecol">
				<p class="titulo">Instituto Federal de Educa&ccedil;&atilde;o, Ci&ecirc;ncia e Tecnologia do Rio Grande do Sul - IFRS</p>
				<address>Rua General Os&oacute;rio, 348 | Bairro Centro | CEP: 95700-000 | Bento Gon&ccedil;alves/RS</address>
				<p>E-mail: <a href="mailto:sas@ifrs.edu.br">sas@ifrs.edu.br</a></p>
			</div>
		</div>
	</div>
</body>