<?php echo $this->Html->css('forms', null, array('inline' => false)); ?>
<?php
$this->start('breadcrumb');
	echo $this->element('layout/breadcrumb', array('links' =>
		array(
			$this->Html->link('Página inicial', array('controller' => 'index', 'action' => 'index')),
			$this->Html->link('Contato', array('controller' => 'contatos', 'action' => 'index')),
		)
	));
$this->end();
?>

<div class="row">
	<div class="onecol"></div>
	<div class="tencol">
		<h2 class="center">Contato</h2>
		<p>Para entrar em contato com a Comiss&atilde;o Organizadora do SAS utilize o formul&aacute;rio abaixo. Se preferir, envie um email para o endere&ccedil;o: <a href="mailto:sas@ifrs.edu.br">sas@ifrs.edu.br</a></p>
		<?php
			echo $this->Form->create('Contato');
		?>
				<fieldset>
					<legend class="negrito">Formulário de Contato</legend>
		<?php
					echo $this->element('forms/contato');
		?>
				</fieldset>
		<?php
			echo $this->Form->end('Enviar');
		?>
	</div>
	<div class="onecol last"></div>
</div>