<h2>Contatos do Site</h2>
<table id="tabela_contatos">
	<thead>
		<tr>
			<th>Nome</th>
			<th>Email</th>
			<th>Assunto</th>
			<th>Mensagem</th>
			<th>Enviado para o email?</th>
			<th>Marcar como respondido</th>
		</tr>
	</thead>
	<tbody>
		<?php
		foreach ($contatos as $key => $contato):
		?>
			<tr>
				<td><?php echo $contato['Contato']['nome']; ?></td>
				<td><?php echo $contato['Contato']['email']; ?></td>
				<td><?php echo $contato['Contato']['assunto']; ?></td>
				<td><?php echo nl2br($contato['Contato']['mensagem']); ?></td>
				<td class="center">
					<?php
					if ($contato['Contato']['enviado'] == 'S') {
					?>
						<span class="sim">Sim</span>
					<?php
					} else if ($contato['Contato']['enviado'] == 'N') {
					?>
						<span class="nao">N&atilde;o</span>
					<?php
					}
					?>
				</td>
				<td class="center">
					<?php echo $this->Html->link('Respondido (Deletar)', array('action' => 'deletar', $contato['Contato']['id']), array('data-role' => 'button', 'data-icon' => 'check', 'data-iconpos' => 'notext', 'data-inline' => 'true', 'data-mini' => 'true'), 'Você tem certeza que esse contato foi respondido? Isso acarretará na exclusão do contato.'); ?>
				</td>
			</tr>
		<?php
		endforeach;
		?>
	</tbody>
</table>
<script>
	$(document).ready(function() {
		$('#tabela_contatos').dataTable({
			"aaSorting" : [
				
			],
		});
	});
</script>