<?php
class Participante extends AppModel {
	public $name = 'Participante';
	
	public $displayField = 'nome_completo';
	
	public $hasOne = array(
		'Obra',
	);
	
	public $hasMany = array(
		'Frequencia',
	);
	
	public $belongsTo = array(
		'Campus',
		'Edicao',
		'Atividade',
		'Apresentacao',
	);
	
	public $validate = array(
		'nome_completo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Nome Completo não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ\-\']+$/'),
				'message' => 'O Nome Completo deve conter somente letras, traço(-) e apóstrofo(\').',
			),
		),
		'siape' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Matrícula SIAPE não pode ser deixada em branco.',
			),
			'regra2' => array(
				'rule' => 'numeric',
				'message' => 'A Matrícula SIAPE deve conter somente números.',
			),
			'regra3' => array(
				'rule' => 'isUnique',
				'message' => 'Já existe uma inscrição com esse SIAPE.',
			),
		),
		'email' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Email não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => 'email',
				'message' => 'O Email deve ser válido.',
			),
		),
		'nome_cracha' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Nome no Crachá não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ\-\']+$/'),
				'message' => 'O Nome no Crachá deve conter somente letras, traço(-) e apóstrofo(\').',
			),
		),
		'confirmado' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'Selecione "Sim" ou "Não".',
			),
			'regra2' => array(
				'rule' => array('inList', array('S', 'N')),
				'message' => 'A Confirmação deve ser "Sim" ou "Não".',
			),
		),
		'campus_id' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'Selecione seu câmpus de origem.',
			),
		),
		'aceito' => array(
			'regra1' => array(
				'rule' => array('equalTo', '1'),
				'message' => 'Você deve ler e aceitar os termos do regulamento.',
			),
		),
	);
	
	
	public function beforeSave($options = array()) {
		if (!$this->data['Participante']['id']) {
			// Seta a edição atual para essa inscrição.
			$edicao_atual = $this->Edicao->find('first', array('conditions' => array('atual' => '1')));
			if ($edicao_atual) {
				$this->data['Participante']['edicao_id'] = $edicao_atual['Edicao']['id'];
			} else {
				return false;
			}
			
			// Seta o número de inscrição.
			$inscricao = $this->query("SELECT MAX(inscricao) AS numero FROM participantes WHERE edicao_id = " . $edicao_atual['Edicao']['id']);
			if ($inscricao) {
				$this->data['Participante']['inscricao'] = $inscricao[0][0]['numero'] + 1;
			} else {
				$this->data['Participante']['inscricao'] = '1';
			}
		}
		
		parent::beforeSave($options);
	}
	
	public function beforeFind($queryData) {
		$edicao_atual = $this->Edicao->find('first', array('recursive' => '-1', 'conditions' => array('atual' => '1')));
		if ($edicao_atual) {
			$queryData['conditions']['Participante.edicao_id'] = $edicao_atual['Edicao']['id'];
		}
		
		return $queryData;
	}
	
	public function afterFind($results, $primary = false) {
		$edicao_atual = $this->Edicao->find('first', array('recursive' => '-1', 'conditions' => array('atual' => '1')));
		if ($results && strtotime(date('Y-m-d')) > strtotime(implode("-", array_reverse(explode("/", $edicao_atual['Edicao']['fim']))))) {
			foreach ($results as $key => $result) {
				if (!empty($result['Frequencia'])) {
					$results[$key][$this->alias]['presente'] = 'S';
					$soma = 0.0;
					$entrada = null;
					$saida = null;
					foreach ($result['Frequencia'] as $key2 => $frequencia) {
						if ($frequencia['entrada_saida'] == 'E') {
							$entrada = new DateTime($frequencia['data_hora']);
						} else if ($frequencia['entrada_saida'] == 'S') {
							$saida = new DateTime($frequencia['data_hora']);
						}
						
						if ($entrada && $saida) {
							$intervalo = $entrada->diff($saida);
							$soma += $intervalo->h; // Horas
							$soma += $intervalo->i / 60; // Minutos
							$soma += $intervalo->s / 60 / 60; // Segundos
							
							$entrada = null;
							$saida = null;
						}
					}
					$results[$key][$this->alias]['horas_presente'] = $soma;
				} else {
					$results[$key][$this->alias]['presente'] = 'N';
				}
			}
		}
		
		return $results;
	}
}
