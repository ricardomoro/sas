<?php
class Foto extends AppModel {
	public $name = 'Foto';
	
	public $displayField = 'titulo';
	
	public $belongsTo = array(
		'Album',
	);
	
	public $validate = array(
		'titulo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Título não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ]+$/'),
				'message' => 'O Título deve conter somente letras.',
			),
		),
		'descricao' => array(
			'regra1' => array(
				'rule' => array('custom', '/[0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ\.\,\-]+$/'),
				'message' => 'A Descrição deve conter somente letras, números e pontuação.',
				'allowEmpty' => true,
			),
		),
		'arquivo' => array(
			'regra1' => array(
				'rule' => array('checkUpload', true),
				'message' => 'Uma foto deve ser enviada.',
			),
			'regra2' => array(
				'rule' => array('mimeType', array('image/gif', 'image/jpeg', 'image/png')),
				'message' => 'A Foto deve ser dos seguintes formatos: GIF, JPEG, JPG ou PNG.',
			),
			'regra3' => array(
				'rule' => array('extension', array('gif', 'jpeg', 'jpg', 'png')),
				'message' => 'A Foto deve ser dos seguintes formatos: GIF, JPEG, JPG ou PNG.',
			),
			'regra5' => array(
				'rule' => 'uploadError',
				'message' => 'Ocorreu um erro com o upload. Por favor, tente novamente.'
			),
		),
	);
	
	public function checkUpload($data, $required = false) {
		$data = array_shift($data);
		
		if (!$required && $data['error'] == 4) {
			return true;
		}
		
		if ($required && $data['error'] !== 0) {
			return false;
		}
		
		if ($data['size'] == 0) {
			return false;
		}
		
		return true;
	}
	
	public function beforeSave($options = array()) {
		parent::beforeSave($options);
		
		if (isset($this->data['Foto'])) {
			if (isset($this->data['Foto']['arquivo'])) {
				if (is_array($this->data['Foto']['arquivo'])) {
					$filename = md5($this->data['Foto']['arquivo']['size'] . date('dmYHis'));
					$ext = strtolower(pathinfo($this->data['Foto']['arquivo']['name'], PATHINFO_EXTENSION));
					if (move_uploaded_file($this->data['Foto']['arquivo']['tmp_name'], WWW_ROOT . 'files/uploads/' . $filename . '.' . $ext)) {
						$this->data['Foto']['arquivo'] = $filename . '.' . $ext;
					} else {
						$this->data['Foto']['arquivo'] = NULL;
						return false;
					}
				}
			}
		}
		
		return true;
	}
}
