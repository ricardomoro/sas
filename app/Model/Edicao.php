<?php
class Edicao extends AppModel {
	public $name = 'Edicao';
	
	public $useTable = 'edicoes';
	
	public $displayField = 'numero';
	
	
	public $hasMany = array(
		'Participante' => array(
			'foreignKey' => 'edicao_id',
		),
	);
	
	
	public $validate = array(
		'abertura_inscricoes' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Data de Abertura das Inscrições não pode ser deixada em branco.',
			),
			'regra2' => array(
				'rule' => array('date', 'dmy'),
				'message' => 'A Data de Abertura das Inscrições deve ser uma data válida.',
			),
		),
		'encerramento_inscricoes' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Data de Encerramento das Inscrições não pode ser deixada em branco.',
			),
			'regra2' => array(
				'rule' => array('date', 'dmy'),
				'message' => 'A Data de Encerramento das Inscrições deve ser uma data válida.',
			),
		),
		'inicio' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Data de Início da Edição não pode ser deixada em branco.',
			),
			'regra2' => array(
				'rule' => array('date', 'dmy'),
				'message' => 'A Data de Início da Edição deve ser uma data válida.',
			),
		),
		'fim' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Data de Término da Edição não pode ser deixada em branco.',
			),
			'regra2' => array(
				'rule' => array('date', 'dmy'),
				'message' => 'A Data de Término da Edição deve ser uma data válida.',
			),
		),
		'numero' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Número não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => 'numeric',
				'message' => 'O Número deve conter somente números.',
			),
			'regra3' => array(
				'rule' => 'isUnique',
				'message' => 'Já existe uma edição com esse Número.',
			),
		),
		'titulo' => array(
			'regra1' => array(
				'rule' => array('custom', '/[0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ]+$/'),
				'allowEmpty' => true,
				'message' => 'O Título deve conter somente letras e números.',
			),
		),
		'atual' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'Selecione "Sim" ou "Não".',
			),
			'regra2' => array(
				'rule' => array('inList', array('1', '0')),
				'message' => 'A Publicação deve ser "Sim" ou "Não".',
			),
		),
	);
	
	
	public function beforeSave($options = array()) {
		$this->data['Edicao']['abertura_inscricoes'] = implode("-", array_reverse(explode("/", $this->data['Edicao']['abertura_inscricoes'])));
		$this->data['Edicao']['encerramento_inscricoes'] = implode("-", array_reverse(explode("/", $this->data['Edicao']['encerramento_inscricoes'])));
		$this->data['Edicao']['inicio'] = implode("-", array_reverse(explode("/", $this->data['Edicao']['inicio'])));
		$this->data['Edicao']['fim'] = implode("-", array_reverse(explode("/", $this->data['Edicao']['fim'])));
		
		// Seta todas as edições como não atuais se essa for a atual.
		if ($this->data['Edicao']['atual'] == '1') {
			$this->query('UPDATE edicoes SET atual = \'0\'');
		}
		
		parent::beforeSave($options);
	}
	
	public function afterFind($results, $primary = false) {
		parent::afterFind($results, $primary);
		
		foreach ($results as $key => $val) {
			if (isset($val['Edicao']['abertura_inscricoes'])) {
				$results[$key]['Edicao']['abertura_inscricoes'] = date('d/m/Y', strToTime($val['Edicao']['abertura_inscricoes']));
			}
			if (isset($val['Edicao']['encerramento_inscricoes'])) {
				$results[$key]['Edicao']['encerramento_inscricoes'] = date('d/m/Y', strToTime($val['Edicao']['encerramento_inscricoes']));
			}
			if (isset($val['Edicao']['inicio'])) {
				$results[$key]['Edicao']['inicio'] = date('d/m/Y', strToTime($val['Edicao']['inicio']));
			}
			if (isset($val['Edicao']['fim'])) {
				$results[$key]['Edicao']['fim'] = date('d/m/Y', strToTime($val['Edicao']['fim']));
			}
		}

		return $results;
	}
}
