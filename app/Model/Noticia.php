<?php
class Noticia extends AppModel {
	public $name = 'Noticia';
	
	public $displayField = 'titulo';
	
	public $hasMany = array(
		'Imagem' => array(
			'dependent' => true,
		),
	);
	
	public $validate = array(
		'titulo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Título não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ]+$/'),
				'message' => 'O Título deve conter somente letras ou números.',
			),
		),
		'conteudo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Conteúdo não pode ser deixado em branco.',
			),
		),
		'publicar' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'Selecione "Sim" ou "Não".',
			),
			'regra2' => array(
				'rule' => array('inList', array('S', 'N')),
				'message' => 'A Publicação deve ser "Sim" ou "Não".',
			),
		),
	);
	
	public function beforeSave($options = array()) {
		if ($this->data['Noticia']['id']) {
			$noticia = $this->findById($this->data['Noticia']['id']);
			
			if ($this->data['Noticia']['publicar'] == 'S' && $noticia['Noticia']['publicar'] == 'N') {
				$this->data['Noticia']['publicado_em'] = date('Y-m-d H:i:s');
			} else if ($this->data['Noticia']['publicar'] == 'N' && $noticia['Noticia']['publicar'] == 'S') {
				$this->data['Noticia']['publicado_em'] = NULL;
			}
		} else if ($this->data['Noticia']['publicar'] == 'S') {
			$this->data['Noticia']['publicado_em'] = date('Y-m-d H:i:s');
		} else {
			$this->data['Noticia']['publicado_em'] = NULL;
		}
		
		parent::beforeSave();
	}
}
