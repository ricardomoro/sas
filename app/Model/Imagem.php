<?php
class Imagem extends AppModel {
	public $name = 'Imagem';
	
	public $useTable = 'imagens';
	
	public $displayField = 'titulo';
	
	/*public $actsAs = array(
		'Upload.Upload' => array(
			'arquivo' => array(
				'pathMethod' => 'primaryKey',
				'path' => '{ROOT}webroot{DS}files{DS}uploads{DS}{model}{DS}',
				'saveDir' => false,
				'thumbnails' => false,
			),
		),
	);*/
	
	public $belongsTo = array(
		'Noticia',
	);
	
	public $validate = array(
		'titulo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Título não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ]+$/'),
				'message' => 'O Título deve conter somente letras.',
			),
		),
		'arquivo' => array(
			'regra1' => array(
				'rule' => array('checkUpload', true),
				'message' => 'Uma imagem deve ser enviada.',
			),
			'regra2' => array(
				'rule' => array('mimeType', array('image/gif', 'image/jpeg', 'image/png')),
				'message' => 'A Imagem deve ser dos seguintes formatos: GIF, JPEG, JPG ou PNG.',
			),
			'regra3' => array(
				'rule' => array('extension', array('gif', 'jpeg', 'jpg', 'png')),
				'message' => 'A Imagem deve ser dos seguintes formatos: GIF, JPEG, JPG ou PNG.',
			),
			'regra5' => array(
				'rule' => 'uploadError',
				'message' => 'Ocorreu um erro com o upload. Por favor, tente novamente.'
			),
		),
	);
	
	public function checkUpload($data, $required = false) {
		$data = array_shift($data);
		
		if (!$required && $data['error'] == 4) {
			return true;
		}
		
		if ($required && $data['error'] !== 0) {
			return false;
		}
		
		if ($data['size'] == 0) {
			return false;
		}
		
		return true;
	}
	
	public function beforeSave($options = array()) {
		parent::beforeSave($options);
		
		if (isset($this->data['Imagem'])) {
			if (isset($this->data['Imagem']['arquivo'])) {
				if (is_array($this->data['Imagem']['arquivo'])) {
					$filename = md5($this->data['Imagem']['arquivo']['size'] . date('dmYHis'));
					$ext = strtolower(pathinfo($this->data['Imagem']['arquivo']['name'], PATHINFO_EXTENSION));
					if (move_uploaded_file($this->data['Imagem']['arquivo']['tmp_name'], WWW_ROOT . 'files/uploads/' . $filename . '.' . $ext)) {
						$this->data['Imagem']['arquivo'] = $filename . '.' . $ext;
					} else {
						$this->data['Imagem']['arquivo'] = NULL;
						return false;
					}
				}
			}
		}
		
		return true;
	}
}
