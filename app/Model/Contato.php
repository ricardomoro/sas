<?php
class Contato extends AppModel {
	public $name = 'Contato';
	
	public $displayField = 'assunto';
	
	public $validate = array(
		'nome' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Nome não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[a-zA-Z]+$/'),
				'message' => 'O Nome deve conter somente letras.',
			),
		),
		'email' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Email não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => 'email',
				'message' => 'O Email deve ser válido.',
			),
		),
		'assunto' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Assunto não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[a-zA-Z]+$/'),
				'message' => 'O Assunto deve conter somente letras.',
			),
		),
		'mensagem' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Mensagem não pode ser deixada em branco.',
			),
		),
	);
}
