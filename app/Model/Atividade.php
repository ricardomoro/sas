<?php
class Atividade extends AppModel {
	public $name = 'Atividade';
	
	public $virtualFields = array(
		'nome_descricao' => 'CONCAT(Atividade.nome, " (", Atividade.descricao, ")")',
	);
	
	public $displayField = 'nome_descricao';
	
	public $hasMany = array(
		'Participante',
	);
}
