<?php
class Certificado extends AppModel {
	public $name = 'Certificado';
	
	public $useTable = false;
	
	public $validate = array(
		'registro' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Número de Registro não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => 'numeric',
				'message' => 'O Número de Registro deve conter somente números.',
			),
			'regra3' => array(
				'rule' => array('minLength', 10),
				'message' => 'O Número de Registro deve conter 10 digitos.',
			),
			'regra4' => array(
				'rule' => array('maxLength', 10),
				'message' => 'O Númeto de Registro deve conter 10 dígitos.',
			),
		),
		'siape' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Matrícula SIAPE não pode ser deixada em branco.',
			),
			'regra2' => array(
				'rule' => 'numeric',
				'message' => 'A Matrícula SIAPE deve conter somente números.',
			),
		),
		'edicao' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'Selecione uma edição do evento.',
			),
		),
	);
}
