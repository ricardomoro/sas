<?php
class Apresentacao extends AppModel {
	public $name = 'Apresentacao';
	
	public $useTable = 'apresentacoes';
	
	public $displayField = 'tipo';
	
	public $hasMany = array(
		'Participante' => array(
			'foreignKey' => 'apresentacao_id',
		),
	);
}
