<?php
class Album extends AppModel {
	public $name = 'Album';
	
	public $useTable = 'albuns';
	
	public $displayField = 'nome';
	
	public $actsAs = array(
		'Tree',
	);
	
	public $hasMany = array(
		'Foto' => array(
			'foreignKey' => 'album_id',
		),
	);
	
	public $validate = array(
		'nome' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Nome não pode ser deixado em branco.',
			),
			'regra2' => array(
				'rule' => array('custom', '/[0-9a-zA-ZéúíóáÉÚÍÓÁèùìòàÈÙÌÒÀõãñÕÃÑêûîôâÊÛÎÔÂëÿüïöäËYÜÏÖÄ]+$/'),
				'message' => 'O Nome deve conter somente letras.',
			),
		),
	);
}
