<?php
class User extends AppModel {
	public $name = 'User';
	
	
	public $validate = array(
		'current_password' => array(
			'r1' => array(
				'rule' => 'notEmpty',
				'message' => 'A Senha Atual deve ser informada.',
			),
			'r2' => array(
				'rule' => 'checkCurrentPassword',
				'message' => 'A Senha atual não confere.',
			),
		),
		'new_password' => array(
			'r1' => array(
				'rule' => 'notEmpty',
				'message' => 'Uma Nova Senha deve ser informada.',
			),
		),
		'check_password' => array(
			'r1' => array(
				'rule' => 'notEmpty',
				'message' => 'Confirme a Nova Senha.',
			),
			'r2' => array(
				'rule' => 'passwordsMatch',
				'message' => 'As Senhas não conferem.',
			),
		),
	);
	
	public function checkCurrentPassword($data) {
		$this->id = AuthComponent::user('id');
		$password = $this->field('password');
		return (AuthComponent::password($data['current_password']) == $password);
	}
	
	public function passwordsMatch($data) {
		if ($this->data['User']['check_password'] != $this->data['User']['new_password']) {
			return false;
		}
		
		return true;
	}
	
	public function beforeSave($options = array()) {
		if (isset($this->data['User']['new_password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['new_password']);
		}
		
		if (isset($this->data['User']['password'])) {
			$this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
		}
		
		return true;
	}
}
	