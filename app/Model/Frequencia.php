<?php
class Frequencia extends AppModel {
	public $name = 'Frequencia';
	
	public $belongsTo = array(
		'Participante',
	);
	
	
	public function beforeSave($options = array()) {
		unset($this->data[$this->alias]['etapa']);
		$this->data[$this->alias]['data_hora'] = date('Y-m-d H:i:s');
		
		$frequencias = $this->find('all', array('conditions' => array('participante_id' => $this->data['Participante']['id']), 'recursive' => '-1'));		
		
		$frequencias_do_dia = 0;
		
		foreach ($frequencias as $frequencia) {
			if (date('Y-m-d', strtotime($frequencia[$this->alias]['data_hora'])) == date('Y-m-d')) {
				$frequencias_do_dia++;
			}
		}
		
		if ($frequencias_do_dia % 2 == 0) {
			$this->data[$this->alias]['entrada_saida'] = 'E';
		} else {
			$this->data[$this->alias]['entrada_saida'] = 'S';
		}
		
		parent::beforeSave($options);
	}
}