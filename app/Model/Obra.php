<?php
class Obra extends AppModel {
	public $name = 'Obra';
	
	public $displayField = 'titulo';
	
	public $belongsTo = array(
		'Participante',
	);
	
	public $validate = array(
		'titulo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Título não pode ser deixado em branco pois você preencheu o Tipo.',
			),
		),
		'tipo' => array(
			'regra1' => array(
				'rule' => 'notEmpty',
				'message' => 'O Tipo não pode ser deixado em branco pois você preencheu o Título.',
			),
		),
	);
}
