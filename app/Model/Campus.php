<?php
class Campus extends AppModel {
	public $name = 'Campus';
	
	public $useTable = 'campus';
	
	public $displayField = 'nome';
	
	public $hasMany = array(
		'Participante',
	);
}
