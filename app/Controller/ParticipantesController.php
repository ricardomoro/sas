<?php
class ParticipantesController extends AppController {
	public $name = 'Participantes';
	
	public $uses = array('Participante');
	
	public $components = array('Paginator');
	
	public $helpers = array('Paginator');
	
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_toggle_confirmacao');
	}
	
	
	public function index() {
		if ($this->edicao_atual) {
			if ((strtotime(date('Y-m-d H:i:s')) >= strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['abertura_inscricoes']))) . ' 00:00:00')) && 
				(strtotime(date('Y-m-d H:i:s')) <= strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['encerramento_inscricoes']))) . ' 23:59:59'))) {
					$this->redirect('nova_inscricao');
			} else {
				$participantes = array();
				$campus = $this->Participante->Campus->find('all', array('recursive' => '-1'));
				foreach ($campus as $key => $value) {
					$participantes[$value['Campus']['nome']] = $this->Participante->find('all', array('conditions' => array('campus_id' => $value['Campus']['id'], 'confirmado' => 'S'), 'recursive' => '-1'));
				}
				$this->set('participantes', $participantes);
			}
		}
	}
	
	public function nova_inscricao() {
		$this->set('title_for_layout', 'Formulário de Inscrição');
		
		if ($this->edicao_atual) {
			if ((strtotime(date('Y-m-d H:i:s')) < strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['abertura_inscricoes']))) . ' 00:00:00')) ||
				(strtotime(date('Y-m-d H:i:s')) > strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['encerramento_inscricoes']))) . ' 23:59:59'))) {
					$this->redirect('index');
			}
		} else {
			$this->redirect('index');
		}
		
		if ($this->request->is('post')) {
			if (empty($this->request->data['Obra']['titulo']) && empty($this->request->data['Obra']['tipo'])) {
				unset($this->request->data['Obra']);
			}
			
			$this->request->data['Participante']['data'] = date('Y-m-d H:i:s');
			
			$this->request->data['Participante']['nome_completo'] = trim($this->request->data['Participante']['nome_completo']);
			
			$this->request->data['Participante']['nome_cracha'] = trim($this->request->data['Participante']['nome_cracha']);
			
			if ($this->Participante->saveAll($this->request->data)) {
				$mail_subject = 'Sua inscrição no 2º Seminário Anual dos Servidores do IFRS foi efetuada com sucesso!';
				$mail_headers = 'From: sas@ifrs.edu.br' . "\r\n" .
								'Reply-To: sas@ifrs.edu.br' . "\r\n";
				mail($this->request->data['Participante']['email'], 'Inscrição no 2º SAS', $mail_subject, $mail_headers);
				$this->Session->setFlash('Sua inscrição foi realizada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect('/');
			} else {
				$this->Session->setFlash('Ocorreu um problema com a sua inscrição. Por favor, verifique os dados e tente novamente.');
			}
		}
		
		$campus = $this->Participante->Campus->find('list');
		$this->set('campus', $campus);
		
		$atividades = $this->Participante->Atividade->find('list');
		$this->set('atividades', $atividades);
		
		$apresetacoes = $this->Participante->Apresentacao->find('list');
		$this->set('apresentacoes', $apresetacoes);
	}
	
	
	// ADMIN
	public function admin_index() {
		if ($this->request->is('post')) {
			$conditions = array();
			
			if (isset($this->request->data['Participante']['filtro_campus']) && !empty($this->request->data['Participante']['filtro_campus'])) {
				$conditions['campus_id'] = $this->request->data['Participante']['filtro_campus'];
			}
			if (isset($this->request->data['Participante']['filtro_atividade']) && !empty($this->request->data['Participante']['filtro_atividade'])) {
				$conditions['atividade_id'] = $this->request->data['Participante']['filtro_atividade'];
			}
			if (isset($this->request->data['Participante']['filtro_apresentacao']) && !empty($this->request->data['Participante']['filtro_apresentacao'])) {
				$conditions['apresentacao_id'] = $this->request->data['Participante']['filtro_apresentacao'];
			}
			if (isset($this->request->data['Participante']['filtro_obra']) && !empty($this->request->data['Participante']['filtro_obra'])) {
				if ($this->request->data['Participante']['filtro_obra'] == 'C') {
					if (!isset($conditions['not'])) {
						$conditions['not'] = array();
					}
					$conditions['not'] = array_merge($conditions['not'], array('Obra.id' => NULL));
				}
				if ($this->request->data['Participante']['filtro_obra'] == 'S') {
					$conditions['Obra.id'] = NULL;
				}
			}
			if (isset($this->request->data['Participante']['filtro_necessidade_especial']) && !empty($this->request->data['Participante']['filtro_necessidade_especial'])) {
				if (!isset($conditions['not'])) {
					$conditions['not'] = array();
				}
				$conditions['not'] = array_merge($conditions['not'], array('necessidade_especial' => ''));
			}
			if (isset($this->request->data['Participante']['filtro_restricao_alimentar']) && !empty($this->request->data['Participante']['filtro_restricao_alimentar'])) {
				if (!isset($conditions['not'])) {
					$conditions['not'] = array();
				}
				$conditions['not'] = array_merge($conditions['not'], array('restricao_alimentar' => ''));
			}
			
			$this->set('participantes', $this->Participante->find('all', array('conditions' => $conditions)));
		} else {
			$this->set('participantes', $this->Participante->find('all'));
		}
		$this->set('campus', $this->Participante->Campus->find('list'));
		$this->set('atividades', $this->Participante->Atividade->find('list', array('fields' => array('id', 'nome'))));
		$this->set('apresentacoes', $this->Participante->Apresentacao->find('list'));
	}
	
	public function admin_ver($id = null) {
		if ($id) {
			$this->set('participante', $this->Participante->findById($id));
		} else {
			$this->redirect('index');
		}
	}
	
	public function admin_toggle_confirmacao() {
		if ($this->request->is('ajax')) {
			$this->layout = 'ajax';
			$this->autoRender = false;
			
			$participante = $this->Participante->findById($this->request->data['id']);
			
			if ($participante['Participante']['confirmado'] == 'S') {
				$participante['Participante']['confirmado'] = 'N';
			} else if ($participante['Participante']['confirmado'] == 'N') {
				$participante['Participante']['confirmado'] = 'S';
			}
			
			if ($this->Participante->save($participante)) {
				echo $participante['Participante']['confirmado'];
			} else {
				$this->response->statusCode(500);
			}
		} else {
			$this->redirect('index');
		}
	}
	
	public function admin_relatorios() {
		if (isset($this->params['named']['tipo'])) {
			$this->layout = 'pdf';
			switch ($this->params['named']['tipo']) {
				case 'dados_cracha':
					$this->layout = false;
					$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S')));
					$this->set('participantes', $participantes);
					$this->render('relatorios/dados_cracha');
					$this->response->download('dados_cracha.csv');
					break;
				case 'lista_emails':
					$this->layout = false;
					$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S')));
					$this->set('participantes', $participantes);
					$this->render('relatorios/lista_emails');
					$this->response->download('lista_de_emails.txt');
					break;
				case 'por_campus':
					ini_set('memory_limit', '256M');
					$participantes = array();
					$campus = $this->Participante->Campus->find('all', array('recursive' => '-1'));
					foreach ($campus as $key => $value) {
						$participantes[$value['Campus']['nome']] = $this->Participante->find('all', array('conditions' => array('campus_id' => $value['Campus']['id'], 'confirmado' => 'S'), 'recursive' => '-1'));
					}
					$this->set('participantes', $participantes);
					$this->render('relatorios/por_campus');
					$this->response->download('relatorio_por_campus.pdf');
					break;
				case 'por_atividade':
					$participantes = array();
					$atividades = $this->Participante->Atividade->find('all');
					foreach ($atividades as $key => $value) {
						$participantes[$value['Atividade']['nome']] = $this->Participante->find('all', array('conditions' => array('atividade_id' => $value['Atividade']['id'], 'confirmado' => 'S'), 'recursive' => '-1'));
					}
					$this->set('participantes', $participantes);
					$this->render('relatorios/por_atividade');
					$this->response->download('relatorio_por_atividade.pdf');
					break;
				case 'por_apresentacao':
					$participantes = array();
					$apresentacoes = $this->Participante->Apresentacao->find('all');
					foreach ($apresentacoes as $key => $value) {
						$participantes[$value['Apresentacao']['tipo']] = $this->Participante->find('all', array('conditions' => array('apresentacao_id' => $value['Apresentacao']['id'], 'confirmado' => 'S'), 'recursive' => '-1'));
					}
					$this->set('participantes', $participantes);
					$this->render('relatorios/por_apresentacao');
					$this->response->download('relatorio_por_apresentacao.pdf');
					break;
				case 'por_obra':
					$obras = $this->Participante->Obra->find('all', array('conditions' => array('Participante.edicao_id' => $this->edicao_atual['Edicao']['id'])));
					$this->set('obras', $obras);
					$this->render('relatorios/por_obra');
					$this->response->download('relatorio_por_obra.pdf');
					break;
				case 'com_necessidade_especial':
					$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S', 'not' => array('necessidade_especial' => ''))));
					$this->set('participantes', $participantes);
					$this->render('relatorios/com_necessidade_especial');
					$this->response->download('relatorio_com_necessidade_especial.pdf');
					break;
				case 'com_restricao_alimentar':
					$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S', 'not' => array('restricao_alimentar' => ''))));
					$this->set('participantes', $participantes);
					$this->render('relatorios/com_restricao_alimentar');
					$this->response->download('relatorio_com_restricao_alimentar.pdf');
					break;
				case 'lista_emails_participantes':
					$this->layout = false;
					$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S')));
					$this->set('participantes', $participantes);
					$this->render('relatorios/lista_emails_participantes');
					$this->response->download('lista_de_emails_dos_participantes_efetivos.txt');
					break;
				case 'frequencia':
					ini_set('memory_limit', '512M');
					$participantes = array();
					$campus = $this->Participante->Campus->find('all', array('recursive' => '-1'));
					foreach ($campus as $key => $value) {
						$participantes[$value['Campus']['nome']] = $this->Participante->find('all', array('conditions' => array('campus_id' => $value['Campus']['id'], 'confirmado' => 'S'), 'recursive' => '1'));
						foreach ($participantes[$value['Campus']['nome']] as $k => $participante) {
							if ($participante['Participante']['presente'] == 'N') {
								unset($participantes[$value['Campus']['nome']][$k]);
							}
						}
					}
					$this->set('participantes', $participantes);
					$this->render('relatorios/frequencia');
					$this->response->download('relatorio_frequencia.pdf');
					break;
				case 'faltas':
					ini_set('memory_limit', '256M');
					$participantes = array();
					$campus = $this->Participante->Campus->find('all', array('recursive' => '-1'));
					foreach ($campus as $key => $value) {
						$participantes[$value['Campus']['nome']] = $this->Participante->find('all', array('conditions' => array('campus_id' => $value['Campus']['id'], 'confirmado' => 'S'), 'recursive' => '1'));
						foreach ($participantes[$value['Campus']['nome']] as $k => $participante) {
							if ($participante['Participante']['presente'] == 'S') {
								unset($participantes[$value['Campus']['nome']][$k]);
							}
						}
					}
					$this->set('participantes', $participantes);
					$this->render('relatorios/faltas');
					$this->response->download('relatorio_faltas.pdf');
					break;
				default:
					$this->redirect('relatorios');
					break;
			}
		}
	}

	public function admin_certificados($id = null) {
		if (strtotime(date('Y-m-d')) <= strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['fim']))))) {
			$this->Session->setFlash('A edição atual ainda não ocorreu. Os certificados só podem ser gerados após o evento.');
			$this->redirect($this->referer());
		}
		
		if ($id) {
			$this->layout = 'pdf';
			$this->set('data', $this->getDataExtensa());
			$participante = $this->Participante->findById($id);
			$participantes = array();
			$participantes[] = $participante;
			$this->set('participantes', $participantes);
			$this->render('certificado');
			$this->response->download($participante['Participante']['nome_completo'] . ' - Certificado.pdf');
		} else if (isset($this->params['named']['todos']) && $this->params['named']['todos'] && isset($this->params['named']['campus'])) {
			ini_set('memory_limit', '512M');
			$this->layout = 'pdf';
			$this->set('data', $this->getDataExtensa());
			$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S', 'certificado' => 'S', 'campus_id' => $this->params['named']['campus'])));
			if ($participantes) {
				$this->set('participantes', $participantes);
				$this->render('certificado');
				$this->response->download('certificados_sas.pdf');
			} else {
				$this->layout = 'admin';
				$this->Session->setFlash('Não existem participantes aptos para receber o certificado nessa unidade.');
			}
		}
		
		$campus = $this->Participante->Campus->find('all', array('recursive' => '-1'));
		$this->set('campus', $campus);
		
		$participantes = $this->Participante->find('all', array('conditions' => array('confirmado' => 'S', 'certificado' => 'S')));
		$this->set('participantes', $participantes);
	}
	
	public function admin_certificacao() {
		if (strtotime(date('Y-m-d')) <= strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['fim']))))) {
			$this->Session->setFlash('A edição atual ainda não ocorreu. As certificações só podem ser registradas após o evento.');
			$this->redirect($this->referer());
		}
		
		if ($this->request->is('post')) {
			foreach ($this->data['Participante'] as $id => $value) {
				if (!$value) {
					continue;
				}
				$this->Participante->id = $id;
				$this->Participante->saveField('certificado', $value, true);
			}
			$this->Session->setFlash('Dados de certificação gravados com sucesso.', 'default', array('class' => 'success'));
		}
		
		$this->paginate = array(
			'conditions' => array('confirmado' => 'S', 'certificado' => null),
			'limit' => 10,
		);
		
		$participantes = $this->paginate('Participante');
		$this->set('participantes', $participantes);
	}
}
