<?php
class EdicoesController extends AppController {
	public $name = 'Edicoes';
	
	public $uses = array('Edicao');
	
	
	public function admin_index() {
		$edicoes = $this->Edicao->find('all', array('recursive' => '-1'));
		$this->set('edicoes', $edicoes);
	}
	
	public function admin_ver($id = null) {
		if ($id) {
			$this->set('edicao', $this->Edicao->findById($id));
		} else {
			$this->redirect('index');
		}
	}
	
	public function admin_nova() {
		if ($this->request->is('post')) {
			if ($this->Edicao->saveAll($this->request->data)) {
				$this->Session->setFlash('Nova edição cadastrada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect('index');
			} else {
				$this->Session->setFlash('Ocorreu um problema com o cadastro da nova edição. Por favor, verifique os dados e tente novamente.');
			}
		}
	}
	
	public function admin_editar($id = null) {
		if ($this->request->is('put')) {
			if ($this->Edicao->saveAll($this->request->data)) {
				$this->Session->setFlash('Edição modificada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect('index');
			} else {
				$this->Session->setFlash('Ocorreu um problema com a modificação da edição. Por favor, verifique os dados e tente novamente.');
			}
		} else {
			if ($id) {
				$this->request->data = $this->Edicao->findById($id);
			} else {
				$this->redirect('index');
			}
		}
	}
	
	public function admin_deletar($id = null) {
		if ($this->request->is('get')) {
			if ($id) {
				if ($this->Edicao->delete($id)) {
					$this->Session->setFlash('Edição removida com sucesso.', 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('Ocorreu um problema com a remoção da edição. Por favor, tente novamente.');
				}
			}
		}
		
		$this->redirect('index');
	}
}
