<?php
class UsersController extends AppController {
	public $name = 'Users';
	
	public $uses = array('User');
	
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_login', 'admin_logout');
	}
	
	public function admin_login() {
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				if ($this->Auth->user('role') == 'admin') {
					$this->redirect(array('prefix' => 'admin', 'controller' => 'index'));
				} else if ($this->Auth->user('role') == 'credenciador') {
					$this->redirect(array('prefix' => 'admin', 'controller' => 'frequencias'));
				}
			} else {
				$this->Session->setFlash('Usuário ou senha inválidos.');
			}
		}
	}
	
	public function admin_logout() {
		$this->Auth->logout();
		$this->Session->setFlash('Você saiu do sistema com sucesso.');
		$this->redirect('login');
	}
	
	public function admin_new() {
		if ($this->Auth->user('id') != '1') {
			$this->redirect(array('controller' => 'index', 'action' => 'index'));
		}
		
		if ($this->request->is('post')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('Usuário criado com sucesso.', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'index', 'action' => 'index'));
			} else {
				$this->Session->setFlash('Ocorreu um problema com a criação do novo usuário. Por favor, verifique os dados e tente novamente.');
			}
		}
	}
	
	public function admin_password() {
		if ($this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash('Senha alterada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect(array('controller' => 'index', 'action' => 'index'));
			} else {
				$this->Session->setFlash('Ocorreu um problema com a alteração da senha. Por favor, verifique os dados e tente novamente.');
			}
		}
		$this->request->data = $this->User->findById($this->Auth->user('id'));
	}
}
