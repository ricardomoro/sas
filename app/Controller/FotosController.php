<?php
class FotosController extends AppController {
	public $name = 'Fotos';
	
	public $uses = array('Album', 'Foto');
	
	
	public function index() {
		$this->set('title_for_layout', 'Fotos');
		$this->set('albuns', $this->Album->find('all', array('conditions' => array('Album.parent_id' => NULL), 'recursive' => '-1')));
	}
	
	public function ver_album($id = null) {
		if ($id) {
			$this->set('album', $this->Album->findById($id));
			$this->set('pai', $this->Album->getParentNode($id));
			$this->set('filhos', $this->Album->children($id, true));
			$this->set('caminho', $this->Album->getPath($id));
		} else {
			$this->redirect('index');
		}
	}
	
	public function admin_index() {
		$albuns = $this->Album->find('all');
		
		foreach ($albuns as $key => $album) {
			$albuns[$key]['pai'] = $this->Album->getParentNode($album['Album']['id']);
		}
		
		$this->set('albuns', $albuns);
	}
	
	public function admin_ver_album($id = null) {
		if ($id) {
			$this->set('album', $this->Album->findById($id));
			$this->set('pai', $this->Album->getParentNode($id));
			$this->set('filhos', $this->Album->children($id, true));
		} else {
			$this->redirect('index');
		}
	}
	
	public function admin_novo_album() {
		if ($this->request->is('post')) {
			if ($this->Album->saveAll($this->request->data)) {
				$this->Session->setFlash('Novo álbum cadastrado com sucesso.', 'default', array('class' => 'success'));
				$this->redirect('index');
			} else {
				$this->Session->setFlash('Ocorreu um problema com o cadastro do novo álbum. Por favor, verifique os dados e tente novamente.');
			}
		}
		
		$this->set('albuns', $this->Album->generateTreeList(null, null, null, '-'));
	}
	
	public function admin_editar_album($id = null) {
		if ($this->request->is('put')) {
			if ($this->Album->saveAll($this->request->data)) {
				$this->Session->setFlash('Álbum modificado com sucesso.', 'default', array('class' => 'success'));
			} else {
				$this->Session->setFlash('Ocorreu um problema com a edição do álbum. Por favor, verifique os dados e tente novamente.');
			}
		} else {
			if ($id) {
				$this->request->data = $this->Album->findById($id);
			} else {
				$this->redirect('index');
			}
		}
		
		$this->set('albuns', $this->Album->generateTreeList(null, null, null, '-'));
	}
	
	public function admin_deletar_album($id = null) {
		if ($this->request->is('get')) {
			if ($id) {
				if ($this->Album->delete($id)) {
					$this->Session->setFlash('Álbum removido com sucesso.', 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('Ocorreu um problema com a remoção do álbum. Por favor, tente novamente.');
				}
			}
		}
		
		$this->redirect('index');
	}
	
	public function admin_nova_foto() {
		if ($this->request->is('post')) {
			if ($this->Foto->saveAll($this->request->data)) {
				$this->Session->setFlash('Nova foto cadastrada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect(array('action' => 'admin_ver_album', $this->request->data['Foto']['album_id']));
			} else {
				$this->Session->setFlash('Ocorreu um problema com o cadastro da nova foto. Por favor, verifique os dados e tente novamente.');
			}
		}
	}
	
	public function admin_deletar_foto($id = null) {
		if ($this->request->is('get')) {
			if ($id) {
				if ($this->Foto->delete($id)) {
					$this->Session->setFlash('Foto removida com sucesso.', 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('Ocorreu um problema com a remoção da foto. Por favor, tente novamente.');
				}
			}
		}
		
		$this->redirect(array('action' => 'admin_ver_album', $this->params['named']['album']));
	}
}
