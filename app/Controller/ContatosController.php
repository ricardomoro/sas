<?php
class ContatosController extends AppController {
	public $name = 'Contatos';
	
	public $uses = array('Contato');
	
	
	public function index() {
		if ($this->request->is('post')) {
			if ($this->Contato->save($this->request->data)) {
				$this->Session->setFlash('Mensagem enviada com sucesso.', 'default', array('class' => 'success'));
				$mail_headers = 'From: ' . $this->request->data['Contato']['email'] . "\r\n" .
								'Reply-To: '. $this->request->data['Contato']['email'] . "\r\n";
				if (mail('sas@ifrs.edu.br', '[Contato do Site] ' . $this->request->data['Contato']['assunto'], $this->request->data['Contato']['mensagem'], $mail_headers)) {
					$this->request->data['Contato']['id'] = $this->Contato->id;
					$this->request->data['Contato']['enviado'] = 'S';
					$this->Contato->save($this->request->data);
				}
			} else {
				$this->Session->setFlash('Ocorreu um problema com o envio do seu contato. Por favor, verifique os dados e tente novamente.');
			}
			unset($this->request->data);
		}
		$this->set('title_for_layout', 'Contato');
	}
	
	public function admin_index() {
		$contatos = $this->Contato->find('all', array('order' => array('Contato.id DESC')));
		
		$this->set('contatos', $contatos);
	}

	public function admin_deletar($id = null) {
		if ($id) {
			if ($this->Contato->delete($id)) {
					$this->Session->setFlash('O Contato foi removido com sucesso.', 'default', array('class' => 'success'));
					$this->redirect('index');
				} else {
					$this->Session->setFlash('Ocorreu um problema com a remoção do contato. Por favor, tente novamente.');
				}
		} else {
			$this->redirect('index');
		}
	}
}
