<?php
class FrequenciasController extends AppController {
	public $name = 'Frequencias';
	
	public $uses = array('Frequencia', 'Participante');
	
	
	public function isAuthorized($user = null) {
        if (isset($this->request->params['admin'])) {
            return (bool)($user['role'] === 'credenciador');
        }
		
		parent::isAuthorized($user);
    }
	
	
	public function admin_index() {
		if ($this->request->is('post')) {
			$participante = $this->Participante->find('first', array('conditions' => array('inscricao' => $this->data['Participante']['inscricao'], 'confirmado' => 'S')));
			if ($participante) {
				$this->set('participante', $participante);
				$this->render('admin_ver');
			} else {
				$this->Session->setFlash('Participante não encontrado ou inscrição cancelada. Por favor, verifique o número de inscrição e tente novamente.');
			}
		}
	}
	
	public function admin_registrar() {
		if (strtotime(date('Y-m-d')) < strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['inicio']))))
		|| strtotime(date('Y-m-d')) > strtotime(implode("-", array_reverse(explode("/", $this->edicao_atual['Edicao']['fim']))))) {
			$this->Session->setFlash('A edição atual ainda não iniciou ou já terminou. As frequências só podem ser registradas durante o evento.');
			$this->redirect('index');
		}
		
		App::import('Helper', 'Time');
		$view = new View($this);
		$timeHelper = $view->loadHelper('Time');
		
		if ($this->request->is('post')) {
			if ($this->data['Frequencia']['etapa'] == '1') {
				$participante = $this->Participante->find('first', array('conditions' => array('inscricao' => $this->data['Participante']['inscricao'], 'confirmado' => 'S')));
				if ($participante) {
					$this->set('participante', $participante);
					$this->render('admin_registrar2');
				} else {
					$this->Session->setFlash('Participante não encontrado ou inscrição cancelada. Por favor, verifique o número de inscrição e tente novamente.');
				}
			} else if ($this->data['Frequencia']['etapa'] == '2') {
				if ($this->Frequencia->saveAssociated($this->data)) {
					$this->Frequencia->read();
					if ($this->Frequencia->data['Frequencia']['entrada_saida'] == 'E') {
						$this->Session->setFlash('Entrada registrada com sucesso em '. $timeHelper->format('d/m/Y - H:i:s', $this->Frequencia->data['Frequencia']['data_hora']) .'.', 'default', array('class' => 'success'));
					} else {
						$this->Session->setFlash('Saída registrada com sucesso em '. $timeHelper->format('d/m/Y - H:i:s', $this->Frequencia->data['Frequencia']['data_hora']) .'.', 'default', array('class' => 'success'));
					}
				} else {
					$this->Session->setFlash('Ocorreu um problema com o registro da frequência. Por favor, verifique os dados e tente novamente.');
				}
			}
		}
	}
}
