<?php
class MobileController extends AppController {
	public $name = 'Mobile';
	
	public $uses = '';
	
	
	public function beforeFilter() {
		parent::beforeFilter();
		if ($this->request->is('mobile')) {
			$this->layout = 'mobile';
		} else {
			$this->redirect(array('controller' => 'index'));
		}
	}
	
	
	public function index() {
		
	}
}
