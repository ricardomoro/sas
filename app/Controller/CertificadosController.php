<?php
class CertificadosController extends AppController {
	public $name = 'Certificados';
	
	public $uses = array('Participante', 'Edicao', 'Certificado');
	
	
	public function index() {
		if ($this->request->is('post')) {
			if (isset($this->request->data['Certificado'])) {
				$this->Certificado->set($this->request->data);
				if ($this->Certificado->validates()) {
					if (isset($this->request->data['Certificado']['registro'])) {
						$numero_edicao = substr($this->request->data['Certificado']['registro'], 0, 2);
						$numero_inscricao = substr($this->request->data['Certificado']['registro'], 2, 10);

						$participante = $this->Participante->find('first', array('recursive' => '0', 'conditions' => array('inscricao' => $numero_inscricao, 'certificado' => 'S', 'Edicao.numero' => $numero_edicao)));
						if ($participante) {
							$this->Session->setFlash('Registro '.$this->request->data['Certificado']['registro'].' validado com sucesso.', 'default', array('class' => 'success'));
							$this->set('participante', $participante);
							$this->render('resultado_valida_certificado');
						} else {
							$this->Session->setFlash('Registro inválido. Por favor, verifique o número de registro no verso do certificado.');
						}
					} else if (isset($this->request->data['Certificado']['siape'])) {
						$siape = $this->request->data['Certificado']['siape'];
						$edicao_id = $this->request->data['Certificado']['edicao'];

						$participante = $this->Participante->find('first', array('recursive' => '0', 'conditions' => array('siape' => $siape, 'certificado' => 'S', 'edicao_id' => $edicao_id)));

						if ($participante) {
							$this->Session->setFlash('Certificado encontrado. Acesse o link abaixo para baixar o certificado.', 'default', array('class' => 'success'));
							$this->set('data', $this->getDataExtensa());
							$this->set('participantes', array($participante));
							$this->render('resultado_consulta_certificados');
						} else {
							$this->Session->setFlash('Nenhum certificado encontrado para a matrícula SIAPE informada, na edição selecionada. Por favor, verifique os dados e tente novamente.');
						}
					}
				} else {
					$this->Session->setFlash('Ocorreu um erro. Por favor, verifique os dados e tente novamente.');
				}
			}
		}

		$edicoes = $this->Edicao->find('list', array('fields' => array('id', 'numero')));
		
		// Adiciona o sinal ordinal feminino no número da edição para ficar mais inteligível para o usuário.
		foreach ($edicoes as $key => $value) {
			$edicoes[$key] = $value.'ª edição';
		}

		$this->set('edicoes', $edicoes);
	}

	public function gerar_certificado($id = null) {
		if ($id) {
			$this->layout = 'pdf';
			$this->set('data', $this->getDataExtensa());
			$participante = $this->Participante->findById($id);
			$participantes = array();
			$participantes[] = $participante;
			$this->set('participantes', $participantes);
			$this->render('../Participantes/certificado');
			$this->response->download($participante['Participante']['nome_completo'] . ' - Certificado.pdf');
		}
	}
}