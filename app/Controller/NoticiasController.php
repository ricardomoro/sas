<?php
class NoticiasController extends AppController {
	public $name = 'Noticias';
	
	public $uses = array('Noticia', 'Imagem');
	
	
	public function beforeFilter() {
		parent::beforeFilter();
		$this->Auth->allow('admin_ajax_adicionar_nova_imagem');
	}
	
	
	public function index() {
		$this->set('title_for_layout', 'Notícias');
		
		$noticias = $this->Noticia->find('all', array('conditions' => array('Noticia.publicar' => 'S'), 'order' => 'publicado_em DESC'));
		
		foreach ($noticias as $key => $noticia) {
			if (strlen($noticia['Noticia']['conteudo']) > 150) {
				$noticias[$key]['Noticia']['conteudo'] = $this->truncate($noticia['Noticia']['conteudo'], 150, '...', true, true);
			}
		}
		
		$this->set('noticias', $noticias);
	}
	
	public function ver($id = null) {
		if ($id) {
			$noticia = $this->Noticia->findById($id);
			$this->set('title_for_layout', 'Notícia - '.$noticia['Noticia']['titulo']);
			$this->set('noticia', $noticia);
		} else {
			$this->redirect('index');
		}
	}
	
	
	public function admin_index() {
		$noticias = $this->Noticia->find('all', array('order' => 'publicado_em DESC'));
		$this->set('noticias', $noticias);
	}
	
	public function admin_ver($id = null) {
		if ($id) {
			$this->set('noticia', $this->Noticia->findById($id));
		} else {
			$this->redirect('index');
		}
	}
	
	public function admin_nova() {
		if ($this->request->is('post')) {
			if ($this->Noticia->saveAll($this->request->data)) {
				$this->Session->setFlash('Nova notícia cadastrada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect('index');
			} else {
				$this->Session->setFlash('Ocorreu um problema com o cadastro da nova notícia. Por favor, verifique os dados e tente novamente.');
			}
		}
	}
	
	public function admin_editar($id = null) {
		if ($this->request->is('put')) {
			if (isset($this->request->data['Deletar'])) {
				foreach ($this->request->data['Deletar'] as $key => $value) {
					$this->Imagem->delete($value);
				}
			}
			
			if ($this->Noticia->saveAll($this->request->data)) {
				$this->Session->setFlash('Notícia modificada com sucesso.', 'default', array('class' => 'success'));
				$this->redirect('index');
			} else {
				$this->Session->setFlash('Ocorreu um problema com a edição da notícia. Por favor, verifique os dados e tente novamente.');
			}
		} else {
			if ($id) {
				$this->request->data = $this->Noticia->findById($id);
			} else {
				$this->redirect('index');
			}
		}
	}
	
	public function admin_deletar($id = null) {
		if ($this->request->is('get')) {
			if ($id) {
				if ($this->Noticia->delete($id)) {
					$this->Session->setFlash('Notícia removida com sucesso.', 'default', array('class' => 'success'));
				} else {
					$this->Session->setFlash('Ocorreu um problema com a remoção da notícia. Por favor, tente novamente.');
				}
			}
		}
		
		$this->redirect('index');
	}
	
	
	public function admin_ajax_adicionar_nova_imagem() {
		if ($this->request->is('ajax')) {
			$this->autoRender = false;
			$this->layout = 'ajax';
			$this->render('/Elements/forms/imagem');
		} else {
			$this->redirect('index');
		}
	}
}
