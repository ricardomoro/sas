<?php
class IndexController extends AppController {
	public $name = 'Index';
	
	public $uses = array('Noticia', 'Imagem', 'Participante', 'Foto', 'Album');
	
	
	public function index() {
		$this->set('title_for_layout', 'Página Inicial');
		
		$noticias = $this->Noticia->find('all', array('conditions' => array('Noticia.publicar' => 'S'), 'limit' => 5, 'order' => 'publicado_em DESC'));
		
		foreach ($noticias as $key => $noticia) {
			if (strlen($noticia['Noticia']['conteudo']) > 200) {
				$noticias[$key]['Noticia']['conteudo'] = $this->truncate($noticia['Noticia']['conteudo'], 200, '...', true, true);
			}
		}
		
		$this->set('noticias', $noticias);
	}
	
	
	public function admin_index() {
		if ($this->limparUploads()) {
			$this->Session->setFlash('Alguns arquivos não utilizados foram removidos!', 'default', array('class' => 'success'));
		}
		
		if ($this->limparRespostasInuteis()) {
			$this->Session->setFlash('Algumas respostas consideradas irrelevantes para "Restrição Alimentar" e "Necessidade Especial" foram removidas!', 'default', array('class' => 'success'));
		}
		
		$fotos = $this->Foto->find('count', array('recursive' => '-1'));
		$this->set('fotos', $fotos);
		
		$albuns = $this->Album->find('count', array('recursive' => '-1'));
		$this->set('albuns', $albuns);
		
		$noticias = $this->Noticia->find('count', array('conditions' => array('Noticia.publicar' => 'S'), 'recursive' => '-1'));
		$this->set('noticias', $noticias);
		
		$noticias2 = $this->Noticia->find('count', array('conditions' => array('Noticia.publicar' => 'N'), 'recursive' => '-1'));
		$this->set('noticias2', $noticias2);
		
		$inscritos = $this->Participante->find('count', array('conditions' => array('Participante.confirmado' => 'S'), 'recursive' => '-1'));
		$this->set('inscritos', $inscritos);
		
		$inscricoes_por_dia = $this->Participante->query(
			' SELECT COUNT(Participante.data) AS count, DATE(Participante.data) AS dia ' .
			' FROM participantes Participante ' .
			' WHERE edicao_id = "'. $this->edicao_atual['Edicao']['id'] . '" ' .
			' AND data IS NOT NULL ' .
			' GROUP BY dia ' .
			' ORDER BY dia ASC; '
		);
		$this->set('inscricoes_por_dia', $inscricoes_por_dia);
	}
	
	private function limparUploads() {
		$files = scandir('files/uploads');
		$imagens = $this->Imagem->find('all', array('recursive' => '-1'));
		$fotos = $this->Foto->find('all', array('recursive' => '-1'));
		
		$limpou = false;
		
		foreach ($files as $file) {
			if ($file == '.' || $file == '..') {
				continue;
			}
			
			$deletar = true;
			
			foreach ($imagens as $imagem) {
				if ($imagem['Imagem']['arquivo'] == $file) {
					$deletar = false;
				}
			}

			foreach ($fotos as $foto) {
				if ($foto['Foto']['arquivo'] == $file) {
					$deletar = false;
				}
			}
			
			if ($deletar) {
				unlink('files/uploads/'.$file);
				$limpou = true;
			}
		}
		
		return $limpou;
	}
	
	private function limparRespostasInuteis() {
		$respostas_inuteis = array(
			'nao',
			'nao.',
			'não',
			'não.',
			'n',
			'n.',
			'não se aplica',
			'não se aplica.',
			'-',
			'.',
			'nÃo', // o PHP não consegue transformar o Ã em "lower".
			'nÃo.', // o PHP não consegue transformar o Ã em "lower".
		);
		
		$participantes = $this->Participante->find('all', array('recursive' => '-1'));
		
		$limpou = false;
		
		foreach ($participantes as $key => $value) {
			foreach ($respostas_inuteis as $k => $v) {
				if (trim(strtolower($value['Participante']['restricao_alimentar'])) == $v) {
					$value['Participante']['restricao_alimentar'] = '';
					$this->Participante->save($value);
					$limpou = true;
				}
				
				if (trim(strtolower($value['Participante']['necessidade_especial'])) == $v) {
					$value['Participante']['necessidade_especial'] = '';
					$this->Participante->save($value);
					$limpou = true;
				}
			}
		}
		
		return $limpou;
	}
}
