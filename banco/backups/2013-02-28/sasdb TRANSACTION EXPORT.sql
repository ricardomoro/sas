-- phpMyAdmin SQL Dump
-- version 3.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 28, 2013 at 11:19 AM
-- Server version: 5.1.67
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT=0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sasdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `albuns`
--

CREATE TABLE IF NOT EXISTS `albuns` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `created` datetime DEFAULT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `lft` int(10) unsigned DEFAULT NULL,
  `rght` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_albuns_albuns1_idx` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `albuns`
--

INSERT INTO `albuns` (`id`, `nome`, `created`, `parent_id`, `lft`, `rght`) VALUES
(1, 'Preparativos para 2º SAS', '2013-02-25 14:06:53', NULL, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `apresentacoes`
--

CREATE TABLE IF NOT EXISTS `apresentacoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `apresentacoes`
--

INSERT INTO `apresentacoes` (`id`, `tipo`) VALUES
(1, 'Vernissage'),
(2, 'Musical'),
(3, 'Literária'),
(4, 'Outro');

-- --------------------------------------------------------

--
-- Table structure for table `atividades`
--

CREATE TABLE IF NOT EXISTS `atividades` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `atividades`
--

INSERT INTO `atividades` (`id`, `nome`, `descricao`) VALUES
(1, 'Relator', 'Responsável pelo relato das discussões e colocações decorrentes da palestra.'),
(2, 'Assistente de mesa', 'Auxiliará o coordenador de mesa e/ou palestrante no acesso aos questionamentos, à plateia, entre outros.'),
(3, 'Voluntário', 'Estará disponível para auxiliar a Comissão Organizadora nos dias do seminário.');

-- --------------------------------------------------------

--
-- Table structure for table `campus`
--

CREATE TABLE IF NOT EXISTS `campus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `campus`
--

INSERT INTO `campus` (`id`, `nome`) VALUES
(1, 'Reitoria'),
(2, 'Bento Gonçalves'),
(3, 'Canoas'),
(4, 'Caxias do Sul'),
(5, 'Erechim'),
(6, 'Farroupilha'),
(7, 'Feliz'),
(8, 'Ibirubá'),
(9, 'Osório'),
(10, 'Porto Alegre'),
(11, 'Restinga'),
(12, 'Rio Grande'),
(13, 'Sertão');

-- --------------------------------------------------------

--
-- Table structure for table `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `assunto` varchar(255) NOT NULL,
  `mensagem` text NOT NULL,
  `enviado` varchar(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `contatos`
--

INSERT INTO `contatos` (`id`, `nome`, `email`, `assunto`, `mensagem`, `enviado`) VALUES
(2, 'Claudia da Silva Gomes', 'claudia.gomes@poa.ifrs.edu.br', 'Certificado de participação no 1º SAS', 'Olá! Por favor, qual o procedimento para solicitar meu certificado de participação no 1º SAS? Aguardo retorno. Att,\r\nClaudia ', 'S'),
(3, 'Leila de Almeida Castillo Iabel', 'leila.iabel@sertao.ifrs.edu.br', 'temas de discussão', 'Olá!\r\nSou professora de educação física do câmpus Sertão e em nossa área temos tido uma grande dificuldade em nos reunir. Gostaria de saber se haverá momentos de encontro por área, ou se haverá algum encontro de intgração esportiva. Sugiro o voleibol pela possibilidade do jogo misto. Seria interessante divulgar para que os colegas e as colegas se preparassem para isso. \r\nE se houver a possibilidade de um encontro entre os professores e as professoras de educação física do IF gostaria de saber para poder incentivar os colegas à inscrição. Temos eventos de nível de IF, entre os IFs do estado, eventos de região sul e eventos nacionais e nunca se fez uma reunião específica para isso. Os alunos e alunas esperam o ano inteiro, treinam o ano inteiro para participarem dos jogos e nesse ano sequer aconteceram. \r\nEspero que compreendam e nos deem essa força.\r\nAbraços.\r\nLeila', 'S'),
(4, 'Andréia Pruinelli', 'andreia.pruinelli@canoas.ifrs.edu.br', 'Informações', 'Olá, equipe do SAS!\r\n\r\nJá está definido o local em que ocorrerá o evento? Como será feito o pagamento? Receberemos diárias?\r\n\r\nObrigada!\r\n\r\n', 'S'),
(5, 'Aline Silva De Bona', 'aline.bona@poa.ifrs.edu.br', 'inscriçã ok', 'Oi, podem confirmar se minha inscrição foi feita corretamente? Pois fiz no site e não da nenhhum aviso de conformação.\r\nObrigada, e aguardo.', 'S'),
(6, 'Gabriele Albuquerque Silva', 'gabriele.silva@sertao.ifrs.br', 'Inscrição', 'Olá\r\nSou servidora no instituto, no campus Sertão, e gostaria de participar do evento. No entanto, como estou chegando essa semana e as inscrições terminam logo queria ver como posso me inscrever. A questão é que ainda não recebi a matrícula siape, pois tomei posse agora essa semana\r\nDesde já agradeço a atenção\r\n', 'S'),
(7, 'Javier García López', 'javier.garcia@riogrande.ifrs.edu.br', 'Consulta', 'Gostaria verificar se a minha inscrição foi realizada, e senão, como posso efetivá-la. \r\n\r\nGrato, ', 'S'),
(8, 'Ana Letícia Franzon Ceconello', 'ana.ceconello@sertao.ifrs.edu.br', 'inscrição', 'Olá, estava em período de férias e não realizei minha inscrição, ainda consigo fazê-la?', 'S'),
(9, 'Marli Daniel', 'marli.daniel@ifrs.edu.br', 'Dúvida sobre hospedagem', 'Olá!\r\nMe ocorreu a seguinte dúvida sobre hospedagem durante o SAS. Ocorre que atualmente estou lotada na Reitoria e permaneço em BG durante a semana hospedada numa pensão, estou aguardando um pedido de remoção para me mudar definitivamente para Erechim. No momento da inscrição não existe a opção para informar se necessitaremos de hospedagem, acredito que vocês estão se baseando pela lotação. Gostaria de saber como proceder pois provavelmente até a data do evento a minha situação tenha mudado e minha lotação estará no campus Erechim.\r\nNo aguardo.\r\nAtt,\r\nMarli', 'S'),
(10, 'Ademilde Irene Petzold Prado', 'ademilde.prado@restinga.ifrs.edu.br', 'Inscrição SAS', 'Boa tarde.\r\n\r\nGostaria muito de participar do 2º SAS. Como tomei posse e entrei em exercício somente ontem (04/02), não pude efetuar a inscrição dentro do prazo estabelecido.\r\nIsto posto, gostaria de saber se ainda é possível me inscrever .\r\n\r\nAtenciosamente,\r\n\r\nAdemilde Prado\r\nAssitente Social - CRESS 9620\r\nIFRS/Campus Restinga\r\n\r\nRua 7121, Loteamento Industrial da Restinga,\r\nLote 16, Quadra F \r\nBairro Restinga - CEP: 91795-130 - Porto Alegre/RS\r\n', 'S'),
(11, 'Claudio Javier Almiron', 'claudio.almiron@gmail.com', 'Inscrição', 'Olá! Acabei perdendo o prazo pra me inscrever. Se tiver como me inscrever ainda eu gostaria de participar. Obrigado', 'S'),
(12, 'Cintia Stocchero', 'cintia.stocchero@restinga.ifrs.edu.br', 'Inscrição no SAS', 'Caros organizadores do SAS:\r\n\r\nSou professora do IFRS, Campus Restinga, mas estive o último ano em exercício provisório no IFRJ. Retornei minhas atividades no Campus REstinga nessa semana e tenho interesse em participar do SAS. Seria possível realizar minha inscrição ainda?\r\n\r\nAtt,', 'S'),
(13, 'IVÂNIA DOS SANTOS LAGO', 'ivania.lago@ifrs.edu.br', 'INSCRIÇÃO PARA O 2º SAS', 'OLÁ,\r\nGOSTARIA DE REALIZAR A MINHA INSCRIÇÃO PARA O 2º SAS, SALIENTANDO QUE ENTREI EM EFETIVO EXERCÍCIO NO IFRS NO DIA 14/02/2013, NO CARGO DE TÉCNICO EM ASSUNTOS EDUCACIONAIS/PROEN.\r\nCONFORME SOLICTADO, MINHA MATRÍCULA NO SIAPE É 1995597.\r\nAGUARDO RETORNO DA INSCRIÇÃO.\r\nATENCIOSAMENTE,\r\nIVÂNIA', 'S'),
(14, 'DENISE LUZIA WOLFF', 'denise.wolff@poa.ifrs.edu.br', 'Desistência', 'Prezados Organizadores, infelizmente solicito o cancelamento de minha inscrição no II SAS.', 'S'),
(15, 'Fernando Hoefling dos Santos', 'fernando.santos@farroupilha.ifrs.edu.br', 'Participação dos novos servidores', 'Boa tarde.\r\n\r\nGostaria de saber se há possibilidade, dos novos servidores como eu, de participar do 2º SAS.\r\n\r\nJá por ser ingressante, tenho uma grande necessidade de interar-me com os assuntos do IFRS.\r\n\r\nDesde já agradeço a atenção.\r\nFernando Hoefling dos Santos ', 'S'),
(16, 'Marcelo Augusto Rauh Schmitt', 'marcelo.schmitt@poa.ifrs.edu.br', 'estadia', 'Caros\r\n\r\nProvavelmente eu estou desinformado e peço desculpas. Mas sempre é tempo de informar-se. Como é a questão dos hotéis para o SAS? Cada um deve definir onde vai ficar e arcar com os custos? Haverá alguma ação da organização para redução de custos? \r\n\r\nParticiparei também como integrante de uma mesa.\r\n\r\nMuito obrigado.\r\n\r\nMarcelo A. Rauh Schmitt', 'S'),
(17, 'Andréia', 'andmein@gmail.com', 'inscrição', 'Vacilei, perdi o prazo da inscrição pois estava viajando, sem acesso a internet por uns dias, me confundi nos prazos. Que pena!\r\nSerá que há possibilidade ainda de abrir novamente as inscrições?\r\nObrigada!!', 'S');

-- --------------------------------------------------------

--
-- Table structure for table `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `creditos` varchar(255) DEFAULT NULL,
  `album_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_fotos_albuns1_idx` (`album_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `fotos`
--

INSERT INTO `fotos` (`id`, `titulo`, `arquivo`, `descricao`, `creditos`, `album_id`) VALUES
(1, 'Márcia Amaral palestrará sobre identidade, protagonismo e transformação social', '458a3106805a55d6757f3d5490b09c78.jpg', 'Márcia Amaral palestrará sobre identidade, protagonismo e transformação social', 'Cristine Thomas', 1);

-- --------------------------------------------------------

--
-- Table structure for table `imagens`
--

CREATE TABLE IF NOT EXISTS `imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `arquivo` varchar(255) NOT NULL,
  `noticia_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_imagens_noticias1_idx` (`noticia_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `imagens`
--

INSERT INTO `imagens` (`id`, `titulo`, `arquivo`, `noticia_id`) VALUES
(2, 'Márcia Amaral é Diretora de Ensino no Câmpus Porto Alegre do IFRS', 'f3b5407f5358162a14cf03da313637e6.jpg', 3);

-- --------------------------------------------------------

--
-- Table structure for table `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `conteudo` text NOT NULL,
  `publicar` char(1) NOT NULL DEFAULT 'N',
  `publicado_em` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `conteudo`, `publicar`, `publicado_em`, `created`, `modified`) VALUES
(1, 'Definidas as temáticas para o II Seminário Anual dos Servidores do IFRS', '<div><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">Foram definidas nesta ter&ccedil;a-feira, dia 15 de janeiro, as tem&aacute;ticas principais do II Semin&aacute;rio&nbsp;Anual dos Servidores do IFRS.</span></div>\r\n<div><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">Confira:</span></div>\r\n<div>&nbsp;</div>\r\n<ul>\r\n<li><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">As Pol&iacute;ticas de Inclus&atilde;o e a Extens&atilde;o no IFRS: Experi&ecirc;ncias, Possibilidades e Desafios</span></li>\r\n<li><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">O trabalho como viv&ecirc;ncia e a Institui&ccedil;&atilde;o que queremos</span></li>\r\n<li><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">Possibilidades e desafios de uma Institui&ccedil;&atilde;o multic&acirc;mpus: como construir a unidade?</span></li>\r\n<li><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">Pesquisa, P&oacute;s-gradua&ccedil;&atilde;o e Internacionaliza&ccedil;&atilde;o no IFRS: Experi&ecirc;ncias,</span><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;"> Possibilidades e </span><span style="color: #222222; font-family: arial,sans-serif; font-size: 13px;">Desafios</span></li>\r\n</ul>\r\n<div>&nbsp;</div>\r\n<div>&nbsp;</div>', 'S', '2013-01-16 13:39:25', '2013-01-16 13:06:01', '2013-01-16 13:39:25'),
(2, 'Pelo menos 657 servidores estarão no evento de 17 a 19 de abril', '<p>Ap&oacute;s uma prorroga&ccedil;&atilde;o de tr&ecirc;s dias, as inscri&ccedil;&otilde;es para o II Semin&aacute;rio Anual de Servidores (SAS) do IFRS se encerraram nesta segunda-feira, dia 28 de janeiro. Est&atilde;o inscritos 657 servidores de todos os c&acirc;mpus e da Reitoria.</p>\r\n<p>&nbsp;</p>\r\n<p>A partir de agora, somente poder&atilde;o se inscrever novos servidores que entrarem em efetivo exerc&iacute;cio a partir do encerramento das inscri&ccedil;&otilde;es at&eacute; o dia 17 de abril, primeiro dia do evento. Para isso, &eacute; necess&aacute;rio entrar em contato com a Comiss&atilde;o Organizadora pelo email sas@ifrs.edu.br, enviando nome completo, matr&iacute;cula no Siape e e-mail institucional.</p>\r\n<p>&nbsp;</p>', 'S', '2013-02-25 10:21:42', '2013-01-30 09:23:42', '2013-02-25 10:21:42'),
(3, 'Identidade, protagonismo e transformação social será tema de palestra de Márcia Amaral', '<blockquote class="western" style="padding: 0cm; border: currentColor; text-align: left; margin-right: 0cm; margin-bottom: 0cm; margin-left: 0cm;">\r\n<p><span style="color: #000000;"><span style="font-family: Verdana, sans-serif;"><span style="font-size: small;">Uma abordagem sobre o papel do IFRS na sociedade, da singularidade de sua miss&atilde;o e atua&ccedil;&atilde;o, e do protagonismo dos servidores na constru&ccedil;&atilde;o institucional afinada aos seus prop&oacute;sitos hist&oacute;ricos e legais. Essa ser&aacute; a linha de abordagem do tema <em>Identidade, protagonismo e transforma&ccedil;&atilde;o social</em> em palestra a ser ministrada por M&aacute;rcia Amaral no II Semin&aacute;rio Anual dos Servidores do IFRS.</span></span></span></p>\r\n<p><span style="color: #000000;"><span style="font-family: Verdana, sans-serif;"><span style="font-size: small;"> &ndash; Farei uma fala afetiva, de convoca&ccedil;&atilde;o dos servidores para vestirem a camiseta do IFRS &ndash; antecipa a palestrante.</span></span></span></p>\r\n<p><span style="color: #000000;"><span style="font-family: Verdana, sans-serif;"><span style="font-size: small;">M&aacute;rcia Amaral &eacute; graduada em Pedagogia (UFRGS/1993), possui mestrado em Hist&oacute;ria e Filosofia da Educa&ccedil;&atilde;o (USP/1998) e doutorado em Psicologia e Educa&ccedil;&atilde;o (USP/2003). Atualmente &eacute; professora e Diretora de Ensino do C&acirc;mpus Porto Alegre do IFRS. M&aacute;rcia tamb&eacute;m lidera o grupo de pesquisa <em>Educa&ccedil;&atilde;o, inova&ccedil;&atilde;o e trabalho</em> e suas investiga&ccedil;&otilde;es se centram majoritariamente na linha de pesquisa <em>Estudos em pol&iacute;ticas e pr&aacute;ticas de Educa&ccedil;&atilde;o</em>. Tem experi&ecirc;ncia na &aacute;rea de educa&ccedil;&atilde;o, atuando principalmente em temas como forma&ccedil;&atilde;o de professores, pr&aacute;ticas de negocia&ccedil;&atilde;o e solu&ccedil;&atilde;o de conflitos como ferramentas educacionais, atua&ccedil;&atilde;o psicopedag&oacute;gica institucional, autorregula&ccedil;&atilde;o da aprendizagem, avalia&ccedil;&atilde;o e forma&ccedil;&atilde;o profissional.</span></span></span></p>\r\n</blockquote>', 'S', '2013-02-25 17:17:06', '2013-02-25 10:17:20', '2013-02-25 17:17:06');

-- --------------------------------------------------------

--
-- Table structure for table `obras`
--

CREATE TABLE IF NOT EXISTS `obras` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `participante_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_obras_participantes1_idx` (`participante_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `obras`
--

INSERT INTO `obras` (`id`, `titulo`, `tipo`, `participante_id`) VALUES
(1, 'Acessibilidade e Tecnologia Assistiva: pensando a inclusão sociodigital de Pessoas com Necessidades Especiais', 'Livro', 4),
(2, 'qualquer uma', 'revista', 7),
(3, 'nao', 'nao', 33),
(4, 'A cultura brasileira e a sexualidade na TV', 'Livro', 41),
(5, 'Não', 'Nenhuma', 162),
(6, 'Não', 'Não', 185),
(7, 'Matemática Financeira e Estatística', 'Livro ', 199),
(8, 'Técnico em Administração - Gestão e Negócios', 'Livro (Selo Tekne - Grupo A - em parceria com o IFRS)', 206),
(9, 'Mística e MST', 'Livro', 215),
(10, 'Decifrando documentos no Processo Gerencial: uma abordagem linguística, jurídica e contábil', 'livro', 384),
(11, 'Enoturismo', 'Livro', 443);

-- --------------------------------------------------------

--
-- Table structure for table `participantes`
--

CREATE TABLE IF NOT EXISTS `participantes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome_completo` varchar(255) NOT NULL COMMENT 'Nome completo para certificação',
  `siape` varchar(255) NOT NULL COMMENT 'Matrícula do SIAPE',
  `email` varchar(255) NOT NULL COMMENT 'Email institucional',
  `nome_cracha` varchar(255) NOT NULL COMMENT 'Nome que aparece no crachá de identificação',
  `necessidade_especial` varchar(255) DEFAULT NULL,
  `restricao_alimentar` varchar(255) DEFAULT NULL COMMENT 'Restrições alimentares',
  `confirmado` varchar(1) NOT NULL DEFAULT 'S' COMMENT 'Confirmação de participação no evento (S/N)',
  `participacao` varchar(1) DEFAULT NULL COMMENT 'Se participou do evento (S/N)',
  `presente` varchar(1) DEFAULT NULL COMMENT 'Se atingiu 75% de presença (S/N)',
  `data` datetime NOT NULL,
  `campus_id` int(10) unsigned NOT NULL COMMENT 'Campus de origem',
  `atividade_id` int(10) unsigned DEFAULT NULL COMMENT 'Atividade especial que desempenhará',
  `apresentacao_id` int(10) unsigned DEFAULT NULL COMMENT 'Apresentação cultural que realizará',
  PRIMARY KEY (`id`),
  KEY `fk_participantes_campus_idx` (`campus_id`),
  KEY `fk_participantes_atividades1_idx` (`atividade_id`),
  KEY `fk_participantes_apresentacoes1_idx` (`apresentacao_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=682 ;

--
-- Dumping data for table `participantes`
--

INSERT INTO `participantes` (`id`, `nome_completo`, `siape`, `email`, `nome_cracha`, `necessidade_especial`, `restricao_alimentar`, `confirmado`, `participacao`, `presente`, `data`, `campus_id`, `atividade_id`, `apresentacao_id`) VALUES
(1, 'Ricardo Moro', '1979742', 'ricardo.moro@ifrs.edu.br', 'Ricardo Moro', '', '', 'S', NULL, NULL, '2012-12-18 09:52:28', 1, NULL, NULL),
(2, 'Adriana Pancotto', '016778545', 'adriana.pancotto@ifrs.edu.br', 'Adri', '', 'Intolerância a lactose', 'S', NULL, NULL, '2012-12-18 10:09:02', 1, NULL, NULL),
(3, 'Andréa Marta Bergonci Camargo', '011023015', 'andrea.camargo@ifrs.edu.br', 'Andréa', '', '', 'S', NULL, NULL, '2012-12-18 10:13:41', 1, NULL, NULL),
(4, 'Andréa Poletto Sonza', '1102836', 'andrea.sonza@ifrs.edu.br', 'Andréa Poletto Sonza', '', '', 'S', NULL, NULL, '2012-12-18 10:21:28', 1, NULL, NULL),
(5, 'Viviane Campanhola Bortoluzzi', '1630209', 'viviane.bortoluzzi@ifrs.edu.br', 'Viviane Bortoluzzi', '', '', 'S', NULL, NULL, '2012-12-18 10:24:59', 1, NULL, NULL),
(6, 'Agostinho Luís Agostini', '1827899', 'agostinho.agostini@ifrs.edu.br', 'Guto Agostini', '', '', 'S', NULL, NULL, '2012-12-18 11:16:55', 1, NULL, 2),
(7, 'silvia schiedeck', '1760729', 'silvia.schiedeck@ifrs.edu.br', 'silvia', '', '', 'S', NULL, NULL, '2012-12-18 13:47:03', 1, 3, 1),
(8, 'Edval Moya Lopes', '1843286', 'edval.moya@feliz.ifrs.edu.br', 'Edval Moya', '', '', 'S', NULL, NULL, '2012-12-18 18:43:08', 7, NULL, NULL),
(9, 'Rogerio Foschiera', '1864493', 'rogerio.foschiera@feliz.ifrs.edu.br', 'Rogerio Foschera', '', '', 'S', NULL, NULL, '2012-12-18 18:45:14', 7, NULL, NULL),
(10, 'Alexandra Andreis', '1818994', 'alexandra.andreis@ifrs.edu.br', 'Alexandra Andreis', '', '', 'S', NULL, NULL, '2012-12-18 18:45:59', 1, NULL, NULL),
(11, 'Maurício Soares Ortiz', '1718434', 'mauricio.ortiz@riogrande.ifrs.edu.br', 'Maurício Ortiz', '', '', 'S', NULL, NULL, '2012-12-18 18:48:51', 12, NULL, NULL),
(12, 'Catia Eli Gemelli', '2789901', 'catia.gemelli@osorio.ifrs.edu.br', 'Catia Gemelli', '', '', 'S', NULL, NULL, '2012-12-18 18:49:18', 9, NULL, NULL),
(13, 'Telmo Francisco Manfron Ojeda', '1665614', 'telmo.ojeda@poa.ifrs.edu.br', 'Telmo', '', '', 'S', NULL, NULL, '2012-12-18 18:50:12', 10, NULL, NULL),
(14, 'SILVANA BARBOSA COSTA GARCIA', '1853613', 'silvana.garcia@riogrande.ifrs.edu.br', 'SILVANA', '', '', 'S', NULL, NULL, '2012-12-18 18:51:25', 12, NULL, NULL),
(15, 'ADRIANA ROMERO LOPES', '1813413', 'adriana.lopes@bento.ifrs.edu.br', 'ADRIANA LOPES', '', '', 'S', NULL, NULL, '2012-12-18 18:54:06', 2, NULL, NULL),
(16, 'Pércio Davies Schmitz', '018134386', 'percio.schmitz@poa.ifrs.edu.br', 'Pércio', '', '', 'S', NULL, NULL, '2012-12-18 18:54:17', 10, NULL, NULL),
(17, 'Débora Della Vechia', '3723066', 'debora.dellavechia@erechim.ifrs.edu.br', 'Débora', '', '', 'S', NULL, NULL, '2012-12-18 18:55:57', 5, NULL, NULL),
(18, 'Raquel de Campos', '1737651', 'raquel.campos@erechim.ifrs.edu.br', 'Raquel de Campos', '', '', 'S', NULL, NULL, '2012-12-18 19:08:01', 5, NULL, NULL),
(19, 'Carlos Stefan Simionovski', '1817339', 'carlos.simionovski@ifrs.edu.br', 'Carlos Stefan Simionovski', '', '', 'S', NULL, NULL, '2012-12-18 19:11:22', 1, NULL, NULL),
(20, 'Bianca Smith Pilla', '1358089', 'bianca.pilla@poa.ifrs.edu.br', 'Bianca', '', '', 'S', NULL, NULL, '2012-12-18 19:11:25', 10, NULL, NULL),
(21, 'Milene Mecca Hannecker', ' 018651259', 'milene.hannecker@bento.ifrs.edu.br', 'Milene', '', '', 'N', NULL, NULL, '2012-12-18 19:20:43', 2, NULL, NULL),
(22, 'Francieli Fuchina', '1824055', 'francieli.fuchina@osorio.ifrs.edu.br', 'Francieli', '', '', 'S', NULL, NULL, '2012-12-18 19:20:46', 9, NULL, NULL),
(23, 'Régis Nogara dos Reis', '1745752', 'regis.reis@erechim.ifrs.edu.br', 'Régis Nogara dos Reis', '', '', 'S', NULL, NULL, '2012-12-18 19:24:03', 5, NULL, NULL),
(24, 'Viviane Marmentini', '1827177', 'viviane.marmentini@canoas.ifrs.edu.br', 'Viviane', '', '', 'S', NULL, NULL, '2012-12-18 19:34:17', 3, NULL, NULL),
(25, 'Patrícia Czervinski', '1816335', 'patricia.czervinski@sertao.ifrs.edu.br', 'Patrícia Czervinski', '', '', 'S', NULL, NULL, '2012-12-18 19:38:34', 13, NULL, NULL),
(26, 'Aline Silva De Bona', '018229735', 'aline.bona@poa.ifrs.edu.br', 'Aline De Bona', '', '', 'S', NULL, NULL, '2012-12-18 19:38:49', 10, NULL, NULL),
(27, 'Gustavo Simões Teixeira', '1810398', 'gustavo.teixeira@riogrande.ifrs.edu.br', 'Gustavo', '', '', 'S', NULL, NULL, '2012-12-18 19:48:53', 12, NULL, NULL),
(28, 'Miguel da Guia Albuquerque', '1687160', 'miguel.albuquerque@riogrande.ifrs.edu.br', 'Miguel Albuquerque', '', '', 'S', NULL, NULL, '2012-12-18 20:01:01', 12, NULL, NULL),
(29, 'Luciano Vargas Gonçalves', '1743775', 'luciano.goncalves@riogrande.ifrs.edu.br', 'Luciano Vargas', '', '', 'S', NULL, NULL, '2012-12-18 20:08:09', 12, 3, NULL),
(30, 'Marlon André da Silva', '1808491', 'marlon.silva@canoas.ifrs.edu.br', 'Marlon André da Silva', '', '', 'S', NULL, NULL, '2012-12-18 20:17:53', 3, NULL, NULL),
(31, 'Claudio Boezzio de Araujo', '018527655', 'claudio.araujo@poa.ifrs.edu.br', 'Claudio', 'Sim, conhecer pessoas e fazer boas amizades!', 'Sim, ficar com fome por muito tempo!', 'S', NULL, NULL, '2012-12-18 20:18:49', 10, 3, NULL),
(32, 'Gustavo de Castro Feijo', '1808286', 'gustavo.feijo@riogrande.ifrs.edu.br', 'Gustavo Feijo', '', '', 'S', NULL, NULL, '2012-12-18 20:19:26', 12, NULL, NULL),
(33, 'VINICIUS KARLINSKI DE BARCELLOS', '019026552', 'vinicius.karlinski@erechim.ifrs.edu.br', 'VINICIUS KARLINSKI', '', '', 'S', NULL, NULL, '2012-12-18 21:17:41', 5, NULL, NULL),
(34, 'Marilia Assunta Sfredo', '1737499', 'marilia.sfredo@erechim.ifrs.edu.br', 'Marilia A. Sfredo', '', '', 'S', NULL, NULL, '2012-12-18 21:20:49', 5, 3, NULL),
(35, 'Luzia Kasper', '1632683', 'luzia.kasper@canoas.ifrs.edu.br', 'Luzia', '', '', 'S', NULL, NULL, '2012-12-18 21:24:53', 3, NULL, NULL),
(36, 'Cristiane Inês Musa', '1796110', 'cristiane.musa@feliz.ifrs.edu.br', 'Cris Musa', '', '', 'S', NULL, NULL, '2012-12-18 21:24:53', 7, NULL, NULL),
(37, 'Rogério Xavier de Azambuja', '1806493', 'rogerio.xavier@farroupilha.ifrs.edu.br', 'Rogério Xavier', '', '', 'S', NULL, NULL, '2012-12-18 21:29:46', 6, NULL, NULL),
(38, 'Liana Paula Cavalett', '1819233', 'liana.cavalett@feliz.ifrs.edu.br', 'Liana Cavalett', '', '', 'S', NULL, NULL, '2012-12-18 21:31:43', 7, NULL, NULL),
(39, 'PATRÍCIA CRISTINA NIENOV', '018681621', 'patricia.nienov@erechim.ifrs.edu.br', 'PATRÍCIA NIENOV', '', '', 'S', NULL, NULL, '2012-12-18 21:37:44', 5, NULL, NULL),
(40, 'Rodrigo Dullius', '1893172', 'rodrigo.dullius@feliz.ifrs.edu.br', 'Dullius', '', '', 'S', NULL, NULL, '2012-12-18 21:37:54', 7, NULL, NULL),
(41, 'Claudio Antonio Cardoso Leite', '1751438', 'claudio.leite@canoas.ifrs.edu.br', 'Claudio Leite', '', 'Sim, açucares', 'S', NULL, NULL, '2012-12-18 21:47:08', 3, 2, 3),
(42, 'Claudia Medianeira Alves Ziegler', '018208592', 'claudia.ziegler@farroupilha.ifrs.edu.br', 'Claudia Ziegler', '', '', 'S', NULL, NULL, '2012-12-18 21:47:48', 6, NULL, NULL),
(43, 'david peres da rosa', '1798432', 'david.darosa@sertao.ifrs.edu.br', 'david peres', '', '', 'S', NULL, NULL, '2012-12-18 21:51:31', 13, NULL, NULL),
(44, 'Diana Lusa', '1743801', 'diana.lusa@bento.ifrs.edu.br', 'Diana Lusa', '', '', 'S', NULL, NULL, '2012-12-18 21:52:24', 2, NULL, NULL),
(45, 'André Bilibio Westphalen', '1791686', 'andre.westphalen@riogrande.ifrs.edu.br', 'André Westphalen', '', '', 'S', NULL, NULL, '2012-12-18 22:43:49', 12, NULL, NULL),
(46, 'Jader da Silva Neto', '1680756', 'jader.neto@bento.ifrs.edu.br', 'Jader', '', '', 'S', NULL, NULL, '2012-12-18 22:50:05', 2, NULL, NULL),
(47, 'Anelise D''Arisbo', '108241050', 'anelise.darisbo@farroupilha.ifrs.edu.br', 'Anelise', '', '', 'S', NULL, NULL, '2012-12-18 22:59:06', 6, NULL, NULL),
(48, 'Denise de Oliveira', '1447848', 'denise.oliveira@sertao.ifrs.edu.br', 'Denise', '', '', 'S', NULL, NULL, '2012-12-19 00:37:02', 13, 3, NULL),
(49, 'Tiago Belmonte Nascimento', '2612779', 'tiago.nascimento@bento.ifrs.edu.br', 'TIAGO BELMONTE', '', '', 'S', NULL, NULL, '2012-12-19 00:53:59', 2, NULL, NULL),
(50, 'Silvar Antônio Botton', '1827142', 'silvar.botton@sertao.ifrs.edu.br', 'Silvar', '', '', 'S', NULL, NULL, '2012-12-19 07:42:01', 13, 3, NULL),
(51, 'Fernanda Schneider', '1823897', 'fernanda.schneider@ibiruba.ifrs.edu.br', 'Fernanda Schneider', '', '', 'S', NULL, NULL, '2012-12-19 08:08:19', 8, NULL, NULL),
(52, 'Sabrina Christello de Lima', '1893263', 'sabrina.lima@canoas.ifrs.edu.br', 'Sabrina', '', '', 'S', NULL, NULL, '2012-12-19 08:09:13', 3, NULL, NULL),
(53, 'MÁRCIO JOSÉ DE OLIVEIRA', '1808596', 'marcio.oliveira@erechim.ifrs.edu.br', 'MARCIO', '', '', 'S', NULL, NULL, '2012-12-19 08:14:15', 5, NULL, NULL),
(54, 'Patrícia Cichota', '1828037', 'patricia.cichota@sertao.ifrs.edu.br', 'Patrícia Cichota', '', '', 'S', NULL, NULL, '2012-12-19 08:22:11', 13, NULL, NULL),
(55, 'Fabiana Lopes da Silva', '1902271', 'fabiana.silva@caxias.ifrs.edu.br', 'Fabiana', '', '', 'S', NULL, NULL, '2012-12-19 08:22:14', 4, NULL, NULL),
(56, 'PATRICIA KISNER', '1810298', 'patricia.kisner@sertao.ifrs.edu.br', 'PATRÍCIA K', '', '', 'S', NULL, NULL, '2012-12-19 08:23:20', 13, NULL, NULL),
(57, 'Lia Mar Vargas Tamanho', '0049239', 'lia.vargas@sertao.ifrs.edu.br', 'Lia', '', '', 'S', NULL, NULL, '2012-12-19 08:25:12', 13, NULL, NULL),
(58, 'Lisiane Cézar de Oliveira', '1828061', 'lisiane.oliveira@ibiruba.ifrs.edu.br', 'Lysy', '', '', 'S', NULL, NULL, '2012-12-19 08:30:17', 8, NULL, NULL),
(59, 'Marilvana Helena Bertolini Paliga', '1827135', 'marilvana.paliga@canoas.ifrs.edu.br', 'Marilvana Paliga', '', '', 'S', NULL, NULL, '2012-12-19 08:36:32', 3, NULL, NULL),
(60, 'LUIZ ANTÔNIO HINING', '1797164', 'luiz.hining@ifrs.edu.br', 'LUIZ ANTÔNIO', '', '', 'S', NULL, NULL, '2012-12-19 08:43:03', 1, NULL, NULL),
(61, 'Andréia Teixeira Inocente', '18083056', 'andreia.inocente@osorio.ifrs.edu.br', 'Andréia', '', 'Sim, intolerância a lactose.', 'S', NULL, NULL, '2012-12-19 08:44:10', 9, NULL, NULL),
(62, 'TATIANE PELLIN CISLAGHI', '017993105', 'tatiane.cislaghi@bento.ifrs.edu.br', 'TATIANE P. CISLAGHI', '', '', 'S', NULL, NULL, '2012-12-19 08:45:45', 2, NULL, NULL),
(63, 'Lindemar José Sertoli', '1213561', 'lindemar.sertoli@sertao.ifrs.edu.br', 'Lindemar', '', 'sou alergico a cebola.', 'S', NULL, NULL, '2012-12-19 08:46:34', 13, NULL, NULL),
(64, 'CRISTIANE ANCILA MICHELIN', '1808390', 'cristiane.michelin@ifrs.edu.br', 'CRISTIANE', '', '', 'S', NULL, NULL, '2012-12-19 08:46:54', 13, NULL, NULL),
(65, 'GILBERTO ROGERIO ZAGO', '1869260', 'gilberto.zago@sertao.ifrs.edu.br', 'GILBERTO ZAGO', '', '', 'S', NULL, NULL, '2012-12-19 08:50:42', 13, 3, NULL),
(66, 'RAMONE TRAMONTINI', '2858717', 'ramone.tramontini@ibiruba.ifrs.edu.br', 'RAMONE', '', '', 'S', NULL, NULL, '2012-12-19 08:52:12', 8, NULL, NULL),
(67, 'Alessandra Isnardi Lemõns', '14438534', 'alessandra.lemons@bento.ifrs.edu.br', 'Alessandra', '', '', 'S', NULL, NULL, '2012-12-19 08:58:59', 2, NULL, NULL),
(68, 'Diego Lieban', '1467895', 'diego.lieban@bento.ifrs.edu.br', 'Diego Lieban', '', '', 'S', NULL, NULL, '2012-12-19 09:00:31', 2, NULL, NULL),
(69, 'Daniel Battaglia', '018061303', 'daniel.battaglia@bento.ifrs.edu.br', 'Daniel Battaglia', '', '', 'S', NULL, NULL, '2012-12-19 09:09:21', 2, NULL, NULL),
(70, 'JULIANA FAGUNDES DOS SANTOS', '1106141', 'juliana.fagundes@sertao.ifrs.edu.br', 'JULIANA SANTOS', '', '', 'S', NULL, NULL, '2012-12-19 09:27:39', 13, NULL, NULL),
(71, 'Claudiomir Feustler Rodrigues de Siqueira', '1893341', 'claudiomir.siqueira@ibiruba.ifrs.edu.br', 'Claudiomir', '', '', 'S', NULL, NULL, '2012-12-19 09:29:45', 8, 3, NULL),
(72, 'Rudinei Müller', '1799228', 'rudinei.muller@bento.ifrs.edu.br', 'Rudinei', '', '', 'S', NULL, NULL, '2012-12-19 09:32:04', 2, NULL, NULL),
(73, 'Carine Bueira Loureiro', '015253902', 'carine.loureiro@poa.ifrs.edu.br', 'Carine Loureiro', '', '', 'S', NULL, NULL, '2012-12-19 09:33:32', 10, NULL, NULL),
(74, 'Aline Simões Menezes', '1697666', 'aline.menezes@riogrande.ifrs.edu.br', 'Aline Menezes', '', '', 'S', NULL, NULL, '2012-12-19 09:38:50', 12, NULL, NULL),
(75, 'Daiane Corrêa da Silva', '1825821', 'daiane.correa@sertao.ifrs.edu.br', 'Daiane', '', '', 'S', NULL, NULL, '2012-12-19 09:41:51', 13, NULL, NULL),
(76, 'LUIZ VICENTE KOCHE VIEIRA', '1198', 'vicente.vieira@ifrs.edu.br', 'VICENTE', '', '', 'S', NULL, NULL, '2012-12-19 09:58:01', 1, NULL, NULL),
(77, 'TÂNIA REGINA PEREIRA CHAVES', '1916375', 'tania.chaves@feliz.ifrs.edu.br', 'TÂNIA CHAVES', '', '', 'S', NULL, NULL, '2012-12-19 10:04:15', 7, NULL, NULL),
(78, 'Osvaldo Casares Pinto', '407908', 'osvaldo.pinto@riogrande.ifrs.edu.br', 'Osvaldo C. Pinto', '', '', 'S', NULL, NULL, '2012-12-19 10:10:25', 12, NULL, NULL),
(79, 'VIVIANE SILVA RAMOS', '1221235', 'viviane.ramos@ifrs.edu.br', 'VIVIANE', '', 'Sim. Lactose e Glúten', 'S', NULL, NULL, '2012-12-19 10:12:45', 1, NULL, NULL),
(80, 'Everson Ferreira Menezes', '1852135', 'everson.menezes@poa.ifrs.edu.br', 'Everson', '', '', 'S', NULL, NULL, '2012-12-19 10:13:25', 10, NULL, NULL),
(81, 'ODIRCE TEIXEIRA ANTUNES', '1104258', 'odirce.antunes@sertao.ifrs.edu.br', 'ODIRCE', '', '', 'S', NULL, NULL, '2012-12-19 10:20:21', 13, NULL, NULL),
(82, 'Ana Paula Lemke', '1827320', 'ana.lemke@feliz.ifrs.edu.br', 'Ana Paula Lemke', '', '', 'S', NULL, NULL, '2012-12-19 10:32:58', 7, NULL, NULL),
(83, 'jaqueline janaina sirena', '1982905', 'jaqueline.sirena@caxias.ifrs.edu.br', 'Jaqueline Sirena', '', '', 'S', NULL, NULL, '2012-12-19 10:33:44', 4, NULL, NULL),
(84, 'GIANE SILVA SANTOS', '1826448', 'giane.santos@osorio.ifrs.edu.br', 'GIANE SANTOS', '', '', 'S', NULL, NULL, '2012-12-19 10:57:02', 9, NULL, NULL),
(85, 'PABLO SAMPIETRO VASCONCELOS', '1982593', 'pablo.vasconcelos@ifrs.edu.br', 'PABLO VASCONCELOS', '', 'SIM, INTOLERÂNCIA A LACTOSE E ALIMENTOS COM EXESSO DE TEMPÊROS FORTES, E CONDIENTOS INDUSTRIALIZADOS ', 'S', NULL, NULL, '2012-12-19 10:58:32', 1, 3, NULL),
(86, 'Vanessa Ecléa de Oliveira', '01800002', 'vanessa.oliveira@sertao.ifrs.edu.br', 'Vanessa Oliveira', '', '', 'S', NULL, NULL, '2012-12-19 11:01:02', 13, NULL, NULL),
(87, 'DIEGO MARTINS DE OLIVEIRA', '1863248', 'diego.oliveira@riogrande.ifrs.edu.br', 'Diego M. de Oliveira', '', 'Doces. Gordura em excesso.', 'S', NULL, NULL, '2012-12-19 11:13:01', 12, NULL, NULL),
(88, 'Melissa Heberle', '1899948', 'melissa.heberle@ibiruba.ifrs.edu.br', 'Melissa', '', '', 'S', NULL, NULL, '2012-12-19 11:13:12', 8, NULL, NULL),
(89, 'Maria Cristina Caminha de Castilhos França', '1796654', 'mcristina.franca@poa.ifrs.edu.br', 'Maria Cristina França', '', '', 'S', NULL, NULL, '2012-12-19 11:18:54', 10, NULL, NULL),
(90, 'Sidny Janaina pedrosa', '1827358', 'sidny.pedrosa@caxias.ifrs.edu.br', 'Sidny Pedrosa', '', '', 'S', NULL, NULL, '2012-12-19 11:20:11', 4, NULL, NULL),
(91, 'Liane Sbardelotto', '1824637', 'liane.sbardelotto@caxias.ifrs.edu.br', 'Liane Sbardelotto', '', '', 'S', NULL, NULL, '2012-12-19 11:22:02', 4, NULL, NULL),
(92, 'MARIA INÊS SIMON', '18680811', 'maria.simon@ibiruba.ifrs.edu.br', 'MARIA INÊS', '', '', 'S', NULL, NULL, '2012-12-19 11:26:41', 8, NULL, NULL),
(93, 'Edson Regis de Jesus', '1811446', 'edson.jesus@canoas.ifrs.edu.br', 'Edson', '', '', 'S', NULL, NULL, '2012-12-19 11:34:00', 3, NULL, NULL),
(94, 'Jeferson de Araujo Funchal', '017966450', 'jeferson.funchal@poa.ifrs.edu.br', 'Jeferson Funchal', '', '', 'S', NULL, NULL, '2012-12-19 11:38:01', 10, NULL, NULL),
(95, 'Caren Fulginiti da Silva', '019023596', 'caren.silva@farroupilha.ifrs.edu.br', 'Caren Silva', '', 'alergia a sementes: nozes, castanhas, pistache, amendoim... alergia a morango.', 'S', NULL, NULL, '2012-12-19 11:44:55', 6, NULL, NULL),
(96, 'Danner Souza Terra', '2711765', 'danner.terra@ifrs.edu.br', 'Danner Terra', '', '', 'S', NULL, NULL, '2012-12-19 11:48:35', 1, NULL, NULL),
(97, 'Elisandra Teresinha Munareto', '1676240', 'elisandra.avila@ifrs.edu.br', 'Elisandra', '', '', 'S', NULL, NULL, '2012-12-19 12:00:48', 1, NULL, NULL),
(98, 'Melissa dietrich da rosa', '016487532', 'Melissa.rosa@farroupilha.ifrs.edu.br', 'Melissa dietrich', '', '', 'S', NULL, NULL, '2012-12-19 12:07:11', 6, 3, NULL),
(99, 'Leonardo da Silva Cezarini', '017970873', 'leonardo.cezarini@ifrs.edu.br', 'Leonardo cezarini', '', '', 'S', NULL, NULL, '2012-12-19 12:44:10', 1, NULL, NULL),
(100, 'Juliane Donadel', '1637983', 'juliane.donadel@farroupilha.ifrs.edu.br', 'Juliane', '', '', 'S', NULL, NULL, '2012-12-19 12:52:25', 6, NULL, NULL),
(101, 'JOCELI DA SILVA E SILVA', '363283', 'joceli.silva@sertao.ifrs.edu.br', 'JOCELI', '', '', 'S', NULL, NULL, '2012-12-19 13:00:29', 13, NULL, NULL),
(102, 'Franck Joy de Almeida', '1858081', 'franck.joy@farroupilha.ifrs.edu.br', 'Franck Joy', '', '', 'S', NULL, NULL, '2012-12-19 13:04:00', 6, NULL, NULL),
(103, 'Diane Blank Bencke', '17484316', 'diane.bencke@farroupilha.ifrs.edu.br', 'Diane B. Bencke', '', '', 'S', NULL, NULL, '2012-12-19 13:08:50', 6, NULL, NULL),
(104, 'Marta Marlice Hanel', '1104665', 'marta.hanel@sertao.ifrs.edu.br', 'Marta', '', '', 'S', NULL, NULL, '2012-12-19 13:21:43', 13, NULL, NULL),
(105, 'Ivan José Suszek', '1791135', 'ivan.suszek@erechim.ifrs.edu.br', 'Ivan Suszek', '', '', 'S', NULL, NULL, '2012-12-19 13:45:35', 5, NULL, NULL),
(106, 'Pâmela Perini', '1867841', 'pamela.perini@riogrande.ifrs.edu.br', 'Pâmela Perini', '', '', 'S', NULL, NULL, '2012-12-19 13:45:46', 12, NULL, NULL),
(107, 'Elizabete Maria da Silva Pedroski', '1828090', 'elizabete.pedroski@ifrs.edu.br', 'Elizabete Pedroski', '', '', 'S', NULL, NULL, '2012-12-19 14:09:04', 1, NULL, NULL),
(108, 'José Edson Azevedo da Silva', '1816675', 'edson.azevedo@farroupilha.ifrs.edu.br', 'Edson Azevedo', '', '', 'S', NULL, NULL, '2012-12-19 14:24:16', 6, NULL, NULL),
(109, 'Letícia Pinho Jeronimo', '1818444', 'leticia.jeronimo@riogrande.ifrs.edu.br', 'Letícia Jeronimo', '', '', 'S', NULL, NULL, '2012-12-19 14:31:59', 12, NULL, NULL),
(110, 'CARINE IVONE POPIOLEK', '1828079', 'CARINE.POPIOLEK@RESTINGA.IFRS.EDU.BR', 'CARINE', '', '', 'S', NULL, NULL, '2012-12-19 14:57:02', 11, NULL, NULL),
(111, 'Roberto Carlos Pereira', '21146281', 'roberto.pereira@riogrande.ifrs.edu.br', 'Roberto Pereira', '', '', 'S', NULL, NULL, '2012-12-19 15:04:08', 12, NULL, NULL),
(112, 'Rodrigo Costa Fredo', '1761771', 'rodrigo.fredo@riogrande.ifrs.edu.br', 'Fredo', '', '', 'S', NULL, NULL, '2012-12-19 15:12:23', 12, NULL, 2),
(113, 'Leonora Marta Devensi', '1824155', 'leonora.devensi@caxias.ifrs.edu.br', 'Leonora Devensi', '', '', 'S', NULL, NULL, '2012-12-19 16:43:03', 4, NULL, NULL),
(114, 'Claudio Enrique Fernández Rodríguez', '1327467', 'claudio.fernandez@canoas.ifrs.edu.br', 'Claudio Fernández', '', '', 'S', NULL, NULL, '2012-12-19 16:49:49', 3, NULL, NULL),
(115, 'BRUNA PINARELLO PIZZOLATO', '1592786', 'bruna.pizzolato@erechim.ifrs.edu.br', 'BRUNA', '', '', 'S', NULL, NULL, '2012-12-19 18:37:10', 5, NULL, NULL),
(116, 'MARCOS BARROS DE SOUZA', '409040', 'marcos.barros@riogrande.ifrs.edu.br', 'MARCOS', '', '', 'S', NULL, NULL, '2012-12-19 18:46:37', 12, NULL, NULL),
(117, 'Luiz Felipe Borges Martins', '018723403', 'luiz.martins@sertao.ifrs.edu.br', 'Luiz Felipe B. Martins', '', '', 'S', NULL, NULL, '2012-12-19 18:48:49', 13, NULL, NULL),
(118, 'Erik Schuler', '1741645', 'erik.schuler@farroupilha.ifrs.edu.br', 'Erik Schuler', '', '', 'S', NULL, NULL, '2012-12-19 18:50:24', 6, NULL, NULL),
(119, 'CELSO LUIS FREITAS', '1671388', 'celso.freitas@riogrande.ifrs.edu.br', 'CELSO', '', '', 'S', NULL, NULL, '2012-12-19 19:39:53', 12, NULL, NULL),
(120, 'Margarete de Quevedo', '017970581', 'margarete.quevedo@ifrs.edu.br', 'Margarete', '', '', 'S', NULL, NULL, '2012-12-19 21:29:39', 1, NULL, NULL),
(121, 'Alessandra Aragon Nevado', '1866512', 'alessandra.nevado@caxias.ifrs.edu.br', 'Alessandra Aragon', '', '', 'S', NULL, NULL, '2012-12-19 22:16:34', 4, NULL, NULL),
(122, 'Pedro Chaves da Rocha', '13727331', 'pedro.rocha@restinga.ifrs.edu.br', 'Pedro Rocha', '', '', 'S', NULL, NULL, '2012-12-20 08:05:29', 11, NULL, NULL),
(123, 'Daiane Truylio', '1757547', 'daiane.truylio@erechim.ifrs.edu.br', 'Daiane Truylio', '', '', 'S', NULL, NULL, '2012-12-20 08:05:53', 5, NULL, NULL),
(124, 'João Henrique Machado', '1917869', 'joao.machado@ifrs.edu.br', 'João Henrique', '', '', 'S', NULL, NULL, '2012-12-20 09:09:16', 1, NULL, NULL),
(125, 'DEIVA CLÁUDIA RODIGUERO BOLZANI', '1105044', 'deiva.bolzani@sertao.ifrs.edu.br', 'Deiva', '', '', 'S', NULL, NULL, '2012-12-20 10:23:48', 13, NULL, NULL),
(126, 'Cristiane Câmara', '1816290', 'cristiane.camara@erechim.ifrs.edu.br', 'Cristiane Câmara', '', '', 'S', NULL, NULL, '2012-12-20 10:40:30', 5, NULL, NULL),
(127, 'Jaqueline Iaroszeski', '1758166', 'jaqueline.iaroszeski@erechim.ifrs.edu.br', 'Jaqueline Iaroszeski', '', '', 'S', NULL, NULL, '2012-12-20 10:44:17', 5, NULL, NULL),
(128, 'RAQUEL DALLA LANA CARDOSO', '1853821', 'raquel.cardoso@ibiruba.ifrs.edu.br', 'RAQUEL DL CARDOSO', '', '', 'S', NULL, NULL, '2012-12-20 11:33:00', 8, NULL, NULL),
(129, 'Josiele Sfredo Michelin', '1774158', 'josiele.michelin@erechim.ifrs.edu.br', 'Josiele S. Michelin', '', '', 'S', NULL, NULL, '2012-12-20 13:26:49', 5, NULL, NULL),
(130, 'Andrea Jessica Borges Monzón', '1796611', 'andrea.monzon@feliz.ifrs.edu.br', 'Andrea Monzón', '', 'alergia a camarão', 'S', NULL, NULL, '2012-12-20 13:31:10', 7, NULL, NULL),
(131, 'Liége Barbieri Silveira', '1893755', 'liege.silveira@canoas.ifrs.edu.br', 'Liége B. Silveira', '', '', 'S', NULL, NULL, '2012-12-20 15:01:50', 3, NULL, NULL),
(132, 'Betania Vargas Oliveira', '1817717', 'betania.vargas@riogrande.ifrs.edu.br', 'Betânia', '', '', 'S', NULL, NULL, '2012-12-20 15:11:41', 12, NULL, NULL),
(133, 'Fernanda Raquel Brand', '018127721', 'fernanda.brand@farroupilha.ifrs.edu.br', 'Fernanda Brand', '', '', 'S', NULL, NULL, '2012-12-20 15:22:39', 6, NULL, NULL),
(134, 'Vanessa Petro', '019435282', 'vanessa.petro@feliz.ifrs.edu.br', 'Vanessa', '', '', 'S', NULL, NULL, '2012-12-20 15:35:02', 7, NULL, NULL),
(135, 'Luciano José Crochemore', '1498426', 'luciano.crochemore@feliz.ifrs.edu.br', 'Crochemore', '', '', 'S', NULL, NULL, '2012-12-20 15:38:16', 7, NULL, NULL),
(136, 'Jeonice Werle Techio', '1668166', 'jeonice.techio@sertao.ifrs.edu.br', 'Jeonice Werle Techio', '', '', 'S', NULL, NULL, '2012-12-20 16:11:05', 13, NULL, NULL),
(137, 'Marcos Dias Mathies', '1855662', 'marcos.mathies@canoas.ifrs.edu.br', 'Marcos Mathies', '', '', 'S', NULL, NULL, '2012-12-20 17:52:06', 3, NULL, NULL),
(138, 'Ivan Paulo Canal', '017963206', 'ivan.canal@ibiruba.ifrs.edu.br', 'Ivan Paulo Canal', '', '', 'S', NULL, NULL, '2012-12-20 19:14:33', 8, NULL, NULL),
(139, 'Caroline Lacerda Dorneles', '1765811', 'caroline.dorneles@riogrande.ifrs.edu.br', 'Caroline Lacerda', '', '', 'S', NULL, NULL, '2012-12-20 19:14:47', 12, NULL, NULL),
(140, 'Marco Aurélio dos Santos Rahn', '1149810', 'marco.aurelio.rahn@riogrande.ifrs.edu.br', 'Rahn', '', '', 'S', NULL, NULL, '2012-12-20 19:37:42', 12, NULL, NULL),
(141, 'Alexandre Expindola de Felippe', '2650418', 'alexandre.defelippe@riogrande.ifrs.edu.br', 'Alexandre', '', '', 'S', NULL, NULL, '2012-12-20 19:39:51', 12, NULL, NULL),
(142, 'Liliane Gonçalves Borges', '1102937', 'liliane.borges@ifrs.edu.br', 'Liliane', '', '', 'S', NULL, NULL, '2012-12-21 10:33:56', 1, NULL, NULL),
(143, 'Sérgio Wortmann', '002739550', 'sergiowortmann@ifrs.edu.br', 'Sérgio Wortmann', '', '', 'S', NULL, NULL, '2012-12-21 11:55:54', 1, 2, NULL),
(144, 'Tatiana Weber', '1796213', 'tatiana.weber@caxias.ifrs.edu.br', 'Tatiana', '', '', 'S', NULL, NULL, '2012-12-21 12:57:59', 4, NULL, NULL),
(145, 'Luis Felipe Rhoden Freitas', '1829458', 'luis.freitas@caxias.ifrs.edu.br', 'Luis Felipe', '', '', 'S', NULL, NULL, '2012-12-21 13:04:13', 4, NULL, NULL),
(146, 'AMIRIS REZENDE BUSATO', '1104442', 'amiris.busato@sertao.ifrs.edu.br', 'AMIRIS', '', '', 'S', NULL, NULL, '2012-12-21 13:49:38', 13, 3, NULL),
(147, 'Marcus André Kurtz Almança', '1806059', 'marcus.almanca@bento.ifrs.edu.br', 'Marcus Almança', '', '', 'S', NULL, NULL, '2012-12-21 13:54:58', 2, 2, NULL),
(148, 'Pablo Daniel Freitas Bueno', '1761530', 'pablo.bueno@riogrande.ifrs.edu.br', 'Pablo Bueno', '', '', 'S', NULL, NULL, '2012-12-21 14:07:37', 12, NULL, NULL),
(149, 'Thaís Teixeira da Silva', '016475887', 'thais.silva@restinga.ifrs.edu.br', 'Thaís', '', '', 'S', NULL, NULL, '2012-12-21 15:06:45', 11, NULL, NULL),
(150, 'Jefferson Pereira de Almeida', '014484366', 'jefferson.almeida@farroupilha.ifrs.edu.br', 'Jefferson Pereira de Almeida', '', '', 'S', NULL, NULL, '2012-12-21 15:49:43', 6, NULL, NULL),
(151, 'Luiz Felipe Velho', '1649275', 'luiz.velho@poa.ifrs.edu.br', 'Luiz Felipe Velho', '', 'Intolerância à lactose', 'S', NULL, NULL, '2012-12-21 15:56:20', 10, NULL, NULL),
(152, 'Justina Bechi Robaski', '1816352', 'justina.robaski@canoas.ifrs.edu.br', 'Justina', '', '', 'S', NULL, NULL, '2012-12-21 16:15:21', 3, 2, NULL),
(153, 'João Cândido Moraes Neves', '1669191', 'joao.neves@caxias.ifrs.edu.br', 'João Neves', '', '', 'S', NULL, NULL, '2012-12-21 16:25:47', 4, NULL, NULL),
(154, 'RAFAEL ALFONSO BRINKHUES', '1824743', 'RAFAEL.BRINKHUES@CAXIAS.IFRS.EDU.BR', 'RAFAEL BRINKHUES', '', '', 'S', NULL, NULL, '2012-12-21 16:41:54', 4, NULL, NULL),
(155, 'André Luiz Portanova Laborde', '1649247', 'andre.laborde@caxias.ifrs.edu.br', 'André Laborde', '', '', 'S', NULL, NULL, '2012-12-21 17:00:29', 4, NULL, NULL),
(156, 'Joana Helena Paloschi', '1838738', 'joana.paloschi@feliz.ifrs.edu.br', 'Joana Paloschi', '', '', 'S', NULL, NULL, '2012-12-21 18:15:20', 7, 3, NULL),
(157, 'Kelen Berra de Mello', '1796302', 'kelen.mello@caxias.ifrs.edu.br', 'Kelen Berra de Mello', '', '', 'S', NULL, NULL, '2012-12-21 21:02:06', 4, NULL, NULL),
(158, 'ROSILENE D''ALASCIO D''AMOREIRA', '1449469', 'rosilene.damoreira@riogrande.ifrs.edu.br', 'ROSILENE', '', '', 'S', NULL, NULL, '2012-12-22 09:40:48', 12, NULL, NULL),
(159, 'José Cláudio Corrêa Seferim', '018272541', 'jose.seferim@caxias.ifrs.edu.br', 'Cláudio Seferim', '', '', 'S', NULL, NULL, '2012-12-25 11:47:00', 4, NULL, NULL),
(160, 'Luciano de Athayde Karasek', '019251882', 'luciano.karasek@caxias.ifrs.edu.br', 'Luciano de A. Karasek', '', '', 'S', NULL, NULL, '2012-12-26 08:59:41', 4, NULL, NULL),
(161, 'Lucia Helena Mendes Borges', '4091132', 'lucia.helena@riogrande.ifrs.edu.br', 'Lucia Helena', '', '', 'S', NULL, NULL, '2012-12-26 19:03:56', 12, NULL, NULL),
(162, 'Volmir Marcos Lima', '049244', 'volmir.lima@sertao.ifrs.edu.br', 'Volmir Marcos Lima', '', '', 'S', NULL, NULL, '2012-12-27 10:12:45', 13, 3, NULL),
(163, 'CRISTINA BOHN CITOLIN', '1822007', 'cristina.citolin@bento.ifrs.edu.br', 'CRISTINA CITOLIN', '', '', 'S', NULL, NULL, '2012-12-27 11:21:28', 2, NULL, NULL),
(164, 'Marília Balbinot Pavan', '1827218', 'marilia.pavan@ifrs.edu.br', 'Marília', '', '', 'S', NULL, NULL, '2012-12-27 13:25:36', 1, NULL, 4),
(165, 'Ângela Flach', '015673227', 'angela.flach@poa.ifrs.edu.br', 'Ângela', '', '', 'N', NULL, NULL, '2012-12-27 13:31:35', 10, NULL, NULL),
(166, 'Arlan Pacheco Figueiredo', '1613322', 'arlan.figueiredo@caxias.ifrs.edu.br', 'Arlan', '', '', 'S', NULL, NULL, '2012-12-27 13:33:41', 4, NULL, NULL),
(167, 'LEANE MARIA FILIPETTO', '1211905', 'leane.filipetto@bento.ifrs.edu.br', 'LEANE', '', '', 'S', NULL, NULL, '2012-12-27 14:56:23', 2, NULL, NULL),
(168, 'ROBERTA RIGO DE AGUIAR', '018196942', 'roberta.aguiar@caxias.ifrs.edu.br', 'ROBERTA', '', '', 'S', NULL, NULL, '2012-12-27 15:00:46', 4, NULL, NULL),
(169, 'Maurício Ivan dos Santos', '01577684', 'mauricio.santos@canoas.ifrs.edu.br', 'Maurício Santos', '', '', 'S', NULL, NULL, '2012-12-27 22:36:46', 3, NULL, 2),
(170, 'Ildo José Seminotti', '1213562', 'ildo.seminotti@sertao.ifrs.edu.br', 'Ildo', '', '', 'S', NULL, NULL, '2012-12-28 16:10:03', 13, NULL, NULL),
(171, 'Rafael Ramires Jaques', '1869122', 'rafael.jaques@feliz.ifrs.edu.br', 'Rafael Jaques', '', '', 'S', NULL, NULL, '2012-12-30 14:28:15', 7, NULL, NULL),
(172, 'MURIEL DE OLIVEIRA', '1875437', 'MURIEL.OLIVEIRA@RIOGRANDE.IFRS.EDU.BR', 'MURIEL', '', '', 'S', NULL, NULL, '2012-12-31 00:02:37', 12, NULL, NULL),
(173, 'José Eli Santos dos Santos', '409036', 'jose.eli@riogrande.ifrs.edu.br', 'José Eli', '', '', 'S', NULL, NULL, '2013-01-02 15:10:09', 12, NULL, NULL),
(174, 'MILENA ALVES BRATTI', '1829182', 'milena.bratti@osorio.ifrs.edu.br', 'MILENA BRATTI', '', '', 'S', NULL, NULL, '2013-01-02 17:06:13', 9, NULL, NULL),
(175, 'Mariângela Scapinelo', '1524461', 'mariangela.scapinelo@ifrs.edu.br', 'Mariangela', '', '', 'S', NULL, NULL, '2013-01-03 08:26:58', 1, NULL, NULL),
(176, 'Ana Lúcia Tomazelli', '1810334', 'ana.tomazelli@ifrs.edu.br', 'Ana Lúcia Tomazelli', '', '', 'S', NULL, NULL, '2013-01-03 08:31:31', 1, NULL, NULL),
(177, 'Magali Inês Pessini', '1809690', 'magali.pessini@caxias.ifrs.edu.br', 'Magali', '', '', 'S', NULL, NULL, '2013-01-03 08:50:42', 4, NULL, NULL),
(178, 'Fabiana Carvalho Donida', '16780655', 'fabiana.donida@ifrs.edu.br', 'Fabiana C. Donida', '', '', 'S', NULL, NULL, '2013-01-03 09:19:23', 1, NULL, NULL),
(179, 'Ana Lethea da Cunha Possa', '1812330', 'ana.possa@caxias.ifrs.edu.br', 'Ana Possa', '', '', 'S', NULL, NULL, '2013-01-03 10:04:01', 4, NULL, NULL),
(180, 'Juliano Cantarelli Toniolo', '1804846', 'juliano.toniolo@caxias.ifrs.edu.br', 'Juliano Toniolo', '', 'Sim. Camarão.', 'S', NULL, NULL, '2013-01-03 10:18:32', 4, NULL, NULL),
(181, 'Júlio Xandro Heck', '1342277', 'julio.heck@ifrs.edu.br', 'Júlio Xandro Heck', '', '', 'S', NULL, NULL, '2013-01-03 10:31:58', 1, NULL, NULL),
(182, 'EDINETE RITA FOLLE CECCONELLO', '1827482', 'edinete.cecconello@ifrs.edu.br', 'EDINETE RITA FOLLE CECCONELLO', '', '', 'S', NULL, NULL, '2013-01-03 10:33:31', 1, NULL, NULL),
(183, 'FABRICIO SOBROSA AFFELDT', '1644988', 'fabricio.sobrosa@poa.ifrs.edu.br', 'FABRÍCIO SOBROSA', '', '', 'S', NULL, NULL, '2013-01-03 10:33:42', 10, NULL, NULL),
(184, 'Jeison Leandro Ruckert', '1866673', 'jeison.ruckert@canoas.ifrs.edu.br', 'Jeison L Ruckert', '', '', 'S', NULL, NULL, '2013-01-03 10:34:58', 3, NULL, NULL),
(185, 'LENIR ANTONIO HANNECKER', '1104196', 'lenir.hannecker@sertao.ifrs.edu.br', 'LENIR ANTONIO', '', '', 'S', NULL, NULL, '2013-01-03 10:36:02', 13, NULL, NULL),
(186, 'Rosane Fabris', '2105045', 'rosane.fabris@ifrs.edu.br', 'Rosane', '', '', 'S', NULL, NULL, '2013-01-03 10:36:09', 1, NULL, NULL),
(187, 'Denise Bilibio', '1450051', 'denise.bilibio@sertao.ifrs.edu.br', 'Denise', '', '', 'S', NULL, NULL, '2013-01-03 10:36:38', 13, NULL, NULL),
(188, 'Adriano Rodrigues Jose', '1755645', 'adriano.jose@canoas.ifrs.edu.br', 'Adriano R Jose', '', '', 'S', NULL, NULL, '2013-01-03 10:37:01', 3, NULL, NULL),
(189, 'LUCIANO MANFROI', '1345023', 'luciano.manfroi@bento.ifrs.edu.br', 'LUCIANO MANFROI', '', '', 'S', NULL, NULL, '2013-01-03 10:39:10', 2, NULL, NULL),
(190, 'Olavo Ramalho Marques', '1824179', 'olavo.marques@caxias.ifrs.edu.br', 'Olavo Marques', '', '', 'S', NULL, NULL, '2013-01-03 10:40:06', 4, NULL, NULL),
(191, 'GINA MIKOWAISKI VALENT', '00358136', 'gina.valent@ifrs.edu.br', 'GINA', '', '', 'S', NULL, NULL, '2013-01-03 10:46:20', 1, NULL, NULL),
(192, 'Flavia Garcez', '1100074', 'flavia.garcez@erechim.ifrs.edu.br', 'Flavia', '', '', 'S', NULL, NULL, '2013-01-03 10:48:45', 5, NULL, NULL),
(193, 'Eduardo da Rocha Bassi', '1816280', 'eduardo.bassi@ifrs.edu.br', 'Eduardo Bassi', '', '', 'S', NULL, NULL, '2013-01-03 10:51:59', 1, NULL, NULL),
(194, 'Ivan Jorge Gabe', ' 1804294', 'ivan.gabe@farroupilha.ifrs.edu.br', 'Ivan', '', '', 'S', NULL, NULL, '2013-01-03 10:52:15', 6, NULL, NULL),
(195, 'Caroline Poletto', '1756410', 'caroline.poletto@bento.ifrs.edu.br', 'Caroline', '', '', 'S', NULL, NULL, '2013-01-03 10:52:47', 2, NULL, NULL),
(196, 'Giovani Silveira Petiz', '0274569', 'proad@ifrs.edu.br', 'Giovani Silveira Petiz', '', '', 'S', NULL, NULL, '2013-01-03 10:54:16', 1, NULL, NULL),
(197, 'Luciana Moreira de Souza', '2453029', 'luciana.souza@bento.ifrs.edu.br', 'Luciana Souza', '', '', 'S', NULL, NULL, '2013-01-03 10:54:37', 2, NULL, NULL),
(198, 'Flávia Reis de Oliveira', '1805320', 'flavia.reis@caxias.ifrs.edu.br', 'Flávia Oliveira', '', '', 'S', NULL, NULL, '2013-01-03 10:59:49', 4, NULL, NULL),
(199, 'Augusto Massashi Horiguti', '1187224', 'augusto.horiguti@farroupilha.ifrs.edu.br', 'Horiguti', '', '', 'S', NULL, NULL, '2013-01-03 11:05:27', 6, NULL, NULL),
(200, 'Milena Silvester Quadros', '1872360', 'milena.quadros@ibiruba.ifrs.edu.br', 'Milena Quadros', '', '', 'S', NULL, NULL, '2013-01-03 11:06:36', 8, NULL, NULL),
(201, 'Édio Fontana', '1099717', 'dap@canoas.ifrs.edu.br', 'ÉDIO', '', '', 'S', NULL, NULL, '2013-01-03 11:07:12', 3, NULL, NULL),
(202, 'Suelen Da Rolt', '1678978', 'suelen.rolt@ifrs.edu.br', 'Suelen Da Rolt', '', '', 'S', NULL, NULL, '2013-01-03 11:07:34', 1, NULL, NULL),
(203, 'Nelson Roza Madeira', '2102351', 'dap@restinga.ifrs.edu.br', 'Nelson Madeira', '', '', 'S', NULL, NULL, '2013-01-03 11:07:47', 11, NULL, NULL),
(204, 'Raquel Maragarete Franzen de Avila', '1867986', 'raquel.avila@bento.ifrs.edu.br', 'Raquel Avila', '', '', 'S', NULL, NULL, '2013-01-03 11:09:57', 2, NULL, NULL),
(205, 'Diego Cechin Sisnandes', '1978224', 'diego.sisnandes@ifrs.edu.br', 'Diego Sisnandes', '', '', 'S', NULL, NULL, '2013-01-03 11:13:09', 1, NULL, NULL),
(206, 'Claudio Vinicius Silva Farias', '1505401', 'claudio.farias@poa.ifrs.edu.br', 'Claudio Farias', '', '', 'S', NULL, NULL, '2013-01-03 11:13:48', 10, NULL, NULL),
(207, 'Juçara Nunes da Silva', '1522972', 'jucara.silva@riogrande.ifrs.edu.br', 'Juçara Silva', '', '', 'S', NULL, NULL, '2013-01-03 11:19:17', 12, 3, NULL),
(208, 'Ana Rosaura Moraes Springer', '1008744', 'ana.springer@poa.ifrs.edu.br', 'Ana Springer', '', '', 'S', NULL, NULL, '2013-01-03 11:19:48', 10, 3, NULL),
(209, 'Laura Zandonai Brancher', '011023112', 'laura.brancher@bento.ifrs.edu.br', 'Laura', '', 'Alergia a manga', 'S', NULL, NULL, '2013-01-03 11:22:07', 2, NULL, NULL),
(210, 'João Luis Pereira Gomes', '3565254', 'joao.gomes@poa.ifrs.edu.br', 'João Luís', '', '', 'S', NULL, NULL, '2013-01-03 11:36:47', 10, NULL, NULL),
(211, 'Adriana Rosa Majola Romagna da Silva', '1860864', 'adriana.majola@ifrs.edu.br', 'Adriana Majola', '', '', 'S', NULL, NULL, '2013-01-03 11:38:32', 1, NULL, NULL),
(212, 'Giovani Forgiarini Aiub', '1720152', 'giovani.aiub@feliz.ifrs.edu.br', 'Giovani Aiub', '', '', 'S', NULL, NULL, '2013-01-03 11:43:27', 7, NULL, NULL),
(213, 'Grazielle Marin Leite', '1827121', 'grazielle.leite@ifrs.edu.br', 'Grazielle', '', '', 'S', NULL, NULL, '2013-01-03 11:45:06', 1, NULL, NULL),
(214, 'Fabiano Dornelles Ramos', '18271634', 'fabiano.ramos@caxias.ifrs.edu.br', 'Fabiano', '', '', 'S', NULL, NULL, '2013-01-03 11:48:16', 4, NULL, NULL),
(215, 'GIOVANI VILMAR COMERLATTO', '1793620', 'giovani.comerlatto@bento.ifrs.edu.br', 'GIOVANI', '', '', 'S', NULL, NULL, '2013-01-03 11:59:38', 2, NULL, 4),
(216, 'MICHELLE CHAGAS DE FARIAS', '016007271', 'michelle.farias@bento.ifrs.edu.br', 'MICHELLE', '', '', 'S', NULL, NULL, '2013-01-03 12:39:07', 2, NULL, NULL),
(217, 'Clúvio Buenno Soares Terceiro', '0169797', 'cluvio.terceiro@poa.ifrs.edu.br', 'Clúvio Terceiro', '', '', 'S', NULL, NULL, '2013-01-03 12:48:55', 10, 1, NULL),
(218, 'fabio augusto marin', '1563948', 'fabio.marin@bento.ifrs.edu.br', 'Marin, Fábio Augusto', '', '', 'S', NULL, NULL, '2013-01-03 12:59:35', 2, 1, NULL),
(219, 'Valdir Francisco Schafer', '55135', 'valdir.schafer@sertao.ifrs.edu.br', 'Valdir Schafer', '', '', 'S', NULL, NULL, '2013-01-03 13:04:35', 13, NULL, NULL),
(220, 'Ademir Dorneles de Dorneles', '1612582', 'ademir.dorneles@poa.ifrs.edu.br', 'Ademir Dorneles', '', '', 'S', NULL, NULL, '2013-01-03 13:04:42', 10, NULL, NULL),
(221, 'Rosangela de Oliveira', '1104478', 'rosangela.oliveira@sertao.ifrs.edu.br', 'Rosangela', 'Sim, 30% perda auditiva.', '', 'S', NULL, NULL, '2013-01-03 13:05:52', 13, NULL, NULL),
(222, 'ADRIANA DE FARIAS RAMOS', '010318399', 'ADRIANA.RAMOS@POA.IFRS.EDU.BR', 'ADRIANA RAMOS', '', '', 'S', NULL, NULL, '2013-01-03 13:07:57', 10, NULL, NULL),
(223, 'Leandro Raizer', '1577807', 'leandro.raizer@gmail.com', 'Leandro Raizer', '', '', 'S', NULL, NULL, '2013-01-03 13:09:05', 9, NULL, NULL),
(224, 'Daniela Rodrigues da Silva', '1796072', 'daniela.silva@canoas.ifrs.edu.br', 'Daniela', '', '', 'S', NULL, NULL, '2013-01-03 13:17:03', 3, NULL, NULL),
(225, 'Marilandi Maria Mascarello Vieira', '1901218', 'marilandi.vieira@sertao.ifrs.edu.br', 'Marilandi', '', '', 'S', NULL, NULL, '2013-01-03 13:17:53', 13, NULL, NULL),
(226, 'roberto saouaya', '0131357', 'roberto.saouaya@osorio.ifrs.edu.br', 'roberto', '', 'sim, temperos químico (glutamaco monossódico', 'S', NULL, NULL, '2013-01-03 13:24:42', 9, 3, NULL),
(227, 'Lílian Escandiel Crizel', '1602034', 'lilian.crizel@feliz.ifrs.edu.br', 'Lílian', '', '', 'S', NULL, NULL, '2013-01-03 13:43:39', 7, NULL, NULL),
(228, 'Sabrina Clavé Eufrásio', '017372356', 'sabrina.eufrasio@canoas.ifrs.edu.br', 'Sabrina Clavé Eufrásio', '', '', 'S', NULL, NULL, '2013-01-03 13:44:28', 3, NULL, NULL),
(229, 'Fernando de Oliveira Leão', '1982901', 'fernando.leao@ibiruba.ifrs.edu.br', 'Fernando Leão', '', '', 'S', NULL, NULL, '2013-01-03 13:45:42', 8, NULL, NULL),
(230, 'Marcia Soares Forgiarini', '1680321', 'marcia.forgiarini@caxias.ifrs.edu.br', 'Marcia Forgiarini', '', '', 'S', NULL, NULL, '2013-01-03 13:54:05', 4, NULL, NULL),
(231, 'Migacir Trindade Duarte Flôres', '2104561', 'migacir.flores@ibiruba.ifrs.edu.br', 'Migacir', '', '', 'S', NULL, NULL, '2013-01-03 14:03:19', 8, NULL, NULL),
(232, 'Edson Carpes Camargo', '1799163', 'edson.camargo@bento.ifrs.edu.br', 'Edson Camargo', '', '', 'S', NULL, NULL, '2013-01-03 14:17:22', 2, NULL, NULL),
(233, 'Flávia Santos Twardowski Pinto', '1770270', 'flavia.pinto@osorio.ifrs.edu.br', 'Flávia Twardowski', '', '', 'S', NULL, NULL, '2013-01-03 14:18:17', 9, NULL, 2),
(234, 'João Rogério Machado Pereira', '1737354', 'joao.pereira@erechim.ifrs.edu.br', 'João Rogério', '', '', 'S', NULL, NULL, '2013-01-03 14:31:39', 5, NULL, NULL),
(235, 'Fabio Rios Kwecko', '18191703', 'fabio.kwecko@riogrande.ifrs.edu.br', 'Fabio Kwecko', '', '', 'S', NULL, NULL, '2013-01-03 15:01:39', 12, NULL, NULL),
(236, 'NOEMI LUCIANE DOS SANTOS', '017966371', 'noemi.santos@erechim.ifrs.edu.br', 'NOEMI L. DOS SANTOS', '', '', 'S', NULL, NULL, '2013-01-03 15:02:21', 5, NULL, NULL),
(237, 'Toni Luis Benazzi', '015544737', 'toni.benazzi@erechim.ifrs.edu.br', 'Toni Benazzi', '', '', 'S', NULL, NULL, '2013-01-03 15:15:31', 5, NULL, NULL),
(238, 'Rosangela Leal Bjerk', '012071897', 'rosangela.bjerk@poa.ifrs.edu.br', 'Rosangela Leal Bjerk', '', '', 'S', NULL, NULL, '2013-01-03 15:18:05', 10, NULL, NULL),
(239, 'Ernani Gottardo', '1796045', 'ernani.gottardo@erechim.ifrs.edu.br', 'Ernani Gottardo', '', '', 'S', NULL, NULL, '2013-01-03 15:23:31', 5, NULL, NULL),
(240, 'CRISTINE STELLA THOMAS', '1817987', 'cristine.thomas@poa.ifrs.edu.br', 'CRISTINE', '', '', 'S', NULL, NULL, '2013-01-03 15:28:12', 10, 3, NULL),
(241, 'GRAZIELA GUIMARAES', '017613329', 'GRAZIELA.GUIMARAES@BENTO.IFRS.EDU.BR', 'GRAZIELA GUIMARÃES', '', '', 'S', NULL, NULL, '2013-01-03 16:05:56', 2, NULL, NULL),
(242, 'Marina Wöhlke Cyrillo', '01799569', 'marina.cyrillo@poa.ifrs.edu.br', 'Marina Cyrillo', '', 'Carne suína', 'S', NULL, NULL, '2013-01-03 16:11:18', 10, NULL, NULL),
(243, 'Eliandra Lanfredi Bottin', '18054749', 'eliandra.bottin@farroupilha.ifrs.edu.br', 'Eliandra', '', '', 'S', NULL, NULL, '2013-01-03 16:50:54', 6, NULL, NULL),
(244, 'Luiz Gaspar Fensterseifer', '1503640', 'luiz.gaspar@bento.ifrs.edu.br', 'Luiz Gaspar', '', '', 'S', NULL, NULL, '2013-01-03 17:01:05', 2, NULL, NULL),
(245, 'Glaucia da Silva Henge', '1823149', 'glaucia.henge@canoas.ifrs.edu.br', 'Glaucia', '', '', 'S', NULL, NULL, '2013-01-03 17:52:43', 3, NULL, NULL),
(246, 'ELISANGELA BATISTA MACIEL RODRIGUES', '01798833', 'elisangela.maciel@bento.ifrs.edu.br', 'ELIS MACIEL RODRIGUES', '', '', 'S', NULL, NULL, '2013-01-03 20:44:55', 2, 1, NULL),
(247, 'Roben Castagna Lunardi', '1870459', 'roben.lunardi@restinga.ifrs.edu.br', 'Roben Lunardi', '', '', 'S', NULL, NULL, '2013-01-03 20:47:14', 11, NULL, NULL),
(248, 'Luís Carlos Cavalheiro da Silva', '1306249', 'cavalheiro@feliz.ifrs.edu.br', 'CAVALHEIRO', '', '', 'S', NULL, NULL, '2013-01-03 21:53:56', 7, NULL, NULL),
(249, 'Geanderson de Souza Lenz', '1796195', 'geanderson.lenz@osorio.ifrs.edu.br', 'Geanderson Lenz', '', '', 'S', NULL, NULL, '2013-01-04 00:10:24', 9, NULL, NULL),
(250, 'Suzana Ferreira da Rosa', '2917801', 'suzana.rosa@ibiruba.ifrs.edu.br', 'Suzana F. da Rosa', '', '', 'N', NULL, NULL, '2013-01-04 08:06:10', 8, NULL, NULL),
(251, 'Márcia Pereira Pedroso', '1810317', 'marcia.pedroso@restinga.ifrs.edu.br', 'Márcia Pedroso', '', '', 'S', NULL, NULL, '2013-01-04 08:07:41', 11, 1, NULL),
(252, 'WILSON ANTONIO ALVES FERREIRA', '1725288', 'WILSON.FERREIRA@RIOGRANDE.IFRS.EDU.BR', 'WILSON.FERREIRA', '', '', 'S', NULL, NULL, '2013-01-04 08:44:54', 12, 1, NULL),
(253, 'GISELE NAVARINI CINI', '017587255', 'gisele.cini@bento.ifrs.edu.br', 'GISELE NAVARINI CINI', '', '', 'S', NULL, NULL, '2013-01-04 09:01:39', 2, NULL, NULL),
(254, 'MARINEZ MAUER', '1797009', 'marinez.mauer@osorio.ifrs.edu.br', 'MARINEZ', '', '', 'S', NULL, NULL, '2013-01-04 09:15:05', 9, NULL, NULL),
(255, 'CRISTINA CERIBOLA CRESPAM', '2771111', 'cristina.crespam@poa.ifrs.edu.br', 'CRISTINA CRESPAM', '', '', 'S', NULL, NULL, '2013-01-04 09:40:20', 10, NULL, NULL),
(256, 'Fernanda Ottonelli Rossato', '1855654', 'fernanda.rossato@poa.ifrs.edu.br', 'Fernanda Ottonelli Rossato', '', '', 'S', NULL, NULL, '2013-01-04 09:45:04', 10, NULL, NULL),
(257, 'Carolina Gheller Miguens', '1818796', 'carolina.gheller@poa.ifrs.edu.br', 'Carolina Gheller', '', '', 'S', NULL, NULL, '2013-01-04 10:17:06', 10, NULL, NULL),
(258, 'ISABEL CRISTINA TEDESCO SELISTRE', '1916354', 'isabel.selistre@osorio.ifrs.edu.br', 'ISABEL SELISTRE', '', '', 'S', NULL, NULL, '2013-01-04 10:41:38', 9, NULL, NULL),
(259, 'Luiza Beatriz Londero de Oliveira', '18736580', 'luiza.oliveira@bento.ifrs.edu.br', 'Luiza Londero de Oliveira', '', '', 'S', NULL, NULL, '2013-01-04 12:48:10', 2, NULL, NULL),
(260, 'Claudia da Silva Gomes', '018275516', 'claudia.gomes@poa.ifrs.edu.br', 'Claudia Gomes', '', '', 'S', NULL, NULL, '2013-01-04 13:24:24', 10, NULL, NULL),
(261, 'JORGE ZANDONAI', '9912371', 'jorge.zandonai@bento.ifrs.edu.br', 'JORGE', '', '', 'S', NULL, NULL, '2013-01-04 14:07:05', 2, NULL, NULL),
(262, 'Marcelo Moraes Galarça', '15347273', 'marcelo.galarca@riogrande.ifrs.edu.br', 'Marcelo Galarça', '', 'Sim. Carne Vermelha.', 'S', NULL, NULL, '2013-01-04 16:40:08', 12, NULL, NULL),
(263, 'Maria Tereza Bolzon Soster', '014919699', 'maria.soster@sertao.ifrs.edu.br', 'Mª Tereza', '', '', 'S', NULL, NULL, '2013-01-04 17:00:49', 13, 3, NULL),
(264, 'Juliana dos Santos', '1668468', 'juliana.santos@sertao.ifrs.edu.br', 'Juliana Santos', '', '', 'S', NULL, NULL, '2013-01-04 17:22:44', 13, NULL, NULL),
(265, 'Carina Fior Postingher Balzan', '016458354', 'carina.balzan@bento.ifrs.edu.br', 'Carina Fior Postingher Balzan', '', '', 'S', NULL, NULL, '2013-01-04 18:05:37', 2, NULL, NULL),
(266, 'Débora Rodiguero de Andrade', '1809869', 'debora.rodiguero@caxias.ifrs.edu.br', 'Débora R. de Andrade', '', '', 'S', NULL, NULL, '2013-01-04 18:14:50', 4, NULL, NULL),
(267, 'Neiva Maria Bervian', '1443275', 'neiva.bervian@bento.ifrs.edu.br', 'Neiva Bervian', '', '', 'S', NULL, NULL, '2013-01-05 07:42:49', 2, NULL, NULL),
(268, 'Juraciara Paganella Peixoto', '1602040', 'jura.peixoto@bento.ifrs.edu.br', 'Juraciara', '', '', 'S', NULL, NULL, '2013-01-05 21:16:15', 2, NULL, NULL),
(269, 'Alexandra de Souza Fonseca', '1728775', 'alexandra.fonseca@caxias.ifrs.edu.br', 'Alexandra', '', '', 'S', NULL, NULL, '2013-01-05 22:38:02', 4, NULL, NULL),
(270, 'Eidi Alfredo Denti', '011951184', 'eidi.denti@sertao.ifrs.edu.br', 'Eidi Alfredo Denti', '', '', 'S', NULL, NULL, '2013-01-06 12:15:40', 13, NULL, NULL),
(271, 'LEILA DE ALMEIDA CASTILLO IABEL', '1889474', 'leila.iabel@sertao.ifrs.edu.br', 'LEILA', '', '', 'S', NULL, NULL, '2013-01-06 13:55:51', 13, NULL, NULL),
(272, 'Anderson Luis Nunes', '1565104', 'anderson.nunes@sertao.ifrs.edu.br', 'Anderson Nunes', '', '', 'S', NULL, NULL, '2013-01-06 19:18:02', 13, NULL, NULL),
(273, 'Noryam Bervian Bispo', '1887190', 'noryam.bispo@sertao.ifrs.edu.br', 'Noryam Bispo', '', '', 'S', NULL, NULL, '2013-01-06 19:19:53', 13, NULL, NULL),
(274, 'LUIS HENRIQUE BOFF', '1872276', 'luis.boff@sertao.ifrs.edu.br', 'Luis Henrique', '', '', 'S', NULL, NULL, '2013-01-06 20:37:04', 13, 3, NULL),
(275, 'MARIA LOURDES PARISOTTO', '1424126', 'mlourdes.parisotto@poa.ifrs.edu.br', 'MARIA LOURDES', '', '', 'S', NULL, NULL, '2013-01-06 20:44:42', 10, NULL, NULL),
(276, 'Marcos Antonio de Oliveira', '0492493', 'marcos.oliveira@sertao.ifrs.edu.br', 'MARCOS', '', '', 'S', NULL, NULL, '2013-01-06 21:07:49', 13, 2, NULL),
(277, 'ONORATO JONAS FAGHERAZZI', '1485265', 'onorato.fagherazzi@riogrande.ifrs.edu.br', 'ONORATO JONAS FAGHERAZZI', '', '', 'S', NULL, NULL, '2013-01-07 00:43:07', 12, NULL, NULL),
(278, 'Valdir Bernardo Tamanho', '1800638', 'valdir.tamanho@sertao.ifrs.edu.br', 'Valdir', '', '', 'S', NULL, NULL, '2013-01-07 08:27:00', 13, NULL, NULL),
(279, 'Andrei Nasser Wichrestink', '1982091', 'andrei.nasser@osorio.ifrs.edu.br', 'Andrei Nasser', '', '', 'S', NULL, NULL, '2013-01-07 08:32:53', 9, NULL, NULL),
(280, 'Clarissa Gracioli Camfield', '1613062', 'clarissa.gracioli@bento.ifrs.edu.br', 'Clarissa Camfield', '', '', 'S', NULL, NULL, '2013-01-07 09:19:59', 2, NULL, NULL),
(281, 'SERGIO RICARDO PEREIRA CARDOSO', '1530330', 'sergio.cardoso@riogrande.ifrs.edu.br', 'SERGIO CARDOSO', '', '', 'S', NULL, NULL, '2013-01-07 09:41:44', 12, NULL, NULL),
(282, 'Ana Paula Silva da Luz', '1806145', 'ana.luz@osorio.ifrs.edu.br', 'Ana Paula Luz', '', '', 'S', NULL, NULL, '2013-01-07 10:37:58', 9, NULL, NULL),
(283, 'Natálie Pacheco Oliveira', '1875153', 'natalie.oliveira@farroupilha.ifrs.edu.br', 'Natálie Oliveira', '', '', 'S', NULL, NULL, '2013-01-07 10:38:56', 6, NULL, NULL),
(284, 'Ivan Prá', '1644994', 'ivan.pra@feliz.ifrs.edu.br', 'Ivan', '', '', 'S', NULL, NULL, '2013-01-07 11:18:45', 7, NULL, NULL),
(285, 'Mauro Cristian Garcia Rickes', '1550406', 'mauro.rickes@riogrande.ifrs.edu.br', 'Mauro C. G. Rickes', '', '', 'S', NULL, NULL, '2013-01-07 11:29:37', 12, NULL, NULL),
(286, 'Wagner Luiz Priamo', '1791666', 'wagner.priamo@sertao.ifrs.edu.br', 'Wagner Priamo', '', '', 'S', NULL, NULL, '2013-01-07 11:45:58', 13, NULL, NULL),
(287, 'Vanderlei Rodrigo Bettiol', '1804685', 'vanderlei.bettiol@sertao.ifrs.edu.br', 'Vanderlei', '', '', 'S', NULL, NULL, '2013-01-07 12:40:24', 13, NULL, NULL),
(288, 'Caroline Daiane Kulba', '1817750', 'caroline.kulba@restinga.ifrs.edu.br', 'Carol', '', '', 'S', NULL, NULL, '2013-01-07 12:47:54', 11, NULL, NULL),
(289, 'CLAUDINE POSSOLI BELTRAM', '2528521', 'claudine.beltram@bento.ifrs.edu.br', 'CLAUDINE', '', '', 'S', NULL, NULL, '2013-01-07 13:23:24', 2, NULL, NULL),
(290, 'CARLA ROSANGELA WACHHOLZ', '1822819', 'carla.wachholz@farroupilha.ifrs.edu.br', 'CARLA WACHHOLZ', '', '', 'S', NULL, NULL, '2013-01-07 13:52:10', 6, NULL, NULL),
(291, 'CARINE MAYER ANDRADE', '1982918', 'carine.andrade@farroupilha.ifrs.edu.br', 'Carine Mayer', '', '', 'S', NULL, NULL, '2013-01-07 14:02:39', 6, NULL, NULL),
(292, 'Josiane Roberta Krebs', '1819738', 'josiane.krebs@caxias.ifrs.edu.br', 'Josiane', '', '', 'S', NULL, NULL, '2013-01-07 14:03:59', 4, NULL, NULL),
(293, 'Daniela Lupinacci Villanova', '1446153', 'daniela.villanova@farroupilha.ifrs.edu.br', 'Daniela Villanova', '', '', 'S', NULL, NULL, '2013-01-07 14:17:13', 6, NULL, NULL),
(294, 'Rosilene Rodrigues Kaizer Perin', '1758751', 'rosilene.perin@sertao.ifrs.edu.br', 'Rosilene Perin', '', '', 'S', NULL, NULL, '2013-01-07 14:23:42', 13, NULL, NULL),
(295, 'Ivone Taderka', '1874599', 'ivone.taderka@farroupilha.ifrs.edu.br', 'Ivone', '', 'Sim, intolerância ao alho.', 'S', NULL, NULL, '2013-01-07 14:27:45', 6, NULL, NULL),
(296, 'Simone Cazzarotto', '1802286', 'simone.cazzarotto@osorio.ifrs.edu.br', 'Simone', '', 'Intolerância à lactose', 'S', NULL, NULL, '2013-01-07 15:56:29', 9, NULL, NULL),
(297, 'Marcelo Juarez Vizzotto', '1811434', 'marcelo.vizzotto@ifrs.edu.br', 'Marcelo', '', '', 'S', NULL, NULL, '2013-01-07 16:59:05', 1, NULL, NULL),
(298, 'Ednei L. Becher', '1758729', 'ednei.becher@gmail.com', 'Ednei Becher', '', '', 'S', NULL, NULL, '2013-01-07 17:44:43', 9, 3, NULL),
(299, 'Delair Bavaresco', '016089758', 'delair@bento.ifrs.edu.br', 'Delair Bavaresco', '', '', 'S', NULL, NULL, '2013-01-07 22:19:30', 2, NULL, NULL),
(300, 'LIDIANE ZAMBENEDETTI', '1744290', 'lidiane.zambenedetti@erechim.ifrs.edu.br', 'LIDIANE ZAMBENEDETTI', '', '', 'S', NULL, NULL, '2013-01-08 08:26:30', 5, NULL, NULL),
(301, 'Lilian Carla Molon', '1665819', 'lilian.molon@bento.ifrs.edu.br', 'Lilian', '', '', 'S', NULL, NULL, '2013-01-08 08:28:23', 2, NULL, NULL),
(302, 'Rodrigo Ernesto Schroer', '1805285', 'rodrigo.schroer@caxias.ifrs.edu.br', 'Rodrigo E. Schroer', '', 'Sim, camarão', 'S', NULL, NULL, '2013-01-08 08:30:03', 4, NULL, NULL),
(303, 'Fernando José Simplicio', '018256351', 'fernando.simplicio@erechim.ifrs.edu.br', 'Simplicio', '', '', 'S', NULL, NULL, '2013-01-08 08:30:30', 5, NULL, NULL),
(304, 'Marta Panazzolo', '1982902', 'marta.panazzolo@caxias.ifrs.edu.br', 'Marta Panazzolo', '', 'Não posso comer QUEIJO', 'S', NULL, NULL, '2013-01-08 08:33:13', 4, NULL, NULL),
(305, 'Jorge Nunes Portela', '1313972', 'jorge.portela@bento.ifrs.edu.br', 'Portela', '', '', 'S', NULL, NULL, '2013-01-08 08:37:27', 2, NULL, NULL),
(306, 'Rodrigo Otávio Câmara Monteiro', '1609634', 'rodrigo.monteiro@bento.ifrs.edu.br', 'Rodrigo Monteiro', '', '', 'S', NULL, NULL, '2013-01-08 08:38:33', 2, NULL, NULL),
(307, 'Marli Daniel', '1830994', 'marli.daniel@ifrs.edu.br', 'Marli Daniel', '', '', 'S', NULL, NULL, '2013-01-08 08:50:53', 1, 2, NULL),
(308, 'Sandra Nicolli Piovesana', '1823992', 'sandra.piovesana@bento.ifrs.edu.br', 'Sandra Piovesana', '', '', 'S', NULL, NULL, '2013-01-08 08:56:25', 2, NULL, NULL),
(309, 'Lissandra Luvizão Lazzarotto', '1799020', 'lissandra.lazzarotto@bento.ifrs.edu.br', 'Lissandra Lazzarotto', '', '', 'S', NULL, NULL, '2013-01-08 08:57:15', 2, NULL, NULL),
(310, 'SUSANA BEATRIS OLIVEIRA SZEWCZYK', '1330503', 'susana.szewczyk@restinga.ifrs.edu.br', 'SUSANA SZEWCZYK', '', '', 'S', NULL, NULL, '2013-01-08 09:38:39', 11, NULL, NULL);
INSERT INTO `participantes` (`id`, `nome_completo`, `siape`, `email`, `nome_cracha`, `necessidade_especial`, `restricao_alimentar`, `confirmado`, `participacao`, `presente`, `data`, `campus_id`, `atividade_id`, `apresentacao_id`) VALUES
(311, 'Maria do Carmo Alves de Oliveira', '1103282', 'maria.oliveira@ifrs.edu.br', 'Maria do Carmo', '', '', 'S', NULL, NULL, '2013-01-08 09:49:17', 1, NULL, NULL),
(312, 'Caroline Possoli Beltram', '2718108', 'caroline.beltram@ifrs.edu.br', 'Caroline Beltram', '', 'Sim, sou vegetariana', 'S', NULL, NULL, '2013-01-08 09:52:13', 1, NULL, NULL),
(313, 'celia de souza osowski', '1346168', 'celia.souza@sertao.ifrs.edu.br', 'celia', '', '', 'S', NULL, NULL, '2013-01-08 09:55:35', 13, NULL, NULL),
(314, 'Méroli Saccardo dos Santos', '018051820', 'meroli.santos@erechim.ifrs.edu.br', 'Méroli Saccardo', '', '', 'S', NULL, NULL, '2013-01-08 09:56:21', 5, NULL, NULL),
(315, 'Alexsandro Cristovão Bonatto', '018275427', 'alexsandro.bonatto@restinga.ifrs.edu.br', 'Alexsandro Bonatto', '', '', 'S', NULL, NULL, '2013-01-08 09:57:00', 11, NULL, NULL),
(316, 'marinez silveira de oliveira', '1809814', 'marinez.silver@feliz.ifrs.edu.br', 'MARINEZ', '', '', 'S', NULL, NULL, '2013-01-08 10:12:30', 7, NULL, NULL),
(317, 'Andressa Comiotto', '2554176', 'andressa.comiotto@bento.ifrs.edu.br', 'Andressa Comiotto', '', '', 'S', NULL, NULL, '2013-01-08 10:40:52', 2, NULL, NULL),
(318, 'Vera Marisa Gasparetto', '1797094', 'veram.gasparetto@osorio.ifrs.edu.br', 'Vera', '', '', 'S', NULL, NULL, '2013-01-08 10:41:28', 9, NULL, NULL),
(319, 'Denise Beatris Tonin', '1808417', 'denise.tonin@caxias.ifrs.edu.br', 'Denise', '', '', 'S', NULL, NULL, '2013-01-08 10:49:01', 4, NULL, NULL),
(320, 'Francisco Leandro Barbosa', '1796118', 'francisco.barbosa@caxias.ifrs.edu.br', 'Leandro', '', '', 'S', NULL, NULL, '2013-01-08 10:55:45', 4, NULL, NULL),
(321, 'Divane Floreni Soares Leal', '017980054', 'divane.leal@restinga.ifrs.edu.br', 'Divane Leal', '', '', 'S', NULL, NULL, '2013-01-08 10:59:38', 11, 3, NULL),
(322, 'Elisane Roseli Ulrich Zanelato', '1447807', 'elisane.ulrich@sertao.ifrs.edu.br', 'Elisane', '', '', 'S', NULL, NULL, '2013-01-08 11:12:48', 13, NULL, NULL),
(323, 'ALEXANDRE VASCONCELOS LEITE', '409048', 'alexandre.leite@ifrs.edu.br', 'ALEXANDRE LEITE', '', '', 'S', NULL, NULL, '2013-01-08 11:15:51', 1, NULL, NULL),
(324, 'Luiz Carlos de Oliveira', '11044748', 'luiz.oliveira@sertao.ifrs.edu.br', 'Chico', '', '', 'S', NULL, NULL, '2013-01-08 11:30:08', 13, NULL, NULL),
(325, 'Danilo Franchini', '018193501', 'danilo.franchini@poa.ifrs.edu.br', 'Danilo Franchini', '', '', 'S', NULL, NULL, '2013-01-08 11:32:52', 10, NULL, NULL),
(326, 'André Rosa Martins', '03584917', 'andre.martins@poa.ifrs.edu.br', 'André Martins', '', '', 'S', NULL, NULL, '2013-01-08 11:37:05', 10, NULL, NULL),
(327, 'Fernando Angelo Pancotto Junior', '1592355', 'fernando.pancotto@bento.ifrs.edu.br', 'Fernando', '', '', 'S', NULL, NULL, '2013-01-08 11:37:24', 2, NULL, NULL),
(328, 'Lysandra Ramos Tieppo', '1979692', 'lysandra.tieppo@ifrs.edu.br', 'Lysandra', '', '', 'S', NULL, NULL, '2013-01-08 11:38:50', 1, NULL, NULL),
(329, 'Jéferson Luís dos Santos Xavier', '1105060', 'jeferson.xavier@sertao.ifrs.edu.br', 'Jéferson', '', '', 'S', NULL, NULL, '2013-01-08 11:49:25', 13, 3, NULL),
(330, 'Maiane Paula Basso', '2884703', 'maiane.basso@ifrs.edu.br', 'Maiane Basso', '', '', 'S', NULL, NULL, '2013-01-08 11:51:25', 1, NULL, NULL),
(331, 'Jaqueline Rosa da Cunha', '014759756', 'jaqueline.cunha@poa.ifrs.edu.br', 'Jaqueline Cunha', '', '', 'S', NULL, NULL, '2013-01-08 12:08:11', 10, NULL, NULL),
(332, 'Rui Manuel Cruse', '1742510', 'rui.cruse@restinga.ifrs.edu.br', 'Cruse, Rui', '', '', 'S', NULL, NULL, '2013-01-08 12:21:09', 11, NULL, NULL),
(333, 'Claudia Lorenzon', '16722396', 'claudia.lorenzon@bento.ifrs.edu.br', 'Claudia', '', '', 'S', NULL, NULL, '2013-01-08 13:02:48', 2, NULL, NULL),
(334, 'Anderson Favero Porte', '1828111', 'anderson.porte@riogrande.ifrs.edu.br', 'Anderson', '', '', 'S', NULL, NULL, '2013-01-08 13:21:26', 12, NULL, NULL),
(335, 'Robson Brum Guerra', '1760365', 'robson.guerra@sertao.ifrs.edu.br', 'Robson', '', '', 'S', NULL, NULL, '2013-01-08 13:27:01', 13, NULL, NULL),
(336, 'Marcio Luiz Tremarin', '1866507', 'marcio.tremarin@ifrs.edu.br', 'Marcio Tremarin', '', '', 'S', NULL, NULL, '2013-01-08 13:30:16', 1, NULL, NULL),
(337, 'GLÉCIA DOS SANTOS LABREA', '1888374', 'glecialabrea@yahoo.com.com', 'GLÉCIA LABREA', '', '', 'S', NULL, NULL, '2013-01-08 13:33:30', 1, NULL, NULL),
(338, 'Margarida Prestes de Souza', '1102332', 'pi@ifrs.edu.br', 'Margarida Souza', '', '', 'S', NULL, NULL, '2013-01-08 14:07:55', 1, 3, NULL),
(339, 'Cleber Rodrigo de Lima Lessa', '1898242', 'cleber.lessa@caxias.ifrs.edu.br', 'Cleber Lessa', '', '', 'S', NULL, NULL, '2013-01-08 14:24:36', 4, NULL, NULL),
(340, 'Soeni Belle', '1313978', 'soeni.belle@bento.ifrs.edu.br', 'Soeni', '', '', 'S', NULL, NULL, '2013-01-08 15:05:30', 2, NULL, NULL),
(341, 'GERSON RAFAEL JUCHEM', '1817183', 'GERSON.JUCHEM@IFRS.EDU.BR', 'GERSON RAFAEL', '', '', 'S', NULL, NULL, '2013-01-08 15:36:35', 1, NULL, NULL),
(342, 'Gleide Penha de Oliveira', '003588831', 'gleidhe.oliveira@poa.ifrs.edu.br', 'Gleidhe Oliveira', '', '', 'S', NULL, NULL, '2013-01-08 15:40:14', 10, NULL, NULL),
(343, 'Alexandre Ducatti', '019374615', 'alexandre.ducatti@bento.ifrs.edu.br', 'Alexandre', '', '', 'S', NULL, NULL, '2013-01-08 15:42:30', 2, NULL, NULL),
(344, 'ROGER GONÇALVES URDANGARIN', '1245721', 'ROGER.URDANGARIN@OSORIO.IFRS.EDU.BR', 'ROGER', '', 'MAIONESE, VEGETAIS VERDES', 'S', NULL, NULL, '2013-01-08 16:48:51', 9, NULL, NULL),
(345, 'RONALDO NUNES ORSINI', '1346337', 'ronaldo.orsini@canoas.ifrs.edu.br', 'RONALDO ORSINI', '', '', 'S', NULL, NULL, '2013-01-08 18:41:09', 3, NULL, NULL),
(346, 'Andréa Ribeiro Gonçalves Leal', '015250814', 'andrea.leal@poa.ifrs.edu.br', 'Andréa Leal', '', '', 'S', NULL, NULL, '2013-01-08 19:36:01', 10, 3, NULL),
(347, 'Michelen Tatiane Rodrigues Franco de Campos Andrighetto', '1669169', 'michelen.andrighetto@osorio.ifrs.edu.br', 'Michelen Andrighetto', '', '', 'S', NULL, NULL, '2013-01-08 21:42:55', 9, NULL, NULL),
(348, 'Keila Nicchelle', '1742845', 'keila.nicchelle@erechim.ifrs.edu.br', 'Keila Nicchelle', '', '', 'S', NULL, NULL, '2013-01-08 21:55:33', 5, NULL, NULL),
(349, 'André Marcelo Schneider', '1590830', 'andre@ifrs.edu.br', 'Schneider', '', '', 'S', NULL, NULL, '2013-01-09 05:50:01', 11, NULL, NULL),
(350, 'Silvia Ozorio Rosa', '1983924', 'silvia.rosa@farroupilha.ifrs.edu.br', 'Silvia Ozorio', '', '', 'S', NULL, NULL, '2013-01-09 08:09:24', 6, NULL, NULL),
(351, 'Michel Víctor Gasperin Krindges', '1669338', 'michel.gasperin@ifrs.edu.br', 'Michel Gasperin', '', '', 'S', NULL, NULL, '2013-01-09 08:48:15', 1, NULL, NULL),
(352, 'Ana Sara Castaman', '1901102', 'ana.castaman@sertao.ifrs.edu.br', 'Ana Sara Castaman', '', '', 'S', NULL, NULL, '2013-01-09 09:38:45', 13, NULL, NULL),
(353, 'beatriz portanova de oliveira', '1968421', 'beatriz.oliveira@ifrs.edu.br', 'beatriz portanova', '', '', 'S', NULL, NULL, '2013-01-09 09:43:28', 1, NULL, NULL),
(354, 'ELÍSIO DE CAMARGO DEBORTOLI', '1625600', 'elisio.debortoli@sertao.ifrs.edu.br', 'ELÍSIO DEBORTOLI', '', '', 'S', NULL, NULL, '2013-01-09 09:44:48', 13, NULL, NULL),
(355, 'Glenda Heller Cáceres', '1623766', 'glenda.caceres@bento.ifrs.edu.br', 'Glenda Cáceres', '', '', 'S', NULL, NULL, '2013-01-09 10:12:43', 2, 1, NULL),
(356, 'Nelson Duarte da Silva', '1128030', 'nelson.duarte@sertao.ifrs.edu.br', 'Nelson Duarte', '', '', 'S', NULL, NULL, '2013-01-09 11:00:43', 13, NULL, NULL),
(357, 'REJANE RITZEL FERREIRA', '1695432', 'rejane.ferreira@farroupilha.ifrs.edu.br', 'REJANE RITZEL', '', '', 'S', NULL, NULL, '2013-01-09 11:38:56', 6, NULL, 2),
(358, 'Tiago Borges Ribeiro Gandra', '2777751', 'tiago.gandra@riogrande.ifrs.edu.br', 'Gandra', '', '', 'S', NULL, NULL, '2013-01-09 13:28:01', 12, NULL, NULL),
(359, 'Ricardo Vasconcelo Silva', '1837689', 'ricardo.silva@farroupilha.ifrs.edu.br', 'Ricardo Vasconcelo', '', '', 'S', NULL, NULL, '2013-01-09 14:12:00', 13, NULL, NULL),
(360, 'Juliana Marcia Rogalski', '1738054', 'juliana.rogalski@sertao.ifrs.edu.br', 'Juliana', '', 'Sim, pouco sal', 'S', NULL, NULL, '2013-01-09 14:23:58', 13, NULL, NULL),
(361, 'Martha Helena Weizenmann', '1818426', 'martha.helena@poa.ifrs.edu.br', 'Martha Weizenmann', '', '', 'S', NULL, NULL, '2013-01-09 14:25:32', 10, NULL, NULL),
(362, 'Juliana Prediger', '1644973', 'juliana.prediger@poa.ifrs.edu.br', 'Juliana', '', '', 'S', NULL, NULL, '2013-01-09 14:27:12', 10, NULL, NULL),
(363, 'Cibele Schwanke', '1315938', 'cibele.schwanke@poa.ifrs.edu.br', 'Cibele Schwanke', '', '', 'S', NULL, NULL, '2013-01-09 15:11:17', 10, NULL, NULL),
(364, 'Marcia Fritsch Gonçalves', '019726368', 'marcia.goncalves@bento.ifrs.edu.br', 'Marcia Gonçalves', '', '', 'S', NULL, NULL, '2013-01-09 19:11:15', 2, NULL, NULL),
(365, 'Clarice Monteiro Escott', '017966493', 'clarice.escott@ifrs.edu.br', 'Clarice Monteiro Escott', '', '', 'S', NULL, NULL, '2013-01-09 21:02:47', 1, 1, NULL),
(366, 'Marcos Paulo Ludwig', '1645508', 'marcos.ludwig@ibiruba.ifrs.edu.br', 'Marcos Ludwig', '', '', 'N', NULL, NULL, '2013-01-10 10:09:09', 8, NULL, NULL),
(367, 'HENRIETTE DE MATTOS PINTO DE FREITAS', '1575091', 'henriette.freitas@riogrande.ifrs.edu.br', 'HENRIETTE FREITAS', '', '', 'S', NULL, NULL, '2013-01-10 10:28:53', 12, NULL, NULL),
(368, 'CONCEIÇÃO APARECIDA GONÇALVES DESTRO', '1985836', 'conceicao.destro@ifrs.edu.br', 'CONCEIÇÃO A. G. DESTRO', '', '', 'S', NULL, NULL, '2013-01-10 11:40:01', 1, 3, NULL),
(369, 'Gilberto Luíz Putti', '1616518', 'gilberto.putti@bento.ifrs.edu.br', 'Gilberto Putti', '', '', 'S', NULL, NULL, '2013-01-10 13:33:03', 2, NULL, NULL),
(370, 'Andréia Maria Pruinelli', '1626042', 'andreia.pruinelli@canoas.ifrs.edu.br', 'Andréia Pruinelli', '', '', 'S', NULL, NULL, '2013-01-10 14:56:26', 3, NULL, NULL),
(371, 'caio graco prates alegretti', '1816257', 'caio.alegretti@canoas.ifrs.edu.br', 'caio', '', '', 'S', NULL, NULL, '2013-01-10 16:09:11', 3, NULL, NULL),
(372, 'Vagner Euzébio Bastos', '1332867', 'vagner.bastos@riogrande.ifrs.edu.br', 'Vagner Bastos', '', '', 'S', NULL, NULL, '2013-01-10 22:21:48', 12, 3, NULL),
(373, 'Vagner Euzébio bastos', '013328670', 'vagner.bastos@riogrande.ifrs.edu.br', 'Vagner Bastos', '', '', 'N', NULL, NULL, '2013-01-10 22:26:11', 12, 3, NULL),
(374, 'Ana Paula Wilke François', '1876907', 'ana.francois@riogrande.ifrs.edu.br', 'Ana Paula François', '', 'Sim, restrição a carne de gado e frango.', 'S', NULL, NULL, '2013-01-11 09:50:24', 12, NULL, NULL),
(375, 'DUILIO CASTRO MILES', '017687020', 'duilio.miles@poa.ifrs.edu.br', 'DUILIO', '', '', 'S', NULL, NULL, '2013-01-11 10:13:14', 10, NULL, NULL),
(376, 'NÁDIA CRISTINA POLETTO', '1222639', 'nadia.poletto@bento.ifrs.edu.br', 'NÁDIA POLETTO', '', '', 'S', NULL, NULL, '2013-01-11 10:23:16', 2, NULL, NULL),
(377, 'PATRICIA PROCHNOW', '1671725', 'patricia.prochnow@osorio.ifrs.edu.br', 'PATRICIA PROCHNOW', '', '', 'S', NULL, NULL, '2013-01-11 11:16:16', 9, NULL, NULL),
(378, 'Luis Claudio Gubert', '1781712', 'luis.gubert@ibiruba.ifrs.edu.br', 'Gubert', '', 'Intolerância a lactose (especificamente ao leite)', 'S', NULL, NULL, '2013-01-11 13:06:57', 8, 3, NULL),
(379, 'Andréia Teresinha Saldanha Gradin', '1106125', 'andreia.gradin@sertao.ifrs.edu.br', 'Andréia', '', '', 'S', NULL, NULL, '2013-01-11 19:45:32', 13, NULL, NULL),
(380, 'Valéria de Oliveira Silveira', '1932916', 'valeria.silveira@osorio.ifrs.edu.br', 'Valéria Silveira', '', '', 'S', NULL, NULL, '2013-01-11 22:18:50', 9, NULL, NULL),
(381, 'Cleber Schroeder Fonseca', '1642142', 'cleber.fonseca@ifrs.edu.br', 'Cleber Fonseca', '', '', 'S', NULL, NULL, '2013-01-12 01:54:17', 1, NULL, NULL),
(382, 'SUZANA PRESTES DE OLIVEIRA', '1811459', 'suzana.prestes@poa.ifrs.edu.br', 'SUZANA PRESTES', '', '', 'S', NULL, NULL, '2013-01-12 13:08:52', 10, 3, NULL),
(383, 'Fabiana Cardoso Fidelis', '1309523', 'fabiana.fidelis@canoas.ifrs.edu.br', 'Fabiana Fidelis', '', '', 'S', NULL, NULL, '2013-01-13 18:13:25', 3, NULL, NULL),
(384, 'Rafaela Fetzner Drey', '1823902', 'rafaela.drey@osorio.ifrs.edu.br', 'Rafaela Drey', '', '', 'S', NULL, NULL, '2013-01-14 08:36:08', 9, NULL, NULL),
(385, 'Marcio Santin', '018038921', 'marcio.santin@ifrs.edu.br', 'Marcio Santin', '', '', 'S', NULL, NULL, '2013-01-14 10:44:28', 1, NULL, NULL),
(386, 'Sandra Denise Stroschein', '2894505', 'sandra.stroschein@bento.ifrs.edu.br', 'Sandra Denise Stroschein', '', '', 'S', NULL, NULL, '2013-01-14 14:35:39', 2, NULL, NULL),
(387, 'WENDELL RIBEIRO E SILVA', '1635275', 'wendell.ribeiro@ifrs.edu.br', 'WENDELL', '', '', 'S', NULL, NULL, '2013-01-14 15:20:56', 1, 3, NULL),
(388, 'Sônia Gotler', '018666655', 'sonia.gotler@farroupilha.ifrs.edu.br', 'Sônia Gotler', '', '', 'S', NULL, NULL, '2013-01-14 15:38:46', 6, NULL, NULL),
(389, 'Eliza Terres Camargo', '1600725', 'eliza.camargo@riogrande.ifrs.edu.br', 'Eliza Camargo', '', '', 'S', NULL, NULL, '2013-01-14 15:44:30', 12, NULL, NULL),
(390, 'Priscila de Pinho Valente', '1825901', 'priscila.valente@riogrande.ifrs.edu.br', 'Priscila Valente', '', '', 'S', NULL, NULL, '2013-01-14 15:52:14', 12, NULL, NULL),
(391, 'JAQUELINE JUSTEN', '1979619', 'jaqueline.justen@bento.ifrs.edu.br', 'JAQUELINE', '', '', 'S', NULL, NULL, '2013-01-14 16:11:53', 2, NULL, NULL),
(392, 'Simão Mendes de Moraes', '1479893', 'simao.moraes@caxias.ifrs.edu.br', 'Simão', 'Nenhuma', 'Frutas, verdura e legumes crus, sou alérgico', 'S', NULL, NULL, '2013-01-14 17:03:26', 4, 1, NULL),
(393, 'Elizabethe Terezinha Pitt Giacomazzi', '1110924', 'bethe.giacomazzi@bento.ifrs.edu.br', 'Bethe', '', '', 'S', NULL, NULL, '2013-01-14 17:40:35', 2, NULL, NULL),
(394, 'Evandro Schlumpf', '1982919', 'evandro.schlumpf@feliz.ifrs.edu.br', 'Evandro', '', '', 'S', NULL, NULL, '2013-01-14 21:24:45', 7, NULL, NULL),
(395, 'Marilene Rosa Miola', '0049232', 'marilene.miola@poa.ifrs.edu.br', 'Marilene Rosa Miola', '', '', 'S', NULL, NULL, '2013-01-14 23:07:02', 10, NULL, NULL),
(396, 'LEONIRCE  ROSA', '00491802', 'leonirce.rosa@sertao.ifrs.edu.br', 'LEONIRCE', '----', '----', 'S', NULL, NULL, '2013-01-15 09:21:56', 13, NULL, NULL),
(397, 'Jairo Antonio Wagner', '1756751', 'jairo.wagner@ifrs.edu.br', 'Jairo', '', '', 'S', NULL, NULL, '2013-01-15 11:25:58', 1, NULL, NULL),
(398, 'ROSANE SOARES DE CARVALHO DUARTE', '1638007', 'rosane.duarte@riogrande.ifrs.edu.br', 'ROSANE DUARTE', '', '', 'S', NULL, NULL, '2013-01-15 12:01:36', 12, 3, NULL),
(399, 'Ionara Cristina Albani', '1875465', 'ionara.albani@riogrande.ifrs.edu.br', 'Ionara', '', '', 'S', NULL, NULL, '2013-01-15 13:08:59', 12, NULL, NULL),
(400, 'ALISSON PAESE', '1987175', 'alisson.paese@ifrs.edu.br', 'ALISSON PAESE', '', '', 'S', NULL, NULL, '2013-01-15 13:46:09', 1, NULL, NULL),
(401, 'THAIS ROBERTA KOCH', '1811427', 'thais.koch@farroupilha.ifrs.edu.br', 'THAIS ROBERTA KOCH', '', '', 'S', NULL, NULL, '2013-01-15 14:49:00', 6, NULL, NULL),
(402, 'ANTONIO LUIS ROMAGNA', '1638755', 'antonio.romagna@bento.ifrs.edu.br', 'ROMAGNA', '', '', 'S', NULL, NULL, '2013-01-15 16:37:28', 2, NULL, NULL),
(403, 'sergio gambarra da silva', '1810561', 'sergio.gambarra@restinga.ifrs.edu.br', 'Sérgio Gambarra', '', '', 'S', NULL, NULL, '2013-01-16 09:18:13', 11, 3, NULL),
(404, 'Adilar Chaves', '000529915', 'adilar.chaves@sertao.ifrs.edu.br', 'Adilar Chaves', '', '', 'S', NULL, NULL, '2013-01-16 09:55:10', 13, NULL, NULL),
(405, 'Priscila Pedrosos', '1983906', 'priscila.pedrosos@sertao.ifrs.edu.br', 'Priscila', '', '', 'S', NULL, NULL, '2013-01-16 10:54:32', 13, NULL, NULL),
(406, 'Eliana Pinho de Azambuja', '1044717', 'eliana.pinho@riogrande.ifrs.edu.br', 'Eliana', '', '', 'S', NULL, NULL, '2013-01-16 10:59:04', 12, 3, NULL),
(407, 'Carla Regina Andre Silva', '1330299', 'carla.andre@riogrande.ifrs.edu.br', 'Carla Andre', '', '', 'S', NULL, NULL, '2013-01-16 11:01:52', 12, 3, NULL),
(408, 'PAULA PORTO PEDONE', '1645820', 'paula.pedone@restinga.ifrs.edu.br', 'PAULA PEDONE', '', '', 'S', NULL, NULL, '2013-01-16 11:40:56', 11, NULL, NULL),
(409, 'Gilmar da Luz Jr', '1829729', 'gilmar.daluz@farroupilha.ifrs.edu.br', 'Gilmar Jr', '', '', 'S', NULL, NULL, '2013-01-16 11:44:06', 6, NULL, NULL),
(410, 'EDUARDO PINHEIRO DE FREITAS', '1669371', 'eduardo.freitas@bento.ifrs.edu.br', 'EDUARDO FREITAS', '', '', 'S', NULL, NULL, '2013-01-16 11:45:35', 2, NULL, NULL),
(411, 'Antonio Braz da Silva Neto', '1804575', 'antonio.braz@caxias.ifrs.edu.br', 'Antonio Braz', '', '', 'S', NULL, NULL, '2013-01-16 11:46:45', 4, NULL, NULL),
(412, 'Liliane Dufau da Silva', '1135374', 'liliane.silva@poa.ifrs.edu.br', 'Liliane Silva', '', '', 'S', NULL, NULL, '2013-01-16 11:52:07', 10, 3, NULL),
(413, 'Jonatan Maicon Antônio Tonin', '018710352', 'jonatan.tonin@bento.ifrs.edu.br', 'Jonatan - BG', '', '', 'S', NULL, NULL, '2013-01-16 11:52:37', 2, NULL, NULL),
(414, 'Jorge da Luz Matos', '1354730', 'jorge.matos@farroupilha.ifrs.edu.br', 'Jorge Matos', '', '', 'S', NULL, NULL, '2013-01-16 12:00:33', 6, 3, NULL),
(415, 'Nisia Gomes Fischer', '1363061', 'nisia.fischer@poa.ifrs.edu.br', 'Nisia', '', '', 'S', NULL, NULL, '2013-01-16 12:00:51', 10, NULL, NULL),
(416, 'Eduardo Fernandes Antunes', '1818514', 'eduardo.antunes@ibiruba.ifrs.edu.br', 'Eduardo Fernandes Antunes', '', '', 'S', NULL, NULL, '2013-01-16 12:03:33', 8, NULL, NULL),
(417, 'Yuri Ferreira Machado', '3789570', 'yuri.machado@poa.ifrs.edu.br', 'Yuri F', '', '', 'S', NULL, NULL, '2013-01-16 12:04:38', 10, NULL, 2),
(418, 'Francisco Renato Wisniewski do Prado', '1936972', 'francisco.prado@poa.ifrs.edu.br', 'Prado', '', '', 'S', NULL, NULL, '2013-01-16 12:07:18', 10, NULL, NULL),
(419, 'Evandro', '1796652', 'evandro.miletto@poa.ifrs.edu.br', 'Evandro Manara Miletto', '', '', 'S', NULL, NULL, '2013-01-16 12:14:43', 10, NULL, NULL),
(420, 'ADEMIR GAUTÉRIO TROINA JÚNIOR', '1798912', 'ademir.troina@poa.ifrs.edu.br', 'ADEMIR TROINA', '', '', 'S', NULL, NULL, '2013-01-16 12:29:55', 10, NULL, NULL),
(421, 'Denise Luzia Wolff', '1527133', 'denise.wolff@poa.ifrs.edu.br', 'Denise Wolff', '', '', 'N', NULL, NULL, '2013-01-16 13:01:53', 10, NULL, NULL),
(422, 'Elisa Daminelli', '1817805', 'elisa.daminelli@osorio.ifrs.edu.br', 'Elisa Daminelli', '', '', 'S', NULL, NULL, '2013-01-16 13:02:28', 9, NULL, NULL),
(423, 'Milene Mecca Hannecker', '018651259', 'milene.hannecker@bento.ifrs.edu.br', 'Milene', '', '', 'S', NULL, NULL, '2013-01-16 13:14:09', 2, NULL, NULL),
(424, 'Agnes Schmeling', '351568', 'agnes.schmeling@poa.ifrs.edu.br', 'Agnes', '', '', 'S', NULL, NULL, '2013-01-16 13:14:45', 9, NULL, NULL),
(425, 'Jean Carlo Hamerski', '1796206', 'jean.hamerski@restinga.ifrs.edu.br', 'Jean Carlo Hamerski', '', '', 'S', NULL, NULL, '2013-01-16 13:26:02', 11, NULL, NULL),
(426, 'Marindia Zeni', '2667264', 'marindia.zeni@sertao.ifrs.edu.br', 'Marindia', '', '', 'S', NULL, NULL, '2013-01-16 13:27:25', 13, NULL, NULL),
(427, 'Aline Silva De Bona', '0182297435', 'aline.bona@poa.ifrs.edu.br', 'Aline De Bona', '', '', 'N', NULL, NULL, '2013-01-16 13:31:07', 10, NULL, NULL),
(428, 'Ceslon Roberto Canto Silva', '1435562', 'celson.silva@poa.ifrs.edu.br', 'Celson Silva', '', '', 'S', NULL, NULL, '2013-01-16 13:36:02', 10, NULL, 2),
(429, 'Simone Caterina Kapusta', '1526806', 'simone.kapusta@poa.ifrs.edu.br', 'Simone Caterina Kapusta', '', '', 'S', NULL, NULL, '2013-01-16 13:39:56', 10, NULL, NULL),
(430, 'Sula Cristina Teixeira Nunes', '1826439', 'sula.nunes@restinga.ifrs.edu.br', 'Sula', '', 'Sim, camarão.', 'S', NULL, NULL, '2013-01-16 13:42:42', 11, NULL, NULL),
(431, 'Magali da Silva Rodrigues', '1418850', 'magali.rodrigues@poa.ifrs.edu.br', 'Magali Rodrigues', '', '', 'S', NULL, NULL, '2013-01-16 13:45:37', 10, NULL, NULL),
(432, 'Vinícius Michelin', '1737166', 'vini.michelin@erechim.ifrs.edu.br', 'Vinícius Michelin', '', '', 'S', NULL, NULL, '2013-01-16 13:57:54', 5, NULL, NULL),
(433, 'Núbia Marta Laux', '1809812', 'nubia.laux@feliz.ifrs.edu.br', 'Núbia', '', '', 'S', NULL, NULL, '2013-01-16 14:00:02', 7, NULL, NULL),
(434, 'RAQUEL BREITENBACH', '1805938', 'raquel.breitenbach@sertao.ifrs.edu.br', 'RAQUEL BREITENBACH', '', '', 'S', NULL, NULL, '2013-01-16 14:09:29', 13, NULL, NULL),
(435, 'Lisiane Trevisan', '017683858', 'lisiane.trevisan@farroupilha.ifrs.edu.br', 'Lisiane', '', '', 'S', NULL, NULL, '2013-01-16 14:14:45', 6, NULL, NULL),
(436, 'Tissiane Schmidt Dolci', '18096700', 'tissiane.dolci@caxias.ifrs.edu.br', 'Tissi', '', '', 'S', NULL, NULL, '2013-01-16 14:23:27', 4, NULL, NULL),
(437, 'Everaldo Carniel', '01757383', 'everaldo.carniel@bento.ifrs.edu.br', 'Everaldo', 'Deficiência visual (cego)', '', 'S', NULL, NULL, '2013-01-16 14:28:37', 2, 3, NULL),
(438, 'Elisabeth ibi Frimm Krieger', '017749530', 'ibi.krieger@poa.ifrs.edu.br', 'Ibi', '', '', 'S', NULL, NULL, '2013-01-16 14:39:17', 10, NULL, NULL),
(439, 'Nilo Barcelos Alves', '017960223', 'nilo.alves@osorio.ifrs.edu.br', 'Nilo', '', '', 'S', NULL, NULL, '2013-01-16 14:50:41', 9, NULL, NULL),
(440, 'Ivair Nilton Pigozzo', '1810325', 'Ivair.pigozzo@ifrs.edu.br', 'Ivair Pigozzo', '', '', 'S', NULL, NULL, '2013-01-16 14:53:28', 1, 3, NULL),
(441, 'KEILA CRISTINA DA ROSA', '1827956', 'keila.rosa@ifrs.edu.br', 'KEILA CRISTINA DA ROSA', '', '', 'S', NULL, NULL, '2013-01-16 15:06:46', 5, NULL, NULL),
(442, 'Deise Inara Cremonini Dagnese', '1812379', 'deise.dagnese@farroupilha.ifrs.edu.br', 'Deise Dagnese', '', '', 'S', NULL, NULL, '2013-01-16 15:41:58', 6, NULL, NULL),
(443, 'Hernanda Tonini', '1853963', 'hernanda.tonini@restinga.ifrs.edu.br', 'Hernanda Tonini', '', '', 'S', NULL, NULL, '2013-01-16 15:55:00', 11, NULL, NULL),
(444, 'Gisele Mion Gugel', '1819773', 'gisele.gugel@bento.ifrs.edu.br', 'Gisele Gugel', '', 'ácido acético (vinagre)', 'S', NULL, NULL, '2013-01-16 15:55:02', 2, NULL, NULL),
(445, 'Shana Paula Segala Miotto', '018292410', 'paula.miotto@bento.ifrs.edu.br', 'Paula Miotto', '', '', 'S', NULL, NULL, '2013-01-16 16:29:25', 2, NULL, NULL),
(446, 'FLÁVIO GALDINO XAVIER', '1110953', 'flavio.xavier@riogrande.ifrs.edu.br', 'GALDINO', '', '', 'S', NULL, NULL, '2013-01-16 18:11:24', 12, NULL, NULL),
(447, 'Clarisse Hammes Perinazzo', '1737008', 'clarisse.perinazzo@erechim.ifrs.edu.br', 'Clarisse', '', '', 'S', NULL, NULL, '2013-01-16 18:34:42', 5, NULL, NULL),
(448, 'Eduardo Girotto', '1893215', 'eduardo.girotto@ibiruba.ifrs.com.br', 'Eduardo Girotto', '', '', 'S', NULL, NULL, '2013-01-16 19:57:45', 8, NULL, NULL),
(449, 'Serguei Nogueira da Silva', '1743068', 'serguei.silva@riogrande.ifrs.edu.br', 'Serguei Silva', '', '', 'S', NULL, NULL, '2013-01-16 20:21:04', 12, NULL, NULL),
(450, 'Ivete Scariot', '1480913', 'ivete.scariot@sertao.ifrs.edu.br', 'IVETE', '', '', 'S', NULL, NULL, '2013-01-16 20:36:22', 11, NULL, NULL),
(451, 'Shana Sabbado Flores', '1796017', 'shana.flores@restinga.ifrs.edu.br', 'Shana', '', 'Carne vermelha e seus derivados (presunto, salsicha, salame, etc) e frango', 'S', NULL, NULL, '2013-01-17 09:15:25', 11, NULL, NULL),
(452, 'ALMIR ANTONIO VALENTI', '1103094', 'almir.valenti@ifrs.edu.br', 'VALENTI', '', 'Tambem não', 'S', NULL, NULL, '2013-01-17 09:52:32', 1, NULL, NULL),
(453, 'Darci Emiliano', '1105050', 'darci.emiliano@sertao.ifrs.edu.br', 'Índio', '', '', 'S', NULL, NULL, '2013-01-17 11:34:50', 13, NULL, NULL),
(454, 'Camila Cusin Dorneles', '1979127', 'camila.dorneles@ifrs.edu.br', 'Camila Dorneles', '', '', 'S', NULL, NULL, '2013-01-17 11:50:49', 2, NULL, NULL),
(455, 'Joel Augusto Luft', '1796588', 'joel.luft@canoas.ifrs.edu.br', 'Joel Augusto Luft', '', '', 'S', NULL, NULL, '2013-01-17 16:33:22', 3, NULL, NULL),
(456, 'enio antonio florencio', '1105067', 'enio.florencio@sertao.ifrs.edu.br', 'enio', '', '', 'S', NULL, NULL, '2013-01-17 19:46:45', 13, NULL, NULL),
(457, 'PEDRO AROLDO FLORES', '014477939', 'pedro.flores@sertao.ifrs.edu.br', 'PEDRO', '', '', 'S', NULL, NULL, '2013-01-18 10:28:15', 13, NULL, NULL),
(458, 'LEILA SCHWARZ', '18691609', 'leila.schwarz@sertao.ifrs.edu.br', 'Leila', '', '', 'S', NULL, NULL, '2013-01-18 14:35:22', 13, NULL, NULL),
(459, 'NAIARA MIGON', '1823481', 'naiara.migon@sertao.ifrs.edu.br', 'Naiara', '', '', 'S', NULL, NULL, '2013-01-18 14:38:30', 13, NULL, NULL),
(460, 'Alexandre Estive Malinowski', '1827491', 'alexandre.malinowski@poa.ifrs.edu.br', 'Alexandre Malinowski', '', '', 'S', NULL, NULL, '2013-01-18 15:13:24', 10, NULL, NULL),
(461, 'Edgar José Stello Junior', '2650884', 'stello.junior@ifrs.edu.br', 'E. Stello Jr', '', '', 'S', NULL, NULL, '2013-01-18 15:32:52', 1, NULL, NULL),
(462, 'Nara Milbrath de OLiveira', '1667822', 'nara.oliveira@canoas.ifrs.edu.br', 'Nara de Oliveira', '', '', 'S', NULL, NULL, '2013-01-18 15:51:36', 3, NULL, NULL),
(463, 'Eder Bortolotto Silva', '1982589', 'eder.silva@ifrs.edu.br', 'Eder Bortolotto Silva', '', '', 'S', NULL, NULL, '2013-01-18 15:56:16', 1, NULL, NULL),
(464, 'EVA REGINA AMARAL', '1812798', 'eva.amaral@farroupilha.ifrs.edu.br', 'EVA AMARAL', 'não, mas utilizo bengalas', 'sal, açucar e gorduras', 'S', NULL, NULL, '2013-01-18 15:56:45', 6, 2, NULL),
(465, 'Fernanda Alves de Paiva', '1646722', 'fernanda.paiva@sertao.ifrs.edu.br', 'Fernanda Paiva', '', '', 'S', NULL, NULL, '2013-01-18 17:20:03', 13, NULL, NULL),
(466, 'Andre Luciano Ciotta', '1743067', 'andre.ciotta@erechim.ifrs.edu.br', 'Andre Luciano', '', '', 'S', NULL, NULL, '2013-01-18 17:40:13', 5, NULL, NULL),
(467, 'FERNANDO FOLLE SERTOLI', '1105879', 'fernando.sertoli@sertao.ifrs.edu.br', 'FERNANDO', '', '', 'S', NULL, NULL, '2013-01-18 22:06:17', 13, NULL, NULL),
(468, 'Vaneisa Gobatto', '1618714', 'vaneisagobatto@sb.iffarroupilha.edu.br', 'Vaneisa', '', '', 'S', NULL, NULL, '2013-01-18 23:01:13', 2, NULL, NULL),
(469, 'FRANCISCA BRUM TOLIO', '1703241', 'francisca.tolio@ibiruba.ifrs.edu.br', 'FRANCISCA BRUM', '', '', 'S', NULL, NULL, '2013-01-19 00:08:25', 8, NULL, NULL),
(470, 'INGRID GONÇALVES CASEIRA', '1549948', 'ingrid.caseira@caxias.ifrs.edu.br', 'INGRID GONÇALVES', '', '', 'S', NULL, NULL, '2013-01-19 12:20:14', 4, NULL, NULL),
(471, 'Marla Heckler', '1843300', 'marla.heckler@caxias.ifrs.edu.br', 'Marla', '', '', 'S', NULL, NULL, '2013-01-19 13:22:39', 4, NULL, NULL),
(472, 'Patrícia Gabana Barbisan', '1972645', 'patricia.barbisan@bento.ifrs.edu.br', 'Patrícia Gabana Barbisan', '', '', 'S', NULL, NULL, '2013-01-20 23:01:03', 2, NULL, NULL),
(473, 'Silvia Maria Polito Ascari', '1940736', 'silvia.ascari@bento.ifrs.edu.br', 'Silvia', '', '', 'S', NULL, NULL, '2013-01-21 09:05:05', 2, NULL, NULL),
(474, 'Maria da Conceição Hatem de Souza', '1503830', 'maria.souza@osorio.ifrs.edu.br', 'Tina Hatem', '', '', 'S', NULL, NULL, '2013-01-21 10:52:44', 9, NULL, 1),
(475, 'Daniel de Carli', '1819037', 'daniel.carli@bento.ifrs.edu.br', 'Daniel de Carli', '', '', 'S', NULL, NULL, '2013-01-21 11:11:19', 2, NULL, NULL),
(476, 'Vivian Treichel Giesel', '018205836', 'vivian.giesel@feliz.ifrs.edu.br', 'Vivian Giesel', '', 'Não posso comer frutos do mar.', 'S', NULL, NULL, '2013-01-21 11:11:34', 7, NULL, NULL),
(477, 'Carlos Eduardo Avelleda', '2332243', 'carlos.avelleda@farroupilha.ifrs.edu.br', 'Avelleda', '', '', 'S', NULL, NULL, '2013-01-21 11:12:20', 6, NULL, NULL),
(478, 'Getulio Jorge Stefanello Júnior', '1611511', 'getulio.stefanello@ifrs.edu.br', 'Getulio', '', '', 'S', NULL, NULL, '2013-01-21 11:12:38', 1, NULL, NULL),
(479, 'Rodney da Silva Rosa', '1675675', 'rodney.rosa@sertao.ifrs.edu.br', 'Rodney', '', '', 'S', NULL, NULL, '2013-01-21 11:13:31', 13, NULL, NULL),
(480, 'Everton Pavan', '1647383', 'everton.pavan@sertao.ifrs.edu.br', 'Everton Pavan', '', '', 'S', NULL, NULL, '2013-01-21 11:14:10', 13, NULL, NULL),
(481, 'Luana Monique Delgado Lopes', '1698502', 'luana.lopes@osorio.ifrs.edu.br', 'Luana Lopes', '', '', 'S', NULL, NULL, '2013-01-21 11:14:16', 9, NULL, NULL),
(482, 'ALESSANDRA RUIZ TREVISOL', '1618641', 'alessandra.trevisol@riogrande.ifrs.edu.br', 'ALESSANDRA TREVISOL', '', '', 'S', NULL, NULL, '2013-01-21 11:15:17', 12, NULL, NULL),
(483, 'LUCIA DE MORAES BATISTA', '1378713', 'lucia.batista@bento.ifrs.edu.br', 'LUCIA M. BATISTA', '', '', 'S', NULL, NULL, '2013-01-21 11:18:02', 2, NULL, NULL),
(484, 'PAULO ROBERTO GARCIA DICKEL', '1828101', 'paulo.dickel@riogrande.ifrs.edu.br', 'PAULO DICKEL', '', '', 'S', NULL, NULL, '2013-01-21 11:18:21', 12, NULL, NULL),
(485, 'Fernando da Silva dos Reis', '1987248', 'fernando.reis@farroupilha.ifrs.edu.br', 'Fernando Reis', '', '', 'S', NULL, NULL, '2013-01-21 11:25:42', 6, NULL, NULL),
(486, 'CARLOS ALBERTO IMLAU', '53073', 'carlos.imlau@sertao.ifrs.edu.br', 'CARLOS', '', '', 'S', NULL, NULL, '2013-01-21 11:27:28', 13, NULL, NULL),
(487, 'Michelle Guimarães Salgueiro', '1876919', 'michelle.salgueiro@caxias.ifrs.edu.br', 'Michelle G. Salgueiro', '', '', 'S', NULL, NULL, '2013-01-21 11:29:13', 4, NULL, NULL),
(488, 'Márcia Amaral Corrêa de Moraes', '1768437', 'marcia.moraes@poa.ifrs.edu.br', 'Márcia A. C. de Moraes', '', '', 'S', NULL, NULL, '2013-01-21 11:35:06', 10, NULL, NULL),
(489, 'Tiago Martins da Silva Goulart', '1612175', 'tiago.goulart@bento.ifrs.edu.br', 'Tiago Goulart', '', '', 'S', NULL, NULL, '2013-01-21 11:43:40', 2, NULL, NULL),
(490, 'NEILA DE TOLEDO E TOLEDO', '1630346', 'neila.toledo@sertao.ifrs.edu.br', 'NEILA TOLEDO', '', '', 'S', NULL, NULL, '2013-01-21 11:48:11', 13, NULL, NULL),
(491, 'Jacira Casagrande', '1102290', 'jacira.casagrande@ifrs.edu.br', 'Jacira', '', '', 'S', NULL, NULL, '2013-01-21 11:51:58', 1, 3, NULL),
(492, 'SABRINA LETÍCIA COUTO DA SILVA', '017966477', 'sabrina.silva@poa.ifrs.edu.br', 'SABRINA LETÍCIA COUTO DA SILVA', '', 'Diabética, restrição com açúcar', 'S', NULL, NULL, '2013-01-21 11:54:05', 10, NULL, NULL),
(493, 'Regina da Silva Borba', '1602915', 'regina.borba@bento.ifrs.edu.br', 'Regina Borba', '', '', 'S', NULL, NULL, '2013-01-21 12:00:56', 2, NULL, NULL),
(494, 'Cristina Simões da Costa', '01459146', 'cristina.costa@poa.ifrs.edu.br', 'Cristina Simões da Costa', '', '', 'S', NULL, NULL, '2013-01-21 12:07:13', 10, NULL, NULL),
(495, 'DARLEI CECCONELLO', '1104557', 'darlei.cecconello@sertao.ifrs.edu.br', 'DARLEI', '', '', 'S', NULL, NULL, '2013-01-21 12:57:09', 13, NULL, NULL),
(496, 'Carlos Alberto Trevisan', '00049044', 'carlos.trevisan@bento.ifrs.edu.br', 'Carlos', '', '', 'S', NULL, NULL, '2013-01-21 13:09:09', 2, NULL, NULL),
(497, 'Greicimara Vogt Ferrari', '1797102', 'greicimara.ferrari@ifrs.edu.br', 'Greicimara Vogt Ferrari', '', '', 'S', NULL, NULL, '2013-01-21 13:16:05', 1, NULL, NULL),
(498, 'Andréa Bulloza Trigo Passos', '1828629', 'andrea.passos@riogrande.ifrs.edu.br', 'Andréa Passos', '', 'Colesterol', 'S', NULL, NULL, '2013-01-21 13:51:28', 12, NULL, NULL),
(499, 'LUCAS LANGNER', '1982591', 'lucas.langner@ifrs.edu.br', 'LUCAS LANGNER', '', '', 'S', NULL, NULL, '2013-01-21 13:56:50', 1, NULL, NULL),
(500, 'Marc Emerim', '1987217', 'marc.emerim@farroupilha.ifrs.edu.br', 'Marc Emerim', '', '', 'S', NULL, NULL, '2013-01-21 14:07:46', 6, NULL, NULL),
(501, 'ELIAS JOSE CAMARGO', '1982903', 'elias.camargo@sertao.ifrs.edu.br', 'ELIAS', '', '', 'S', NULL, NULL, '2013-01-21 14:20:36', 13, NULL, NULL),
(502, 'Luciana Pereira Bernd', '1806114', 'luciana.bernd@bento.ifrs.edu.br', 'Luciana Bernd', '', '', 'S', NULL, NULL, '2013-01-21 14:46:40', 2, NULL, NULL),
(503, 'Ricardo Augusto Manfredini', '1796362', 'ricardo.manfredini@farroupilha.ifrs.edu.br', 'Ricardo A. Manfredini', '', 'Sim, intolerância à lactose.', 'S', NULL, NULL, '2013-01-21 15:23:33', 6, NULL, NULL),
(504, 'paulo roberto sangoi', '2364475', 'SANGOI@POA.IFRS.EDU.BR', 'SANGOI', '', '', 'S', NULL, NULL, '2013-01-21 15:36:25', 10, NULL, NULL),
(505, 'Tiane Pacheco Lovatel', '018113656', 'tiane.lovatel@poa.ifrs.edu.br', 'Tiane Lovatel', '', '', 'S', NULL, NULL, '2013-01-21 16:26:11', 10, NULL, NULL),
(506, 'Alini Gomes Ferreira', '1829395', 'alini.ferreira@restinga.ifrs.edu.br', 'Alini Ferreira', '', '', 'S', NULL, NULL, '2013-01-21 19:36:00', 11, NULL, NULL),
(507, 'Nara Regina Atz', '1348405', 'nara.atz@poa.ifrs.edu.br', 'Nara ATZ', '-----', '------', 'S', NULL, NULL, '2013-01-21 19:58:13', 10, NULL, NULL),
(508, 'Eliane Velasco Simões Portes', '01606094', 'eliane.velasco@canoas.ifrs.edu.br', 'Eliane Velasco', '', 'Sim, intolerância a lactose', 'S', NULL, NULL, '2013-01-21 20:04:45', 3, NULL, NULL),
(509, 'Sílvia Regina Grando', '1612437', 'silvia.grando@feliz.ifrs.edu.br', 'Sílvia Grando', '', '', 'S', NULL, NULL, '2013-01-22 09:09:57', 7, NULL, NULL),
(510, 'MARCELO LIMA CALIXTO', '1847670', 'marcelo.calixto@ibiruba.ifrs.edu.br', 'CALIXTO', '', '', 'S', NULL, NULL, '2013-01-22 09:29:31', 8, NULL, NULL),
(511, 'Daner Silva Martins', '1330439', 'daner.martins@riogrande.ifrs.edu.br', 'Daner', '', '', 'S', NULL, NULL, '2013-01-22 09:41:08', 12, NULL, NULL),
(512, 'Angelita Freitas da Silva', '018043895', 'angelita.silva@erechim.ifrs.edu.br', 'Angelita', '', '', 'S', NULL, NULL, '2013-01-22 09:48:49', 5, NULL, NULL),
(513, 'EDUARDO ANGONESI PREDEBON', '1737277', 'eduardo.predebon@erechim.ifrs.edu.br', 'EDUARDO A. PREDEBON', '', '', 'S', NULL, NULL, '2013-01-22 09:48:59', 5, NULL, NULL),
(514, 'Luciana Maria Bernstein Pavan', '1916331', 'luciana.pavan@erechim.ifrs.edu.br', 'Luciana Maria Bernstein Pavan', '', '', 'S', NULL, NULL, '2013-01-22 09:53:12', 5, NULL, NULL),
(515, 'Carla do Couto Nunes', '1988036', 'carla.nunes@feliz.ifrs.edu.br', 'Carla', '', '', 'S', NULL, NULL, '2013-01-22 11:21:02', 7, NULL, NULL),
(516, 'Simone Bertazzo Rossato', '015764974', 'simone.rossato@bento.ifrs.edu.br', 'Simone B. Rossato', '', 'Sim... Intolerância à lactose', 'S', NULL, NULL, '2013-01-22 13:56:32', 2, NULL, NULL),
(517, 'TÂNIA SALETE BIANCHI CARVALHO', '1102345', 'tania.carvalho@ifrs.edu.br', 'TÂNIA', '', '', 'S', NULL, NULL, '2013-01-22 17:22:43', 1, NULL, NULL),
(518, 'Maurício Covolan Rosito', '1669378', 'mauricio.rosito@bento.ifrs.edu.br', 'Maurício Rosito', '', '', 'S', NULL, NULL, '2013-01-22 17:30:50', 2, NULL, NULL),
(519, 'neudy alexandro demichei', '1756949', 'neudy.demichei@restinga.ifrs.edu.br', 'Alex', '', '', 'S', NULL, NULL, '2013-01-22 18:13:05', 11, 3, NULL),
(520, 'André Zimmer', '1580090', 'andre.zimmer@feliz.ifrs.edu.br', 'André Zimmer', '', '', 'S', NULL, NULL, '2013-01-22 18:57:03', 7, NULL, NULL),
(521, 'Giácomo Gai Soares', '2814410', 'giacomo.soares@ibiruba.ifrs.edu.br', 'Giácomo', '', '', 'S', NULL, NULL, '2013-01-22 20:07:10', 8, NULL, NULL),
(522, 'NUREIVE GOULARTE BISSACO', '1908664', 'nureive.bissaco@caxias.ifrs.edu.br', 'NUREIVE BISSACO', '', '', 'S', NULL, NULL, '2013-01-22 21:17:17', 4, NULL, NULL),
(523, 'Sandro Itamar Bueno dos Santos', '1102323', 'sandro.santos@osorio.ifrs.edu.br', 'Sandro Itamar', '', '', 'S', NULL, NULL, '2013-01-22 21:53:32', 9, NULL, NULL),
(524, 'Márcia Gallina', '1635276', 'marcia.gallina@osorio.ifrs.edu.br', 'Márcia', '', '', 'S', NULL, NULL, '2013-01-22 21:56:39', 9, NULL, NULL),
(525, 'Vilmar Rudinei Ulrich', '1104555', 'vilmar.ulrich@sertao.ifrs.edu.br', 'Vilmar', '', '', 'S', NULL, NULL, '2013-01-23 00:04:59', 13, NULL, NULL),
(526, 'Roberto Valmorbida de Aguiar', '8000773', 'roberto.aguiar@sertao.ifrs.edu.br', 'Roberto V. de Aguiar', '', '', 'S', NULL, NULL, '2013-01-23 01:41:12', 13, NULL, NULL),
(527, 'Patrícia Rodrigues da Rosa', '1610877', 'patricia.rosa@canoas.ifrs.edu.br', 'Patrícia Rosa', '', '', 'S', NULL, NULL, '2013-01-23 02:37:23', 3, NULL, NULL),
(528, 'Daniel Souza Cardoso', '1579883', 'daniel.cardoso@riogrande.ifrs.edu.br', 'Daniel Cardoso', '', '', 'S', NULL, NULL, '2013-01-23 09:29:09', 12, NULL, NULL),
(529, 'Tanandra Bernieri', '1880718', 'tanandra.bernieri@caxias.ifrs.edu.br', 'Tanandra', '', '', 'S', NULL, NULL, '2013-01-23 10:48:23', 4, NULL, NULL),
(530, 'Mario Wolfart Júnior', '1808612', 'mario.wolfart@riogrande.ifrs.edu.br', 'Mario Júnior', '', '', 'S', NULL, NULL, '2013-01-23 11:45:09', 12, NULL, NULL),
(531, 'Carolina López Israel', '1698654', 'carolina.israel@feliz.ifrs.edu.br', 'Carolina López Israel', '', '', 'S', NULL, NULL, '2013-01-23 13:51:06', 12, NULL, NULL),
(532, 'Icaro Bittencourt', '1924862', 'icaro.bittencourt@farroupilha.ifrs.edu.br', 'Icaro Bittencourt', '', '', 'S', NULL, NULL, '2013-01-23 14:35:49', 6, NULL, NULL),
(533, 'Cristiano Escobar Carvalho Bernardes', '1988689', 'cristiano.bernardes@restinga.ifrs.edu.br', 'Cristiano Bernardes', '', '', 'S', NULL, NULL, '2013-01-23 15:13:56', 11, NULL, NULL),
(534, 'Valdinei Marcolla', '1491378', 'valdinei.marcolla@caxias.ifrs.edu.br', 'Marcolla', '', '', 'S', NULL, NULL, '2013-01-23 16:35:03', 4, NULL, NULL),
(535, 'Taís Letícia Bernardi', '1869153', 'tais.bernardi@sertao.ifrs.edu.br', 'Taís Letícia', '', '', 'S', NULL, NULL, '2013-01-23 16:53:47', 13, 3, NULL),
(536, 'Luciano Gomes Furlan', '015364143', 'luciano.furlan@restinga.ifrs.edu.br', 'Luciano Furlan', '', '', 'S', NULL, NULL, '2013-01-24 08:41:22', 11, NULL, NULL),
(537, 'Luana Maris Moravski Dapper', '1982592', 'luana.dapper@ibiruba.ifrs.edu.br', 'Luana', '', '', 'S', NULL, NULL, '2013-01-24 08:42:09', 8, NULL, NULL),
(538, 'Alexandre Jesus da Silva Machado', '4089693', 'alexandre.machado@riogrande.ifrs.edu.br', 'Alexandre Machado', '', '', 'S', NULL, NULL, '2013-01-24 08:47:11', 12, 3, NULL),
(539, 'Camila Duarte Teles', '1774408', 'camila.teles@bento.ifrs.edu.br', 'Camila Duarte Teles', '', '', 'S', NULL, NULL, '2013-01-24 08:47:55', 2, NULL, NULL),
(540, 'DÁRIO LISSANDRO BEUTLER', '017371422', 'dario.beutler@erechim.ifrs.edu.br', 'DÁRIO L. BEUTLER', '', '', 'S', NULL, NULL, '2013-01-24 08:55:10', 5, NULL, NULL),
(541, 'BERNHARD SYDOW', '003565548', 'bernhard.sydow@poa.ifrs.edu.br', 'BERNHARD', 'Visual Miopia -5,0', '', 'S', NULL, NULL, '2013-01-24 09:32:14', 10, 1, 2),
(542, 'Aline Ferraz da Silva', '1840396', 'aline.ferraz@poa.ifrs.edu.br', 'Aline Ferraz', '', 'Embutidos', 'S', NULL, NULL, '2013-01-24 09:40:44', 10, NULL, NULL),
(543, 'Thaís Helena da Silveira', '1982923', 'thais.silveira@feliz.ifrs.edu.br', 'Thaís', '', '', 'S', NULL, NULL, '2013-01-24 09:44:31', 7, NULL, NULL),
(544, 'Gleison Samuel do Nascimento', '1669375', 'gleison.nascimento@restinga.ifrs.edu.br', 'Gleison Nascimento', '', '', 'S', NULL, NULL, '2013-01-24 09:45:48', 11, NULL, NULL),
(545, 'Lúcio Olímpio de Carvalho Vieira', '1273549', 'lucio.vieira@poa.ifrs.edu.br', 'Lúcio Vieira', '', '', 'S', NULL, NULL, '2013-01-24 09:55:56', 10, 1, NULL),
(546, 'Natalia Labella de Sánchez', '1609218', 'natalia.sanchez@poa.ifrs.edu.br', 'Natalia Labella', '', '', 'S', NULL, NULL, '2013-01-24 10:00:02', 10, NULL, NULL),
(547, 'NELI TERESINHA FERREIRA MACHADO', '1681247', 'neli.machado@poa.ifrs.edu.br', 'NELI MACHADO', '', '', 'S', NULL, NULL, '2013-01-24 10:07:04', 10, NULL, NULL),
(548, 'TÂNIA JUREMA FLORES DA ROSA AIUB', '01669160', 'tania.aiub@ifrs.edu.br', 'TÂNIA AIUB', '', '', 'S', NULL, NULL, '2013-01-24 10:25:23', 1, 3, NULL),
(549, 'Matheus Milani', '1902722', 'matheus.milani@feliz.ifrs.edu.br', 'Matheus', '', '', 'S', NULL, NULL, '2013-01-24 10:47:33', 7, NULL, NULL),
(550, 'Raquel Andrade Ferreira', '1973603', 'raquel.ferreira@sertao.ifrs.edu.br', 'Raquel ferreira', '', '', 'S', NULL, NULL, '2013-01-24 10:48:10', 13, NULL, 1),
(551, 'Elaine Pires Salomão Carbonera', '1811469', 'elaine.carbonera@restinga.ifrs.edu.br', 'Elaine P. S. Carbonera', '', 'Sim. Intolerânica à lactose.', 'S', NULL, NULL, '2013-01-24 11:03:11', 11, NULL, NULL),
(552, 'Ricardo Toledo de Carvalho', '1758784', 'ricardo.carvalho@sertao.ifrs.edu.br', 'Ricardo Carvalho', '', '', 'S', NULL, NULL, '2013-01-24 11:06:58', 13, NULL, NULL),
(553, 'Sandro Neves Soares', '018692664', 'sandro.soares@bento.ifrs.edu.br', 'Sandro Neves', '', '', 'S', NULL, NULL, '2013-01-24 11:14:03', 2, 3, NULL),
(554, 'Cláudia Silva Estima', '017966353', 'claudia.estima@poa.ifrs.edu.br', 'Cláudia Estima', '', '', 'S', NULL, NULL, '2013-01-24 11:16:28', 10, NULL, NULL),
(555, 'RENATA TRINDADE SEVERO', '1796053', 'renata.severo@poa.ifrs.edu.br', 'RENATA T. SEVERO', '', '', 'S', NULL, NULL, '2013-01-24 11:22:27', 10, NULL, NULL),
(556, 'André Luiz Marcondes', '1982908', 'andre.marcondes@ibiruba.ifrs.edu.br', 'André Luiz Marcondes', '', '', 'S', NULL, NULL, '2013-01-24 11:26:26', 8, NULL, NULL),
(557, 'André Marek', '1982915', 'andre.marek@ibiruba.ifrs.edu.br', 'André Marek', '', '', 'S', NULL, NULL, '2013-01-24 11:34:12', 8, NULL, NULL),
(558, 'LUCIANO APARECIDO KEMPSKI', '1071053', 'luciano.kempski@erechim.ifrs.edu.br', 'LUCIANO', '', '', 'S', NULL, NULL, '2013-01-24 12:29:34', 5, NULL, NULL),
(559, 'Cimara Valim de Melo', '1796019', 'cimara.melo@canoas.ifrs.edu.br', 'Cimara Valim', '', '', 'S', NULL, NULL, '2013-01-24 12:48:04', 3, NULL, NULL),
(560, 'Guilherme Brandt de Oliveira', '1822731', 'guilherme.brandt@restinga.ifrs.edu.br', 'Guilherme Brandt', '', '', 'S', NULL, NULL, '2013-01-24 12:52:36', 11, NULL, NULL),
(561, 'Elói Da Rosa Soares', '1963040', 'eloi.soares@sertao.ifrs.edu.br', 'Elói', '', '', 'S', NULL, NULL, '2013-01-24 13:19:00', 13, NULL, NULL),
(562, 'Gabriela Silva Morél de Oliveira', '1819203', 'gabriela.morel@osorio.ifrs.edu.br', 'Gabriela Morél', '', '', 'S', NULL, NULL, '2013-01-24 14:02:08', 9, 3, NULL),
(563, 'Felipe Luy Valério', '1504977', 'felipe.valerio@bento.ifrs.edu.br', 'Felipe', '', '', 'S', NULL, NULL, '2013-01-24 14:14:21', 2, NULL, NULL),
(564, 'Gisele Palma', '1889424', 'gisele.palma@bento.ifrs.edu.br', 'Gisele Palma', '', '', 'S', NULL, NULL, '2013-01-24 14:25:09', 2, NULL, NULL),
(565, 'Fábio Azambuja Marçal', '16101936', 'fabio.marcal@restinga.ifrs.edu.br', 'Fábio Marçal', '', 'nãp', 'S', NULL, NULL, '2013-01-24 14:58:39', 11, NULL, NULL),
(566, 'Pauline Fagundes Rosales', '1991463', 'pauline.rosales@bento.ifrs.edu.br', 'Pauline Rosales', '', '', 'S', NULL, NULL, '2013-01-24 15:27:25', 2, NULL, NULL),
(567, 'Fernanda Medeiros de Albuquerque', '1796134', 'Fernanda.albuquerque@osorio.ifrs.edu.br', 'Fernanda Albuquerque', '', '', 'S', NULL, NULL, '2013-01-24 17:06:57', 9, NULL, NULL),
(568, 'Eduardo Marczwski da Silva', '1817833', 'Eduardo.silva@osorio.ifrs.edu.br', 'Eduardo Silva', '', '', 'S', NULL, NULL, '2013-01-24 17:08:32', 9, NULL, NULL),
(569, 'Maria Inês Varela Paim', '1808301', 'ines.paim@erechim.ifrs.edu.br', 'Inês', '', 'Alergia a camarão, frutos do mar.', 'S', NULL, NULL, '2013-01-24 17:36:26', 5, NULL, NULL),
(570, 'Douglas Neves Ricalde', '1760980', 'douglas.ricalde@poa.ifrs.edu.br', 'Douglas Neves', '', '', 'S', NULL, NULL, '2013-01-24 17:59:46', 10, NULL, NULL),
(571, 'Cláudio da Silva Goebel', '1586995', 'claudio.goebel@ifrs.edu.br', 'Cláudio', '', '', 'S', NULL, NULL, '2013-01-24 19:02:33', 1, NULL, NULL),
(572, 'Aneti Fernanda Ritzel', '012184829', 'aneti.ritzel@bento.ifrs.edu.br', 'Aneti', '', '', 'S', NULL, NULL, '2013-01-25 10:13:43', 2, NULL, NULL),
(573, 'Maria Helena Schneid Vasconcelos', '019324235', 'maria.vasconcelos@bento.ifrs.edu.br', 'Maria Helena S.Vasconcelos', '', '', 'S', NULL, NULL, '2013-01-25 10:14:51', 3, 3, NULL),
(574, 'Lucas Coradini', '1564962', 'lucas.coradini@farroupilha.ifrs.edu.br', 'Lucas Coradini', '', '', 'S', NULL, NULL, '2013-01-25 10:23:25', 6, NULL, NULL),
(575, 'Suzana França de Oliveira', '1928320', 'suzana.franca@sertao.ifrs.edu.br', 'Suzana F.de Oliveira', '', '', 'S', NULL, NULL, '2013-01-25 10:43:22', 13, NULL, NULL),
(576, 'Elisa Iop', '1805998', 'elisa.iop@sertao.ifrs.edu.br', 'Elisa Iop', '', '', 'S', NULL, NULL, '2013-01-25 11:02:37', 13, NULL, 1),
(577, 'Fabiane Eloisa Morandini Miotto', '2890400', 'fabiane.miotto@sertao.ifrs.edu.br', 'Fabiane', '', '', 'S', NULL, NULL, '2013-01-25 11:03:08', 13, NULL, NULL),
(578, 'Daniel de Moura', '1868189', 'daniel.moura@bento.ifrs.edu.br', 'Daniel de Moura', '', '', 'S', NULL, NULL, '2013-01-25 11:18:34', 2, NULL, NULL),
(579, 'Rafael campos Vieira', '018897487', 'rafael.vieira@feliz.ifrs.edu.br', 'Rafael Vieira', '', '', 'S', NULL, NULL, '2013-01-25 11:21:10', 7, NULL, NULL),
(580, 'Deise Puhl', '2942925', 'deise.puhl@feliz.ifrs.edu.br', 'Deise Puhl', '', '', 'S', NULL, NULL, '2013-01-25 11:47:16', 7, NULL, NULL),
(581, 'Mariano Nicolao', '1796086', 'mariano.nicolao@canoas.ifrs.edu.br', 'Mariano', '', '', 'S', NULL, NULL, '2013-01-25 11:54:20', 3, NULL, NULL),
(582, 'CINARA FONTANA TRICHES', '018204368', 'cinara.triches@farroupilha.ifrs.edu.br', 'CINARA FONTANA TRICHES', '', 'Intolerância à lactose', 'S', NULL, NULL, '2013-01-25 12:08:37', 6, NULL, NULL),
(583, 'Luciane Calabria', '018853366', 'luciane.calabria@farroupilha.ifrs.edu.br', 'Luciane Calabria', '', '', 'S', NULL, NULL, '2013-01-25 12:15:23', 6, NULL, NULL),
(584, 'VANDERLEI AUGUSTO SEGAT', '1699532', 'vanderlei.segat@ifrs.edu.br', 'VANDERLEI', '', '', 'S', NULL, NULL, '2013-01-25 12:18:15', 1, NULL, NULL),
(585, 'FABIO LUIZ DA COSTA CARRIR', '1761764', 'fabio.carrir@riogrande.ifrs.edu.br', 'FABIO CARRIR', '', '', 'S', NULL, NULL, '2013-01-25 12:23:37', 12, NULL, NULL),
(586, 'Eduardo Marques de Camargo', '1870308', 'eduardo.camargo@ibiruba.ifrs.edu.br', 'Eduardo Camargo', '', '', 'S', NULL, NULL, '2013-01-25 13:01:39', 8, NULL, NULL),
(587, 'Carolina dos Santos Binda', '1808283', 'carolina.binda@sertao.ifrs.edu.br', 'Carolina Binda', '', '', 'S', NULL, NULL, '2013-01-25 13:17:14', 13, NULL, NULL),
(588, 'Leda araujo alves', '019840020', 'leda.alves@poa.ifrs.edu.br', 'Leda Araújo', '', '', 'S', NULL, NULL, '2013-01-25 13:52:45', 10, NULL, NULL),
(589, 'Vinícius Lima Lousada', '1373769', 'vinicius.lousada@ifrs.edu.br', 'Vinícius', '', '', 'S', NULL, NULL, '2013-01-25 14:44:04', 1, NULL, 4),
(590, 'Iara Elisabeth Schneider', '1645829', 'iara.schneider@poa.ifrs.edu.br', 'Iara Schneider', '', '', 'S', NULL, NULL, '2013-01-25 14:54:40', 10, NULL, NULL),
(591, 'Sandro Luís Moresco Martins', '1923837', 'sandro.martins@erechim.ifrs.edu.br', 'Sandro Martins', '', '', 'S', NULL, NULL, '2013-01-25 15:08:06', 5, NULL, NULL),
(592, 'ELISANDRA MOTTIN FRESCHI', '1950691', 'elisandra.freschi@erechim.ifrs.edu.br', 'ELISANDRA MOTTIN FRESCHI', '', '', 'S', NULL, NULL, '2013-01-25 15:15:32', 5, NULL, NULL),
(593, 'Nicolai Duarte Arrieta', '1982594', 'nicolai.arrieta@ifrs.edu.br', 'Nicolai Arrieta', '', '', 'S', NULL, NULL, '2013-01-25 15:32:04', 1, NULL, NULL),
(594, 'ANDRE LUIZ BEDENDO', '1935191', 'andre.bedendo@erechim.ifrs.edu.br', 'ANDRE BEDENDO', '', '', 'S', NULL, NULL, '2013-01-25 15:38:49', 5, NULL, NULL),
(595, 'HELEN GULARTE CABRAL', '1681460', 'helen.cabral@riogrande.ifrs.edu.br', 'HELEN CABRAL', '', '', 'S', NULL, NULL, '2013-01-25 15:44:42', 12, NULL, NULL),
(596, 'marisa dutra paz', '003564551', 'marisa.paz@poa.ifrs.edu.br', 'marisa', '', '', 'S', NULL, NULL, '2013-01-25 16:01:36', 10, NULL, NULL),
(597, 'Gibran Fernando Ibrahim', '019829000', 'gibran.ibrahim@bento.ifrs.edu.br', 'Gibran Ibrahim', '', '', 'S', NULL, NULL, '2013-01-25 16:14:13', 2, 3, NULL),
(598, 'Cássio Silva Moreira', '1344052', 'cassio.moreira@poa.ifrs.edu.br', 'Cássio Moreira', '', '', 'S', NULL, NULL, '2013-01-25 16:14:55', 10, NULL, 4),
(599, 'Marleide Canizares', '12189456', 'marleide.canizares@bento.ifrs.edu.br', 'Marleide Canizares', '', '', 'S', NULL, NULL, '2013-01-25 16:21:53', 2, NULL, NULL),
(600, 'Leonardo Alvarenga Pereira', '1798998', 'leonardo.pereira@bento.ifrs.edu.br', 'Leonardo', '', '', 'S', NULL, NULL, '2013-01-25 16:58:22', 2, NULL, NULL),
(601, 'Ubiratã Escobar Nunes', '1798666', 'ubirata.nunes@bento.ifrs.edu.br', 'Ubiratã', '', '', 'S', NULL, NULL, '2013-01-25 16:59:36', 2, NULL, NULL),
(602, 'Mauro Maisonave de Melo', '018273351', 'mauro.melo@caxias.ifrs.edu.br', 'Mauro Melo', '', '', 'S', NULL, NULL, '2013-01-25 17:16:29', 4, NULL, 2),
(603, 'Edimar Manica', '1804782', 'edimar.manica@ibiruba.ifrs.edu.br', 'Edimar Manica', '', '', 'S', NULL, NULL, '2013-01-25 17:18:43', 8, NULL, NULL),
(604, 'Katielle de Moraes Bilhan', '01924654', 'katielle.bilhan@bento.ifrs.edu.br', 'Katielle Bilhan', '', '', 'S', NULL, NULL, '2013-01-25 17:25:01', 2, NULL, NULL),
(605, 'Tânia Regina Pereira Chaves', '01936375', 'tania.chaves@feliz.ifrs.edu.br', 'Tânia Chaves', '', '', 'N', NULL, NULL, '2013-01-25 17:26:17', 7, NULL, NULL),
(606, 'Enildo de Matos de Oliveira', '1578426', 'enildo.oliveira@erechim.ifrs.edu.br', 'Enildo', '', '', 'S', NULL, NULL, '2013-01-25 18:54:56', 5, NULL, NULL),
(607, 'Marcelo Pizzutti', '1982631', 'marcelo.pizzutti@ifrs.edu.br', 'Marcelo', '', '', 'S', NULL, NULL, '2013-01-25 19:07:21', 1, NULL, NULL),
(608, 'Giandra Volpato', '1829223', 'giandra.volpato@poa.ifrs.edu.br', 'Giandra', '', '', 'S', NULL, NULL, '2013-01-25 19:14:04', 10, NULL, NULL),
(609, 'Sabrina Hax Duro Rosa', '1577995', 'sabrina.rosa@riogrande.ifrs.edu.br', 'Sabrina Rosa', '', '', 'S', NULL, NULL, '2013-01-25 19:24:58', 12, 2, NULL),
(610, 'Daniel Capella Zanotta', '01846459', 'daniel.zanotta@riogrande.ifrs.edu.br', 'Daniel Zanotta', '', '', 'S', NULL, NULL, '2013-01-25 19:52:53', 12, NULL, NULL),
(611, 'Marcos Daniel Schmidt de Aguiar', '1808553', 'marcos.aguiar@osorio.ifrs.edu.br', 'Marcos Aguiar', '', '', 'S', NULL, NULL, '2013-01-25 20:04:30', 9, NULL, NULL),
(612, 'Andre Bisognin', '1963055', 'andre.bisognin@sertao.ifrs.edu.br', 'Bisognin', '', '', 'S', NULL, NULL, '2013-01-25 20:04:50', 13, NULL, NULL),
(613, 'Erildo Dorico', '1321589', 'erildo.dorico@caxias.ifrs.edu.br', 'Dorico', '', '', 'S', NULL, NULL, '2013-01-25 20:22:17', 4, NULL, NULL),
(614, 'JANDIRA ALMEIDA DE OLIVEIRA', '019023863', 'jandira.oliveira@farroupilha.ifrs.edu.br', 'JANDIRA', '', '', 'S', NULL, NULL, '2013-01-25 20:26:00', 6, NULL, NULL),
(615, 'Laura Gotleib da Rosa', '1680548', 'laura.rosa@ibiruba.ifrs.edu.br', 'Laura Gotleib', '', 'Intolerancia a lactose', 'S', NULL, NULL, '2013-01-25 21:02:40', 8, NULL, NULL),
(616, 'Larissa Dias de Ávila', '1229315', 'larissa.avila@bento.ifrs.edu.br', 'Larissa', '', '', 'S', NULL, NULL, '2013-01-25 22:13:20', 2, NULL, NULL),
(617, 'Cláudio Kuczkowski', '1583403', 'claudio.kuczkowski@sertao.ifrs.edu.br', 'Kuczkowski', '', '', 'S', NULL, NULL, '2013-01-25 22:35:46', 13, NULL, NULL),
(618, 'bolivar de jesus dias urruth', '388171', 'bolivarurruth@gmail.com', 'bolivar', '', '', 'S', NULL, NULL, '2013-01-25 23:00:37', 12, 3, NULL),
(619, 'Luis Fernando Oliveira Lopes', '018638521', 'luis.lopes@riogrande.ifrs.edu.br', 'Luis Fernando Lopes', '', '', 'S', NULL, NULL, '2013-01-26 00:05:01', 12, NULL, NULL),
(620, 'CAROLINE DO AMARAL FRIGGI', '019347111', 'caroline.friggi@bento.ifrs.edu.br', 'CAROLINE FRIGGI', '', '', 'S', NULL, NULL, '2013-01-26 08:10:58', 2, NULL, NULL);
INSERT INTO `participantes` (`id`, `nome_completo`, `siape`, `email`, `nome_cracha`, `necessidade_especial`, `restricao_alimentar`, `confirmado`, `participacao`, `presente`, `data`, `campus_id`, `atividade_id`, `apresentacao_id`) VALUES
(621, 'Artur da Silva Rossetto', '1737076', 'artur.rossetto@erechim.ifrs.edu.br', 'Artur Rossetto', '', '', 'S', NULL, NULL, '2013-01-26 13:27:16', 5, NULL, NULL),
(622, 'Manuela Rosing Agostini', '1935712', 'manuela.agostini@sertao.ifrs.edu.br', 'Manuela R. Agostini', '', '', 'S', NULL, NULL, '2013-01-26 14:49:07', 13, NULL, NULL),
(623, 'Fernanda Arboite de Oliveira', '1827130', 'fernanda.oliveira@osorio.ifrs.edu.br', 'Fernanda Arboite', '', '', 'S', NULL, NULL, '2013-01-26 17:32:58', 9, NULL, NULL),
(624, 'Patrícia Nogueira Hübler', '1796126', 'patricia.hubler@canoas.ifrs.edu.br', 'Patrícia Hübler', '', '', 'S', NULL, NULL, '2013-01-26 22:43:13', 3, NULL, NULL),
(625, 'Fernanda  Krüger Garcia', '2727513', 'fernanda.garcia@poa.ifrs.edu.br', 'Fernanda  Krüger Garcia', '', '', 'S', NULL, NULL, '2013-01-27 08:28:21', 10, NULL, 2),
(626, 'Javier García López', '1274777', 'javier.garcia@riogrande.ifrs.edu.br', 'Javier', '', '', 'S', NULL, NULL, '2013-01-27 11:31:42', 12, NULL, NULL),
(627, 'Sérgio Wesner Viana', '2759927', 'sergio.viana@erechim.ifrs.edu.br', 'Sérgio Viana', '', '', 'S', NULL, NULL, '2013-01-27 18:02:09', 10, NULL, NULL),
(628, 'FABIO YOSHIMITSU OKUYAMA', '1796638', 'FABIO.OKUYAMA@POA.IFRS.EDU.BR', 'FABIO OKUYAMA', '', '', 'S', NULL, NULL, '2013-01-27 19:08:42', 10, NULL, NULL),
(629, 'Cristiane Silva da Silva', '1336289', 'cristiane.silva@canoas.ifrs.edu.br', 'Cristiane Silva', '', '', 'S', NULL, NULL, '2013-01-27 22:09:02', 3, NULL, NULL),
(630, 'Fernando Covolan Rosito', '1867800', 'fernando.rosito@riogrande.ifrs.edu.br', 'Fernando', '', '', 'S', NULL, NULL, '2013-01-27 23:06:59', 12, NULL, NULL),
(631, 'Marcelo Augusto Rauh Schmitt', '2245519', 'marcelo.schmitt@poa.ifrs.edu.br', 'Marcelo Schmitt', '', '', 'S', NULL, NULL, '2013-01-27 23:34:31', 10, NULL, NULL),
(632, 'Claudio Henrique Kray', '1518167', 'claudio.kray@osorio.ifrs.edu.br', 'Claudio Kray', '', '', 'S', NULL, NULL, '2013-01-27 23:37:38', 9, NULL, NULL),
(633, 'Rubilar Tomasi', '019435241', 'rubilar.tomasi@sertao.ifrs.edu.br', 'Rubilar', '', '', 'S', NULL, NULL, '2013-01-28 00:51:36', 13, NULL, NULL),
(634, 'Nilso Ricardo Krauzer da Rosa', '015124878', 'nilso.rosa@canoas.ifrs.edu.br', 'Nilso Krauzer', '', '', 'S', NULL, NULL, '2013-01-28 09:04:58', 3, 3, NULL),
(635, 'Amilton de Moura Figueiredo', '1495949', 'amilton.figueiredo@ifrs.edu.br', 'Amilton Figueiredo', '', '', 'S', NULL, NULL, '2013-01-28 09:05:31', 1, NULL, NULL),
(636, 'Josiane Carolina Soares Ramos do Amaral', ' 1669385', 'josiane.amaral@poa.ifrs.edu.br', 'Josiane do Amaral', '', '', 'S', NULL, NULL, '2013-01-28 09:07:05', 10, NULL, NULL),
(637, 'CLEIVA  AGUIAR  DE LIMA', '1093668', 'cleiva.lima@riogrande.ifrs.edu.br', 'CLEIVA  LIMA', '', '', 'S', NULL, NULL, '2013-01-28 09:51:36', 12, NULL, NULL),
(638, 'Mônica Giacomini', '1827598', 'monica.giacomini@ibiruba.ifrs.edu.br', 'Mônica', '', '', 'S', NULL, NULL, '2013-01-28 11:47:17', 8, NULL, NULL),
(639, 'Juliano Elesbão Rathke', '1759468', 'juliano.rathke@ibiruba.ifrs.edu.br', 'Juliano Rathke', '', '', 'S', NULL, NULL, '2013-01-28 11:50:22', 8, NULL, NULL),
(640, 'Cláudia Schreiner', '015000931', 'claudia.schreiner@poa.ifrs.edu.br', 'Cláudia Schreiner', '', '', 'S', NULL, NULL, '2013-01-28 12:11:48', 10, NULL, 2),
(641, 'Elisângela Dagostini Beux', '018051464', 'elisangela.beux@canoas.ifrs.edu.br', 'Elisângela Beux', '', '', 'S', NULL, NULL, '2013-01-28 13:09:22', 3, NULL, NULL),
(642, 'Norton Pizzi Manassi', '019248369', 'norton.manassi@farroupilha.ifrs.edu.br', 'Norton Pizzi Manassi', '', '', 'N', NULL, NULL, '2013-01-28 13:54:11', 6, NULL, NULL),
(643, 'André Luiz Silva de Andrades', '1982898', 'andre.andrades@restinga.ifrs.edu.br', 'André Andrades', '', '', 'S', NULL, NULL, '2013-01-28 13:54:24', 11, NULL, NULL),
(644, 'Norton Pizzi Manassi', '1924836', 'norton.manassi@farroupilha.ifrs.edu.br', 'Norton Pizzi Manassi', '', '', 'S', NULL, NULL, '2013-01-28 13:57:45', 6, NULL, NULL),
(645, 'Lívia de Castro Côrtes', '1829926', 'livia.cortes@poa.ifrs.edu.br', 'Lívia Côrtes', '', '', 'S', NULL, NULL, '2013-01-28 14:51:44', 10, NULL, NULL),
(646, 'Renato Avellar de Albuquerque', '1990519', 'renato.albuquerque@poa.ifrs.edu.br', 'Renato Albuquerque', '', '', 'S', NULL, NULL, '2013-01-28 15:40:35', 10, NULL, NULL),
(647, 'Camila Lombard Pedrazza', '1680847', 'camila.pedrazza@poa.ifrs.edu.br', 'Camila Pedrazza', '', '', 'S', NULL, NULL, '2013-01-28 16:00:12', 10, NULL, NULL),
(648, 'Carolina Moraes dos Reis', '016808622', 'carolina.reis@poa.ifrs.edu.br', 'Carol Reis', '', '', 'S', NULL, NULL, '2013-01-28 16:45:10', 10, 3, NULL),
(649, 'Lícia Carla Lima da Silva', '1761393', 'licia.silva@bento.ifrs.edu.br', 'Lícia Lima da Silva', '', '', 'S', NULL, NULL, '2013-01-28 16:52:45', 2, NULL, NULL),
(650, 'Bryan Aislan Zinn', '1992053', 'bryan.zinn@ifrs.edu.br', 'Bryan', '', '', 'S', NULL, NULL, '2013-01-28 17:17:08', 1, NULL, NULL),
(651, 'Bruno Chagas Alves Fernandes', '1993557', 'bruno.fernandes@ifrs.edu.br', 'Bruno Fernandes', '', '', 'S', NULL, NULL, '2013-01-28 17:20:33', 1, NULL, NULL),
(652, 'Luis Henrique Gularte Ferreira', '017704278', 'luis.ferreira@bento.ifrs.edu.br', 'Luis', '', '', 'S', NULL, NULL, '2013-01-28 17:25:50', 2, NULL, NULL),
(653, 'João Luís Komosinski', '1840411', 'joao.komosinski@caxias.ifrs.edu.br', 'Goy', '', 'Ovo-lacto-vegetariano', 'S', NULL, NULL, '2013-01-28 17:33:30', 4, NULL, NULL),
(654, 'Simone Weide Luiz', '1925035', 'simone.luiz@farroupilha.ifrs.edu.br', 'Simone', '', '', 'S', NULL, NULL, '2013-01-28 19:09:32', 6, 3, NULL),
(655, 'Viviani Rios Kwecko', '1526447', 'viviani.kwecko@riogrande.ifrs.edu.br', 'Viviani Kwecko', '', '', 'S', NULL, NULL, '2013-01-28 20:26:30', 12, NULL, NULL),
(656, 'Suelena de Araujo Borges', '018229964', 'suelena.borges@poa.ifrs.edu.br', 'Suelena Borges', '', 'Sim. Levarei minha própria alimentação, pois é muito detalhada para ser providenciada. Preciso apenas de um local para aquecer a comida em banho-maria ao meio-dia e à noite.', 'S', NULL, NULL, '2013-01-28 21:18:42', 10, NULL, 2),
(657, 'Leandro Pinheiro Vieira', '1916979', 'leandro.vieira@riogrande.ifrs.edu.br', 'Leandro', 'Sim. Dificuldade de subir e descer escadas.', '', 'S', NULL, NULL, '2013-01-28 22:44:14', 12, NULL, NULL),
(658, 'Jovani José Alberti', '995602', 'jovani.alberti@ibiruba.ifrs.edu.br', 'Jovani', '', '', 'S', NULL, NULL, '2013-01-28 23:37:35', 8, NULL, NULL),
(659, 'Iuri Correa Soares', '1990496', 'iuri.correa@poa.ifrs.edu.br', 'Iuri Soares', '', '', 'S', NULL, NULL, '2013-02-04 00:00:00', 13, NULL, NULL),
(660, 'Maria Isabel Accorsi', '1856202', 'maria.accorsi@bento.ifrs.edu.br', 'Maria Isabel Accorsi', '', '', 'S', NULL, NULL, '2013-02-07 00:00:00', 2, NULL, NULL),
(661, 'Bryan Aislan Zinn', '1992053', 'bryan.zinn@ifrs.edu.br', 'Bryan Aislan Zinn', '', '', 'S', NULL, NULL, '2013-02-07 00:00:00', 1, NULL, NULL),
(662, 'Sandra Cristina Donner', '1779678', 'sandra.donner@canoas.ifrs.edu.br', 'Sandra Cristina Donner', '', '', 'S', NULL, NULL, '2013-02-07 00:00:00', 3, NULL, NULL),
(663, 'Carolina Fontoura Cartana', '1886167', 'carolina.cartana@ifrs.edu.br', 'Carolina Fontoura Cartana', '', '', 'S', NULL, NULL, '2013-02-07 00:00:00', 1, NULL, NULL),
(664, 'Bruna Poletto Salton', '1995468', 'bruna.salton@ifrs.edu.br', 'Bruna Poletto Salton', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 1, NULL, NULL),
(665, 'Marlene Gallina Rego', '1850802', 'marlene.rego@ifrs.edu.br', 'Marlene Gallina Rego', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 1, NULL, NULL),
(666, 'Bruno Chagas Alves Fernandes', '1993557', 'bruno.fernandes@ifrs.edu.br', 'Bruno Chagas Alves Fernandes', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 1, NULL, NULL),
(667, 'Ademilde Irene Petzold Prado', '1995996', 'ademilde.prado@restinga.ifrs.edu.br', 'Ademilde Irene Petzold Prado', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 11, NULL, NULL),
(668, 'Paula Biegelmeier Leão', '1968385', 'paula.leao@feliz.ifrs.edu.br', 'Paula Biegelmeier Leão', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 7, NULL, NULL),
(669, 'Cecília Brasil Biguelini', '1976333', 'cecilia.biguelini@feliz.ifrs.edu.br', 'Cecília Brasil Biguelini', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 7, NULL, NULL),
(670, 'Viviane Diehl', '1732533', 'viviane.diehl@feliz.ifrs.edu.br', 'Viviane Diehl', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 7, NULL, NULL),
(671, 'Tulio Lima Basegio', '1995818', 'tulio.basegio@feliz.ifrs.edu.br', 'Tulio Lima Basegio', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 7, NULL, NULL),
(672, 'Ivania dos Santos Lago', '1995597', 'ivania.lago@ifrs.edu.br', 'Ivania dos Santos Lago', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 1, NULL, NULL),
(673, 'Lauri Paulus', '1995397', 'lauri.paulus@ifrs.edu.br', 'Lauri Paulus', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 1, NULL, NULL),
(674, 'Jair Adriano Strapazzon', '1997691', 'jair.strapazzon@ifrs.edu.br', 'Jair Adriano Strapazzon', '', '', 'S', NULL, NULL, '2013-02-25 00:00:00', 1, NULL, NULL),
(675, 'Paulo César Machado', '1993636', 'paulo.machado@ifrs.edu.br', 'Paulo César Machado', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 1, NULL, NULL),
(676, 'Cintia Mussi Alvim Stocchero', '1810596', 'cintia.stocchero@restinga.ifrs.edu.br', 'Cintia Mussi Alvim Stocchero', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 11, NULL, NULL),
(677, 'Diego Eckhard', '1693567', 'diego.eckhard@canoas.ifrgs.edu.br', 'Diego Eckhard', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 3, NULL, NULL),
(678, 'Tânia Craco', '1995260', 'tania.craco@farroupilha.ifrs.edu.br', 'Tânia Craco', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 6, NULL, NULL),
(679, 'Vinicius Hartmann Ferreira', '1885105', 'vinicius.ferreira@feliz.ifrs.edu.br', 'Vinicius Hartmann Ferreira', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 7, NULL, NULL),
(680, 'Simone Valentini', '1996027', 'simone.valentini@farroupilha.ifrs.edu.br', 'Simone Valentini', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 6, NULL, NULL),
(681, 'Priscila Silva Esteves', '1998008', 'priscila.esteves@feliz.ifrs.edu.br', 'Priscila Silva Esteves', '', '', 'S', NULL, NULL, '2013-02-26 00:00:00', 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'admin', '80abd89e635956300adead08bb5a4887bd9d0de9'),
(2, 'joao', '1ecf1dcbb95ed986f1d3ad902f8d0595cf6728a8'),
(3, 'silvia', 'd97d58d3e77d7a9e633400f9db8ee04ea592c113');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albuns`
--
ALTER TABLE `albuns`
  ADD CONSTRAINT `fk_albuns_albuns1` FOREIGN KEY (`parent_id`) REFERENCES `albuns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fotos`
--
ALTER TABLE `fotos`
  ADD CONSTRAINT `fk_fotos_albuns1` FOREIGN KEY (`album_id`) REFERENCES `albuns` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `imagens`
--
ALTER TABLE `imagens`
  ADD CONSTRAINT `fk_imagens_noticias1` FOREIGN KEY (`noticia_id`) REFERENCES `noticias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `obras`
--
ALTER TABLE `obras`
  ADD CONSTRAINT `fk_obras_participantes1` FOREIGN KEY (`participante_id`) REFERENCES `participantes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `participantes`
--
ALTER TABLE `participantes`
  ADD CONSTRAINT `fk_participantes_apresentacoes1` FOREIGN KEY (`apresentacao_id`) REFERENCES `apresentacoes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_participantes_atividades1` FOREIGN KEY (`atividade_id`) REFERENCES `atividades` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_participantes_campus` FOREIGN KEY (`campus_id`) REFERENCES `campus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
